import java.nio.file.Paths;

import com.oly.cms.common.utils.ZipUtils;

public class Itest {

    public static void main(String[] args) {
        String rootPath = "D:/vue/.tzg";
        String backTemplatesPath = Paths.get(rootPath, "themes", "love", "templates", "zgblog")
                .toString();
        String backStaticPath = Paths.get(rootPath, "themes", "love", "static", "zgblog").toString();
        String savePath = Paths.get(rootPath, "back", "love.zip").toString();
        ZipUtils.themeToZip(backTemplatesPath, backStaticPath, savePath, "love", true);
    }

}
