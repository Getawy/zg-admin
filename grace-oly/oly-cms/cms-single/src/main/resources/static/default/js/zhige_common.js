var zgPostHand = {
	lookMaxTime:10,
    lookRecord: function () {
        this.ajaxPost();
    },
    likeRecord: function () {

    },
    nastyRecord: function () {

    },
    shareRecord: function () {

    },
    scoreRecord: function () {

    },
    collectRecord: function () {
        
    }
}

var zgLocalStorage={
	storage:function (){
		return windons.localStorage;
	},
	addItem:function (key,value){
		this.storage.setItem(key,value);
	},
	removeItem:function (key){
		this.storage.removeItem(key);
	},
	getItem:function (key){
    return	this.storage.getItem(key);
	}

}

//JS转换时间戳为“刚刚”、“1分钟前”、“2小时前”“1天前”等格式
var minute = 1000 * 60;
var hour = minute * 60;
var day = hour * 24;
var halfamonth = day * 15;
var month = day * 30;
function getDateDiff(dateTimeStamp) {
    //若你得到的时间格式不是时间戳，可以使用下面的JavaScript函数把字符串转换为时间戳, 本函数的功能相当于JS版的strtotime：
    var idata = Date.parse(dateTimeStamp.replace(/-/gi,"/"));  //js函数代码：字符串转换为时间
    var now = new Date().getTime();
    var diffValue = now - idata;
    if (diffValue < 0) {

        //若日期不符则弹出窗口告之
        //alert("结束日期不能小于开始日期！");
    }
    var monthC = diffValue / month;
    var weekC = diffValue / (7 * day);
    var dayC = diffValue / day;
    var hourC = diffValue / hour;
    var minC = diffValue / minute;
    if (monthC >= 1) {
        result = "发表于" + parseInt(monthC) + "个月前";
    }
    else if (weekC >= 1) {

        result = "发表于" + parseInt(weekC) + "周前";
    }
    else if (dayC >= 1) {
        result = "发表于" + parseInt(dayC) + "天前";
    }
    else if (hourC >= 1) {
        result = "发表于" + parseInt(hourC) + "个小时前";
    }
    else if (minC >= 1) {
        result = "发表于" + parseInt(minC) + "分钟前";
    } else
        result = "刚刚发表";
    return result;
}
