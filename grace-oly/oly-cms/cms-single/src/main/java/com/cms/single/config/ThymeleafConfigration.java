package com.cms.single.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;

import com.grace.common.enums.GraceServerRoot;

/**
 * 加载任意目录模板
 * 模板优先级：
 * 查找非ClassLoader 内的资源
 * 查找ClassLoader templates/themes 目录下的资源
 * 查找ClassLoader templates 目录下的资源
 * 
 */
@Configuration
public class ThymeleafConfigration {
    @Autowired
    private SpringTemplateEngine templateEngine;

    /**
     * 加载任意目录模板
     */
    @PostConstruct
    public void extension() {
        FileTemplateResolver resolver = new FileTemplateResolver();
        // 模板位置.tzg/server/olyCode/themes/templates/theme
        resolver.setPrefix(GraceServerRoot.THEME_DIR.getWorkRoot(GraceServerRoot.THEME_TEMPLATE_DIR.getValue()) + "/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode(TemplateMode.HTML);
        resolver.setOrder(1);
        resolver.setCacheable(false);
        resolver.setCharacterEncoding(("UTF-8"));
        templateEngine.addTemplateResolver(resolver);
    }

    /**
     * ClassLoader加载
     */
    @Bean
    public ClassLoaderTemplateResolver thirdTemplateResolver() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("templates/themes/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding("UTF-8");
        // 越小优先级别越高
        templateResolver.setOrder(2);
        templateResolver.setCheckExistence(true);
        return templateResolver;
    }

    // @Bean
    // public SpringResourceTemplateResolver firstTemplateResolver() {
    // SpringResourceTemplateResolver templateResolver = new
    // SpringResourceTemplateResolver();
    // templateResolver.setPrefix("file:////"+GraceConfig.getWorkPath()+"/themes/");
    // templateResolver.setSuffix(".html");
    // templateResolver.setTemplateMode(TemplateMode.HTML);
    // templateResolver.setCharacterEncoding("UTF-8");
    // templateResolver.setOrder(0);
    // templateResolver.setCheckExistence(true);
    // templateResolver.setCacheable(false);
    // System.out.println("+++++++{PPPPPPPPPPPPPPPPPPPPPPPPPP");
    // return templateResolver;
    // }

    // @Bean
    // public ClassLoaderTemplateResolver secondTemplateResolver() {
    // ClassLoaderTemplateResolver templateResolver = new
    // ClassLoaderTemplateResolver();
    // templateResolver.setPrefix("file:////"+GraceConfig.getWorkPath()+"/themes/");
    // templateResolver.setSuffix(".html");
    // templateResolver.setTemplateMode(TemplateMode.HTML);
    // templateResolver.setCharacterEncoding("UTF-8");
    // templateResolver.setOrder(0);
    // templateResolver.setCheckExistence(true);
    // templateResolver.setCacheable(false);
    // System.out.println("+++++++{PPPPPPPPPPPPPPPPPPPPPPPPPP");
    // return templateResolver;

    // }

}