package com.cms.single.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.enums.OrderEnums;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.general.taglib.ArticleTag;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;

@CrossOrigin
@RestController
@RequestMapping
public class ArticleApiController extends BaseController {

  @Autowired
  private ArticleTag articleService;

  /**
   * 文章综合查询
   * 
   * @param parm
   * @return
   */
  @GetMapping("/{themeName}/api/article/list")
  public AjaxResult listArticleByArticleParam(ArticleSearchParam parm, @PathVariable("themeName") String themeName) {
    List<WebArticleVo> list = new ArrayList<>();
    startPage();
    list = articleService.listWebArticles(parm);

    return AjaxResult.success(PageData.getData(list, 200));
  }

  /**
   * 按照年月获取文章数量
   * 
   * @param themeName 主题名
   * @param pageNum   页面数
   * @param pageSize  每页数量
   * @return
   */
  @GetMapping("/{themeName}/api/article/listArticleTimeNum/{pageNum}/{pageSize}")
  public AjaxResult listArticleTimeNum(@PathVariable("themeName") String themeName,
      @PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize) {
    return AjaxResult.success(articleService.listArticleTimeNum(themeName, pageNum, pageSize));
  }

  /**
   * 依据类型获取所有文章数量
   * 
   * @param themeName
   * @return
   */
  @GetMapping("/{themeName}/api/article/getArticleNum/{articleType}")
  public AjaxResult getArticleNum(@PathVariable("themeName") String themeName,
      @PathVariable("articleTypeEnums") ArticleTypeEnums articleTypeEnums) {
    return AjaxResult.success(articleService.selectArticleNumByType(themeName, articleTypeEnums));
  }

  /**
   * 获取文章HTML
   * 
   * @param themeName
   * @param articleId
   * @return
   */
  @GetMapping("/{themeName}/api/article/getHtml/{articleId}")
  public AjaxResult getArticleHtmlById(@PathVariable("themeName") String themeName,
      @PathVariable("articleId") Long articleId) {
    return AjaxResult.success(articleService.selectWebArticleHtmlById(articleId, themeName));
  }

  /**
   * 获取文章MD
   * 
   * @param themeName
   * @param articleId
   * @return
   */
  @GetMapping("/{themeName}/api/article/getMd/{articleId}")
  public AjaxResult getArticleMdById(@PathVariable("themeName") String themeName,
      @PathVariable("articleId") Long articleId) {
    return AjaxResult.success(articleService.selectWebArticleMdById(articleId, themeName));
  }

  /**
   * 
   * @param categoryId
   * @param num
   * @param size
   * @param themeName
   * @return
   */
  @GetMapping("/{themeName}/api/article/list/category/{categoryId}/{num}/{size}")
  public AjaxResult listArticleByCategoryId(@PathVariable("categoryId") long categoryId,
      @PathVariable("num") Integer num,
      @PathVariable("size") Integer size, @PathVariable("themeName") String themeName) {
    return AjaxResult.success(articleService.listWebArticlesByCategoryId(themeName, categoryId, num, size));
  }

  /**
   * 
   * @param themeName
   * @param categoryId
   * @param num
   * @param size
   * @param order
   * @return
   */
  @GetMapping({ "/{themeName}/api/article/list/category/{categoryId}/{num}/{size}/order/{order}",
      "/{themeName}/api/article/list/tag/{categoryId}/{num}/{size}/order/{order}" })
  public AjaxResult listArticleByCategoryId(@PathVariable("themeName") String themeName,
      @PathVariable("categoryId") long categoryId,
      @PathVariable("num") Integer num,
      @PathVariable("size") Integer size,
      @PathVariable("order") OrderEnums order) {
    return AjaxResult.success(articleService.selectPageDataByCategoryId(themeName, categoryId, num, size, order));
  }

  /**
   * 
   * @param themeName
   * @param categoryId
   * @param num
   * @param size
   * @return
   */
  @GetMapping({ "/{themeName}/api/article/list/category/{categoryId}/{num}/{size}",
      "/{themeName}/api/article/list/tag/{categoryId}/{num}/{size}" })
  public AjaxResult listArticleByCategoryId(@PathVariable("themeName") String themeName,
      @PathVariable("categoryId") long categoryId,
      @PathVariable("num") Integer num,
      @PathVariable("size") Integer size) {
    return AjaxResult.success(articleService.listWebArticlesByCategoryId(themeName, categoryId, num, size));
  }

  /**
   * 
   * @param themeName
   * @param type
   * @param size
   * @param num
   * @return
   */
  @GetMapping("/{themeName}/api/article/list/type/{type}/{num}/{size}")
  public AjaxResult listArticleByType(@PathVariable("themeName") String themeName, @PathVariable("type") int type,
      @PathVariable("size") Integer size,
      @PathVariable("num") int num) {
    return AjaxResult.success(articleService.listWebArticleByType(themeName, type, num, size));
  }

  /**
   * 
   * @param pageNum   页数
   * @param pageSize  大小
   * @param themeName 主题名字
   * @param crTime    时间
   * @return
   */
  @GetMapping({ "/{themeName}/api/article/groupByTime/{num}/{size}/{crTime}",
      "/{themeName}/api/article/groupByTime/{num}/{size}" })
  public PageArticleTimeLine groupByTime(@PathVariable("themeName") String themeName, @PathVariable("num") int pageNum,
      @PathVariable("size") int pageSize,
      @PathVariable(name = "crTime", required = false) String crTime) {
    return articleService.groupByTime(pageNum, pageSize, themeName, crTime);
  }

  /**
   * 文章列表
   * 
   * @param themeName
   * @param pageNum
   * @param pageSize
   * @return
   */
  @GetMapping("/{themeName}/api/article/pageArticle/{num}/{size}")
  public PageData pageArticle(@PathVariable("themeName") String themeName, @PathVariable("num") int pageNum,
      @PathVariable("size") int pageSize) {
    return articleService.pageArticles(themeName, pageNum, pageSize);
  }

}
