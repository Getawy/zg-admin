package com.oly.cms.query.mapper;

import java.util.List;
import com.oly.cms.common.domain.entity.CmsWeb;

public interface WebSearchMapper {

    /**
     * 查询站点
     * 
     * @param webName 站点名
     * @return 站点
     */
    public CmsWeb selectCmsWebByWebName(String webName);

    /**
     * 查询站点列表
     * 
     * @param cmsWeb 站点
     * @return 站点集合
     */
    public List<CmsWeb> selectCmsWebList(CmsWeb cmsWeb);
}
