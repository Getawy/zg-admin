package com.oly.cms.query.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grace.common.utils.StringUtils;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.query.mapper.ArticleSearchMapper;
import com.oly.cms.query.mapper.ThemeSearchMapper;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.oly.cms.query.service.IThemeSearchService;

@Service
public class ThemeSearchServiceImpl implements IThemeSearchService {

    @Autowired
    private ThemeSearchMapper themeSearchMapper;

    @Autowired
    private ArticleSearchMapper articleSearchMapper;

    @Override
    public List<CmsTheme> listCmsTheme(String webName) {
        CmsTheme cmsTheme = new CmsTheme();
        cmsTheme.setWebName(webName);
        return themeSearchMapper.listCmsTheme(cmsTheme);
    }

    @Override
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme) {

        return themeSearchMapper.selectCmsThemeByWt(cmsTheme);
    }

    @Override
    public CmsTheme selectCmsThemeByWt(String webName, String themeName) {
        CmsTheme cmsTheme = new CmsTheme();
        cmsTheme.setWebName(webName);
        cmsTheme.setThemeName(themeName);
        CmsTheme returnTheme = this.selectCmsThemeByWt(cmsTheme);
        if (returnTheme != null) {
            Long supportCategoryId = returnTheme.getSupportCategoryId();
            WebArticleVo webArticleVo = selectArticleLastInsert(supportCategoryId);
            if (webArticleVo != null) {
                returnTheme.setArticleLastInsert(webArticleVo.getCreateTime());
            }
        }
        return returnTheme;
    }

    @Override
    public CmsTheme selectCmsThemeByWt(String wt) {
        String[] s1 = StringUtils.split(wt, "_");
        return this.selectCmsThemeByWt(s1[0], s1[1]);
    }

    private WebArticleVo selectArticleLastInsert(Long supportCategoryId) {
        ArticleSearchParam articleSearchParam = new ArticleSearchParam();
        articleSearchParam.setSupportCategoryId(supportCategoryId);
        return articleSearchMapper.selectArticleLastInsert(articleSearchParam);
    }

}
