package com.oly.cms.query.mapper;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsTheme;

public interface ThemeSearchMapper {
    /**
     * 查询主题
     * webName->w
     * themeName->t
     * 
     * @param cmsTheme
     * @return 主题
     */
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme);

    /**
     * 查询主题列表 通过站点名字
     * 
     * @param cmsTheme
     * @return 主题集合
     */
    public List<CmsTheme> listCmsTheme(CmsTheme cmsTheme);

}
