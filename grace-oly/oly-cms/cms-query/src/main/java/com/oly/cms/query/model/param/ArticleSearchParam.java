package com.oly.cms.query.model.param;

import com.oly.cms.common.domain.vo.ArticleVo;
import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.enums.SearchVisibleType;

/**
 * 文章请求参数
 */
public class ArticleSearchParam extends ArticleVo {
    private static final long serialVersionUID = 1L;

    /* 开始时间 如2022-08 */
    private String crTime;

    // 是否获取Md源码
    private Boolean useArticleMd = false;

    // 是否获取解析内容
    private Boolean useArticleContent = false;

    // 默认通过
    private SearchVisibleType useVisible = SearchVisibleType.DEFAULT;

    // 文章类型
    private ArticleTypeEnums articleTypeEnum;
    // 支持文章类型列表
    private String supportArticleType;

    public ArticleTypeEnums getArticleTypeEnum() {
        return articleTypeEnum;
    }

    public void setArticleTypeEnum(ArticleTypeEnums articleTypeEnum) {
        super.setArticleType(articleTypeEnum.ordinal());
        this.articleTypeEnum = articleTypeEnum;
    }

    public Boolean getUseArticleMd() {
        return useArticleMd;
    }

    public void setUseArticleMd(Boolean useArticleMd) {
        this.useArticleMd = useArticleMd;
    }

    public Boolean getUseArticleContent() {
        return useArticleContent;
    }

    public void setUseArticleContent(Boolean useArticleContent) {
        this.useArticleContent = useArticleContent;
    }

    public String getCrTime() {
        return crTime;
    }

    public void setCrTime(String crTime) {
        this.crTime = crTime;
    }

    public SearchVisibleType getUseVisible() {
        return useVisible;
    }

    public void setUseVisible(SearchVisibleType useVisible) {
        this.useVisible = useVisible;
    }

    public String getSupportArticleType() {
        return supportArticleType;
    }

    public void setSupportArticleType(String supportArticleType) {
        this.supportArticleType = supportArticleType;
    }

}
