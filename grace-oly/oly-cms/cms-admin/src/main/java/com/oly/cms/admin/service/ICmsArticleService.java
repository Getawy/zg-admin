package com.oly.cms.admin.service;

import java.util.List;

import com.oly.cms.admin.model.param.ArticleBenchParam;
import com.oly.cms.common.domain.entity.CmsArticle;
import com.oly.cms.common.domain.vo.ArticleVo;

/** 文章相关操作 */
public interface ICmsArticleService {
    /**
     * 插入文章
     * 
     * @param articleVo
     * @return
     */
    int insertCmsArticle(ArticleVo articleVo);

    /**
     * 根据批量删除文章
     * 
     * @param articleIds
     * @return
     */
    int deleteCmsArticleByIds(Long[] articleIds);

    /**
     * 批量更新文章顶置
     * 
     * @param benchParam
     * @return
     */
    int batchArticleTop(ArticleBenchParam benchParam);

    /**
     * 批量更新文章状态
     * 
     * @param benchParam
     * @return
     */
    int batchArticleVisible(ArticleBenchParam benchParam);

    /**
     * 更新关联分类
     * 
     * @param articleVo
     * @return
     */
    int batchArticleCategory(ArticleVo articleVo);

    /**
     * 更新更新人
     * 
     * @param articleId
     * @param updateBy
     * @return
     */
    int updateArticleUpdateBy(Long articleId, String updateBy);

    /**
     * 根据文章id获取文章
     * 
     * @param id
     * @return
     */
    ArticleVo selectCmsArticleById(Long id);

    /**
     * 获取文章列表
     * 
     * @param artVo
     * @return
     */
    List<CmsArticle> listCmsArticle(ArticleVo artVo);

    /**
     * 更新文章通过文章id
     * 
     * @param cmsArticle
     * @return
     */
    int updateCmsArticleById(ArticleVo cmsArticle);

    int removeArticleCategory(Long articleId, Long categoryId, List<String> syncThemeNames);

    int addArticleCategory(Long articleId, Long categoryId, List<String> syncThemeNames);

    int addArticleCategory(Long articleId, Long[] categoryIds, String themeName);

    int removeArticleCategory(Long articleId, Long[] categoryIds, String themeName);

    /**
     * 检查是否cmsArticle唯一
     * 
     * @param cmsArticle
     * @return
     */
    boolean checkArticleUnique(CmsArticle cmsArticle);

    /**
     * 获取关联列表
     * 
     * @param articleIds
     * @return
     */
    Long[] selectCategoryIdsByArticleIds(Long[] articleIds);

}
