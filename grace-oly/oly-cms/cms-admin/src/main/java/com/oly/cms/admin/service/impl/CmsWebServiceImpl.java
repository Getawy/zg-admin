package com.oly.cms.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oly.cms.admin.mapper.CmsWebMapper;
import com.oly.cms.admin.service.ICmsWebService;
import com.oly.cms.common.domain.entity.CmsWeb;
import com.oly.cms.query.mapper.WebSearchMapper;
import com.grace.common.utils.DateUtils;

/**
 * 站点Service业务层处理
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
@Service
public class CmsWebServiceImpl implements ICmsWebService {
    @Autowired
    private CmsWebMapper cmsWebMapper;

    @Autowired
    private WebSearchMapper webSearchMapper;

    /**
     * 查询站点
     * 
     * @param webId 站点主键
     * @return 站点
     */
    @Override
    public CmsWeb selectCmsWebByWebName(String webName) {
        return webSearchMapper.selectCmsWebByWebName(webName);
    }

    /**
     * 查询站点列表
     * 
     * @param cmsWeb 站点
     * @return 站点
     */
    @Override
    public List<CmsWeb> selectCmsWebList(CmsWeb cmsWeb) {
        return webSearchMapper.selectCmsWebList(cmsWeb);
    }

    /**
     * 新增站点
     * 
     * @param cmsWeb 站点
     * @return 结果
     */
    @Override
    public int insertCmsWeb(CmsWeb cmsWeb) {
        cmsWeb.setCreateTime(DateUtils.getNowDate());
        return cmsWebMapper.insertCmsWeb(cmsWeb);
    }

    /**
     * 修改站点
     * 
     * @param cmsWeb 站点
     * @return 结果
     */
    @Override
    public int updateCmsWeb(CmsWeb cmsWeb) {
        cmsWeb.setUpdateTime(DateUtils.getNowDate());
        return cmsWebMapper.updateCmsWeb(cmsWeb);
    }

    /**
     * 批量删除站点
     * 
     * @param webNames 需要删除的站点主键
     * @return 结果
     */
    @Override
    public int deleteCmsWebByWebNames(String[] webNames) {
        return cmsWebMapper.deleteCmsWebByWebNames(webNames);
    }

    /**
     * 删除站点信息
     * 
     * @param webName 站点主键
     * @return 结果
     */
    @Override
    public int deleteCmsWebByWebName(String webName) {
        return cmsWebMapper.deleteCmsWebByWebName(webName);
    }
}
