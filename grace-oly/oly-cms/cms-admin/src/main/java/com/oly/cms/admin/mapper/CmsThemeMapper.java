package com.oly.cms.admin.mapper;

import org.apache.ibatis.annotations.Param;

import com.oly.cms.common.domain.entity.CmsTheme;

/**
 * 主题Mapper接口
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
public interface CmsThemeMapper {

    /**
     * 新增主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    public int insertCmsTheme(CmsTheme cmsTheme);

    /**
     * 修改主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    public int updateCmsTheme(CmsTheme cmsTheme);

    /**
     * 删除主题
     * 
     * @param cmsTheme
     * @return 结果
     */
    public int deleteCmsThemeByTn(CmsTheme cmsTheme);

    /**
     * 分类关联主题
     * 
     * @param themeName
     * @param categoryId
     * @return
     */
    public int categoryUnionTheme(@Param("themeName") String themeName, @Param("categoryId") Long categoryId);

}
