package com.oly.cms.admin.service;

import com.oly.cms.common.domain.VisibleParam;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.entity.CmsCategoryTheme;
import com.oly.cms.query.model.param.CategorySearchParam;

import java.util.List;

public interface ICmsCategoryService {
    /**
     * 添加类目
     * 
     * @param param
     * @return
     */
    int insertCmsCategory(CategorySearchParam param);

    /**
     * 修改类目
     * 
     * @param param
     * @return
     */
    int updateCmsCategory(CategorySearchParam param);

    /**
     * 删除单个类目导航
     * 
     * @param categoryId
     * @return
     */
    int deleteCmsCategoryById(Long categoryId);

    /**
     * 获取列表
     * 
     * @param param
     * @return
     */
    List<CmsCategory> listCmsCategory(CategorySearchParam param);

    /**
     * 获取一条类目 所有字段
     * 
     * @param categoryId
     * @return
     */
    CmsCategory selectCmsCategoryById(CategorySearchParam param);

    /**
     * 验证同类目下类目名唯一
     * 是否存在子节点
     * 
     * @param param
     * @param node
     * @return
     */
    boolean checkCategoryUnique(CmsCategory param, boolean node);

    /**
     * 统计关联文章
     * 
     * @param categoryId
     * @return
     */
    int countArticleByCategoryId(Long categoryId);

    /**
     * 获取类目列表通过文章id 文章关联类目
     * 
     * @param vParam
     * @return
     */
    List<CmsCategory> listCmsCategoryByArticleId(VisibleParam vParam);

    /**
     * 更新分类状态
     * 
     * @param param
     * @return
     */
    int updateChildCategoryVisible(CmsCategory param);

    /**
     * 添加分类主题关联
     * 
     * @param themeName
     * @param categoryIds
     * @return
     */
    int insertCategoryTheme(String themeName, List<Long> categoryIds);

    /**
     * 移除分类
     * 
     * @param themeName
     * @param categoryIds
     * @return
     */
    int deleteCategoryTheme(String themeName, List<Long> categoryIds);

    List<CmsCategoryTheme> listCategoryTheme(CmsCategoryTheme categoryTheme);

}
