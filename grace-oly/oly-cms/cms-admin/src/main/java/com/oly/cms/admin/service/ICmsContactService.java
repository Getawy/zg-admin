package com.oly.cms.admin.service;

import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.query.mapper.ContactSearchMapper;

public interface ICmsContactService extends ContactSearchMapper {

    /**
     * 新增反馈|建议
     * 
     * @param cmsContact 反馈|建议
     * @return 结果
     */
    public int insertCmsContact(CmsContact cmsContact);

    /**
     * 修改反馈|建议
     * 
     * @param cmsContact 反馈|建议
     * @return 结果
     */
    public int updateCmsContact(CmsContact cmsContact);

    /**
     * 批量删除反馈|建议
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsContactByIds(Long[] contactIds);
}
