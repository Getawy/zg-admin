package com.oly.cms.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.forum.comment.domain.CmsComment;
import com.oly.cms.admin.mapper.CmsReportMapper;
import com.oly.cms.admin.service.ICmsReportService;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.common.domain.entity.CmsLogRecord;
import com.oly.cms.common.domain.entity.CmsUnion;
import com.oly.cms.common.domain.vo.ArticleVo;

@Service
public class CmsReportServiceImpl implements ICmsReportService {

    @Autowired
    private CmsReportMapper cmsReportMapper;

    @Override
    public int countCmsArticle(ArticleVo artVo) {

        return cmsReportMapper.countCmsArticle(artVo);
    }

    @Override
    public int countCmsLogRecord(CmsLogRecord cmsLogRecord) {

        return cmsReportMapper.countCmsLogRecord(cmsLogRecord);
    }

    @Override
    public int countCmsCat(CmsCategory CmsCategory) {

        return cmsReportMapper.countCmsCat(CmsCategory);
    }

    @Override
    public int countCmsComment(CmsComment cmsComment) {

        return cmsReportMapper.countCmsComment(cmsComment);
    }

    @Override
    public int countCmsContact(CmsContact cmsContact) {

        return cmsReportMapper.countCmsContact(cmsContact);
    }

    @Override
    public int countCmsUnion(CmsUnion cmsUnion) {

        return cmsReportMapper.countCmsUnion(cmsUnion);
    }

}
