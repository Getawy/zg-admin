package com.oly.cms.admin.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 文章关联相关
 */
public interface CmsArticleLiquidMapper {

    /**
     * 添加文章与分类关联
     * 
     * @param articleId
     * @param categoryId
     * @return
     */
    int 
    insertArticleCategory(@Param("articleId") Long articleId, @Param("categoryId") Long categoryId);

    /**
     * 批量关联文章与分类关联
     * 
     * @param articleId
     * @param categoryIds
     * @return
     */
    int batchInsertArticleCategory(@Param("articleId") Long articleId, @Param("categoryIds") Long[] categoryIds);

    /**
     * 删除文章关联分类
     * 
     * @param articleId
     * @param categoryId
     * @return
     */
    int deleteArticleCategory(@Param("articleId") Long articleId, @Param("categoryId") Long categoryId);

    /**
     * 批量删除文章与分类关联
     * 
     * @param articleIds
     * @return
     */
    int batchDeleteArticleCategoryByArticleIds(Long[] articleIds);

    /**
     * 插入文章统计
     * 
     * @param articleId
     * @return
     */
    int insertCmsArticleCount(Long articleId);

    /**
     * 通过分类ID更新分类关联文章数量
     * 
     * @param categoryIds
     * @return
     */
    int updateCmsCategoryArticleCountByIds(Long[] categoryIds);

    /**
     * 统计文章关联分类数
     * 
     * @param articleId
     * @return
     */
    int selectCountArticleCategory(long articleId);

    int deleteCmsCommentByIds(Long[] articleIds);

    int deleteCmsCommentRecordByIds(Long[] articleIds);

    int deleteCmsLookRecordByIds(Long[] articleIds);

    int deleteCmsShareRecordByIds(Long[] articleIds);

    int deleteCmsLikeRecordByIds(Long[] articleIds);

    int deleteCmsNastyRecordByIds(Long[] articleIds);

    int deleteCmsCollectRecordByIds(Long[] articleIds);

    int deleteCmsScoreRecordByIds(Long[] articleIds);

    int deleteCmsArticleCountByIds(Long[] articleIds);

    /**
     * 获取关联列表
     * 
     * @param articleIds
     * @return
     */
    Long[] selectCategoryIdsByArticleIds(Long[] articleIds);

    /**
     * 检查当前主题是否包含当前分类
     * 
     * @param themeName
     * @param categoryId
     * @return
     */
    int checkThemeIncludeCategory(@Param("themeName") String themeName, @Param("categoryId") Long categoryId);

}
