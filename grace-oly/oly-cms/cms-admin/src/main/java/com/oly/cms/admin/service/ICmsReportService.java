package com.oly.cms.admin.service;

import com.cms.forum.comment.domain.CmsComment;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.common.domain.entity.CmsLogRecord;
import com.oly.cms.common.domain.entity.CmsUnion;
import com.oly.cms.common.domain.vo.ArticleVo;

public interface ICmsReportService {
    /**
     * 统计文章
     * 
     * @param artVo
     * @return
     */
    int countCmsArticle(ArticleVo artVo);

    /**
     * 统计记录
     * 
     * @param cmsLogRecord
     * @return
     */
    int countCmsLogRecord(CmsLogRecord cmsLogRecord);

    /**
     * 统计分类
     * 
     * @param CmsCategory
     * @return
     */
    int countCmsCat(CmsCategory CmsCategory);

    /**
     * 评论统计
     * 
     * @param cmsComment
     * @return
     */
    int countCmsComment(CmsComment cmsComment);

    /**
     * 统计反馈
     * 
     * @param cmsContact
     * @return
     */
    int countCmsContact(CmsContact cmsContact);

    /**
     * 联盟统计
     * 
     * @param cmsUnion
     * @return
     */
    int countCmsUnion(CmsUnion cmsUnion);

}
