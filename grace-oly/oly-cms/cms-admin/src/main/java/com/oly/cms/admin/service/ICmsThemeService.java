package com.oly.cms.admin.service;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsTheme;

/**
 * 主题Service接口
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
public interface ICmsThemeService {
    /**
     * 查询主题
     * 
     * @param cmsTheme
     * @return 主题
     */
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme);

    /**
     * 查询主题列表
     * 
     * @param cmsTheme
     * @return 主题集合
     */
    public List<CmsTheme> listCmsTheme(CmsTheme cmsTheme);

    /**
     * 新增主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    public int insertCmsTheme(CmsTheme cmsTheme);

    /**
     * 修改主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    public int updateCmsTheme(CmsTheme cmsTheme);

    /**
     * 删除主题信息
     * 
     * @param cmsTheme
     * @return 结果
     */
    public int deleteCmsThemeByTn(CmsTheme cmsTheme);

    /**
     * 分类关联主题
     * 
     * @param themeName
     * @param categoryId
     * @return
     */
    int categoryUnionTheme(String themeName, Long categoryId);

}
