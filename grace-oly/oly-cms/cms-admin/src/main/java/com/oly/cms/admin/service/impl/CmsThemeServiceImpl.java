package com.oly.cms.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.oly.cms.admin.mapper.CmsThemeMapper;
import com.oly.cms.admin.service.ICmsThemeService;
import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.event.CacheWebRefreshAllEvent;
import com.oly.cms.query.mapper.ThemeSearchMapper;
import com.grace.common.utils.DateUtils;

/**
 * 主题Service业务层处理
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
@Service
public class CmsThemeServiceImpl implements ICmsThemeService {
    @Autowired
    private CmsThemeMapper cmsThemeMapper;

    @Autowired
    private ThemeSearchMapper searchMapper;

    @Autowired
    private ApplicationEventPublisher app;

    /**
     * 查询主题
     * 
     * @param cmsTheme
     * @return 主题
     */
    @Override
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme) {
        return searchMapper.selectCmsThemeByWt(cmsTheme);
    }

    /**
     * 查询主题列表
     * 
     * @param cmsTheme
     * @return 主题
     */
    @Override
    public List<CmsTheme> listCmsTheme(CmsTheme cmsTheme) {
        return searchMapper.listCmsTheme(cmsTheme);
    }

    /**
     * 新增主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    @Override
    public int insertCmsTheme(CmsTheme cmsTheme) {
        cmsTheme.setCreateTime(DateUtils.getNowDate());
        int re = cmsThemeMapper.insertCmsTheme(cmsTheme);
        app.publishEvent(new CacheWebRefreshAllEvent(this, CmsCacheConstant.THEME_CACHE_KEY_PREFIX));
        return re;
    }

    /**
     * 修改主题
     * 
     * @param cmsTheme 主题
     * @return 结果
     */
    @Override
    public int updateCmsTheme(CmsTheme cmsTheme) {
        int re = cmsThemeMapper.updateCmsTheme(cmsTheme);
        app.publishEvent(new CacheWebRefreshAllEvent(this, CmsCacheConstant.THEME_CACHE_KEY_PREFIX));
        return re;
    }

    /**
     * 删除主题信息
     * 
     * @param cmsTheme
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteCmsThemeByTn(CmsTheme cmsTheme) {
        return cmsThemeMapper.deleteCmsThemeByTn(cmsTheme);
    }

    /**
     * 主题关联分类
     * 
     * @param themeName
     * @param categoryId
     * @return 结果
     */
    @Override
    public int categoryUnionTheme(String themeName, Long categoryId) {
        return cmsThemeMapper.categoryUnionTheme(themeName, categoryId);
    }

}
