package com.oly.cms.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oly.cms.admin.mapper.CmsContactMapper;
import com.oly.cms.admin.service.ICmsContactService;
import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.query.mapper.ContactSearchMapper;

@Service
public class CmsContactServiceImpl implements ICmsContactService {

    @Autowired
    private ContactSearchMapper contactSearchMapper;

    @Autowired
    private CmsContactMapper cmsContactMapper;

    @Override
    public int deleteCmsContactByIds(Long[] contactIds) {

        return cmsContactMapper.deleteCmsContactByIds(contactIds);
    }

    @Override
    public int insertCmsContact(CmsContact cmsContact) {

        return cmsContactMapper.insertCmsContact(cmsContact);
    }

    @Override
    public int updateCmsContact(CmsContact cmsContact) {

        return cmsContactMapper.updateCmsContact(cmsContact);
    }

    @Override
    public CmsContact selectCmsContactById(Long contactId) {

        return contactSearchMapper.selectCmsContactById(contactId);
    }

    @Override
    public List<CmsContact> listCmsContact(CmsContact cmsContact) {

        return contactSearchMapper.listCmsContact(cmsContact);
    }

}
