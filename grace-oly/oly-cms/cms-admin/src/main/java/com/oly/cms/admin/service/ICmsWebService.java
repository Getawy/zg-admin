package com.oly.cms.admin.service;

import com.oly.cms.common.domain.entity.CmsWeb;
import com.oly.cms.query.service.IWebSearchService;

/**
 * 站点Service接口
 * 
 * @author zhiGe
 * @date 2022-12-06
 */
public interface ICmsWebService extends IWebSearchService {

    /**
     * 新增站点
     * 
     * @param cmsWeb 站点
     * @return 结果
     */
    public int insertCmsWeb(CmsWeb cmsWeb);

    /**
     * 修改站点
     * 
     * @param cmsWeb 站点
     * @return 结果
     */
    public int updateCmsWeb(CmsWeb cmsWeb);

    /**
     * 批量删除站点
     * 
     * @param webIds 需要删除的站点主键集合
     * @return 结果
     */
    public int deleteCmsWebByWebNames(String[] webNames);

    /**
     * 删除站点信息
     * 
     * @param webName 站点主键
     * @return 结果
     */
    public int deleteCmsWebByWebName(String webName);
}
