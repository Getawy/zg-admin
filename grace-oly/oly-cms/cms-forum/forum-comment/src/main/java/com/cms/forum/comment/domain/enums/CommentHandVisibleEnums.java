package com.cms.forum.comment.domain.enums;

public enum CommentHandVisibleEnums {
    // 喜欢
    LIKE,
    // 不喜欢
    NASTY,
    // 取消喜欢
    C_LIKE,
    // 取消不喜欢
    C_NASTY;
}
