package com.cms.forum.comment.domain.enums;

public enum CommentVisibleEnums {
    /* 审核中 */
    CHECK,
    /* 禁用|审核不通过 */
    CLOSE,
    /* 审核通过 */
    PASS
}
