package com.cms.forum.comment.domain.enums;

public enum CommentTypeEnum {
    ARTICLE,
    SITE,
    LINKS;
}
