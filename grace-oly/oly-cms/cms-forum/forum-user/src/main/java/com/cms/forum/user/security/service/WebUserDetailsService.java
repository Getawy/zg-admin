package com.cms.forum.user.security.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.grace.common.core.domain.entity.SysUser;
import com.grace.common.core.domain.model.LoginUser;
import com.grace.common.enums.UserStatus;
import com.grace.common.utils.StringUtils;
import com.grace.system.service.ISysUserService;

/**
 * 用户验证处理
 * @author grace
 */
@Service
public class WebUserDetailsService implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(WebUserDetailsService.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private WebPasswordService passwordService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = userService.selectUserByUserName(username);
        if (StringUtils.isNull(user)) {
            log.info("登录用户：{} 不存在.", username);
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        } else if (UserStatus.DELETED.getCode().equals(user.getDelFlag())) {
            log.info("登录用户：{} 已被删除.", username);
            throw new LockedException("对不起，您的账号：" + username + " 已被删除");
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", username);
            throw new DisabledException("对不起，您的账号：" + username + " 已停用");
        }

        passwordService.validate(user);
        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUser user) {
        return new LoginUser(user.getUserId(), user.getDeptId(), user, null);
    }
}
