package com.cms.forum.user.service;

import java.util.List;

import com.cms.forum.comment.domain.enums.CommentTypeEnum;
import com.cms.forum.comment.domain.vo.CmsCommentVo;
import com.oly.cms.common.model.support.PageData;

public interface IHandCommentService {

    List<CmsCommentVo> listCommentOneByTypeId(String typeId, int pageNum, int pageSize);

    List<CmsCommentVo> listCommentTwoByParentId(long parentId, int pageNum, int pageSize);

    List<CmsCommentVo> listCommentByTypeId(String typeId, int pageNum, int pageSize);

    PageData pageOneCommentByTypeId(String typeId, int pageNum, int pageSize);

    PageData pageTwoCommentByParentId(long parentId, int pageNum, int pageSize);

    PageData pageCommentByTypeId(String typeId, int pageNum, int pageSize);

    /**
     * 评论类型
     * 
     * @param commentTypeEnum
     * @return
     */
    boolean commentSupport(CommentTypeEnum commentTypeEnum);

}
