package com.cms.forum.user.security.hand;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.alibaba.fastjson2.JSONObject;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.ServletUtils;
import com.grace.common.utils.StringUtils;

/** 认证异常处理类 */
public class WebAuthenticationEntryPointImpl implements AuthenticationEntryPoint {
    /**
     * 处理认证异常的函数。
     * 当用户请求访问受保护的资源时，如果认证失败，会触发此方法。
     * 根据请求是否为AJAX请求，采取不同的错误处理方式。
     * 
     * @param request       HttpServletRequest对象，代表客户端的HTTP请求。
     * @param response      HttpServletResponse对象，用于向客户端发送响应。
     * @param authException 认证异常对象，包含认证失败的详细信息。
     * @throws IOException      如果发生I/O错误。
     * @throws ServletException 如果发生Servlet相关错误。
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) throws IOException, ServletException {
        if (ServletUtils.isAjaxRequest(request)) {
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.getWriter().write(JSONObject.toJSONString(AjaxResult.error(StringUtils
                    .format("请求访问：{}，认证失败，无法访问系统资源:{}", request.getRequestURI(), authException.getMessage()))));
            response.getWriter().flush();
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, authException.getMessage());
        }

    }

}