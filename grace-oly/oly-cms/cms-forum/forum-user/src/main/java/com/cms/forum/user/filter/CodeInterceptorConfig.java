package com.cms.forum.user.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CodeInterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private CodeInterceptor codeInterceptor;

    /**
     * 自定义拦截规则
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(codeInterceptor).addPathPatterns("/web/hand/user/register");
    }

}
