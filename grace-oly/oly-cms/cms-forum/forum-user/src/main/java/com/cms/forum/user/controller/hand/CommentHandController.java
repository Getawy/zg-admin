package com.cms.forum.user.controller.hand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cms.forum.comment.domain.CmsComment;
import com.cms.forum.comment.domain.CmsCommentHand;
import com.cms.forum.comment.domain.enums.CommentHandVisibleEnums;
import com.cms.forum.comment.domain.enums.CommentTypeEnum;
import com.cms.forum.comment.domain.enums.CommentVisibleEnums;
import com.cms.forum.comment.domain.properties.OlyCommentProperties;
import com.cms.forum.comment.domain.vo.CmsCommentVo;
import com.cms.forum.comment.service.impl.CmsCommentHandServiceImpl;
import com.cms.forum.comment.service.impl.CmsCommentServiceImpl;
import com.cms.forum.user.service.RecordStatusService;
import com.cms.forum.user.service.tadlib.CommentTag;
import com.grace.common.annotation.RepeatSubmit;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.text.Convert;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.ServletUtils;
import com.grace.common.utils.html.EscapeUtil;
import com.grace.common.utils.ip.IpUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 评论控制
 * 
 * 小于默认一分钟重复提交
 */
@RestController
@RequestMapping
public class CommentHandController {

    @Autowired
    private CmsCommentServiceImpl cmsCommentService;

    @Autowired
    private CmsCommentHandServiceImpl cmsCommentHandService;

    @Autowired
    private SysSearchConfigServiceImpl configService;

    @Autowired
    private CommentTag commentTag;

    @Autowired
    private RecordStatusService recordStatusService;

    /**
     * 添加评论
     * 
     * @param cmsComment
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/comment/addComment")
    public AjaxResult addComment(@PathVariable("themeName") String themeName, @Validated CmsComment cmsComment,
            CommentTypeEnum type) {
        String maxSize = configService.selectConfigDefaultValue(
                OlyCommentProperties.COMMENT_CONFIG_GROUP.defaultValue(),
                OlyCommentProperties.COMMENT_MAX_SIZE);
        if (Integer.parseInt(maxSize) < EscapeUtil.clean(cmsComment.getContent()).length()) {
            return AjaxResult.error("内容超出大上限:" + maxSize + "个字符");
        }
        if (commentTag.commentSupport(type)) {
            switch (type) {
                case ARTICLE:
                    if (!recordStatusService.selectArticleCommentStatus(Convert.toLong(cmsComment.getTypeId()))) {
                        return AjaxResult.error("评论已关闭.");
                    }
                    break;
                case SITE:

                    break;
                case LINKS:

                    break;
            }
            if (SecurityUtils.getLoginUser() == null) {
                return AjaxResult.error("请登录后进行操作.");
            } else if (SecurityUtils.getUsername() == cmsComment.getReplyBy()) {
                return AjaxResult.error("不允许回复自己.");
            } else {
                UserAgent userAgent = getUserAgent();
                String ip = IpUtils.getIpAddr();
                ;
                cmsComment.setIp(ip);
                cmsComment.setCommentType(type.ordinal());
                cmsComment.setFromBy(SecurityUtils.getUsername());
                cmsComment.setUserSystem(userAgent.getOperatingSystem().getName());
                cmsComment.setUserBower(userAgent.getBrowser().getName());
                String value = configService.selectConfigDefaultValue(
                        OlyCommentProperties.COMMENT_CONFIG_GROUP.defaultValue(),
                        OlyCommentProperties.COMMENT_DEFAULT_VISIBLE);
                cmsComment.setVisible(CommentVisibleEnums.valueOf(value).ordinal());
                return AjaxResult.success(cmsCommentService.insertCmsComment(cmsComment));
            }
        } else {
            return AjaxResult.error("全站禁止评论!");
        }
    }

    /**
     * 点赞|取消点赞
     * 
     * @param commentId
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/comment/addCommentLike")
    public AjaxResult addCommentLike(@PathVariable("themeName") String themeName, long commentId) {
        CmsComment comment = cmsCommentService.selectCmsCommentVoById(commentId);
        if (comment == null || comment.getVisible().intValue() != CommentVisibleEnums.PASS.ordinal()) {
            return AjaxResult.error("操作节点不存在或者不存在");
        }
        CmsCommentHand cmsCommentHand = new CmsCommentHand();
        cmsCommentHand.setCommentId(commentId);
        cmsCommentHand.setUserId(SecurityUtils.getUserId());
        CmsCommentHand aHand = cmsCommentHandService.selectCmsCommentHand(cmsCommentHand);
        if (aHand == null) {
            aHand = new CmsCommentHand();
            aHand.setTypeId(comment.getTypeId());
            aHand.setUserId(SecurityUtils.getUserId());
            aHand.setCommentId(commentId);
            aHand.setVisible(CommentHandVisibleEnums.LIKE.ordinal());
            setHand(aHand);
            return AjaxResult.success(cmsCommentHandService.insertCmsCommentHand(aHand));
        } else {
            int vvl = aHand.getVisible().intValue();
            if (vvl == CommentHandVisibleEnums.LIKE.ordinal()) {
                aHand.setVisible(CommentHandVisibleEnums.C_LIKE.ordinal());
            } else if (vvl == CommentHandVisibleEnums.NASTY.ordinal()) {
                return AjaxResult.error("不允许同时赞和踩");
            } else {
                aHand.setVisible(CommentHandVisibleEnums.LIKE.ordinal());
            }
            setHand(aHand);
            cmsCommentHandService.updateCmsCommentHand(aHand);
            return AjaxResult.success(aHand);
        }
    }

    /**
     * 反对|取消反对
     * 
     * @param commentId
     * @return
     */
    @RepeatSubmit(interval = 60000)

    @PostMapping("/{themeName}/hand/comment/addCommentNasty")
    public AjaxResult addCommentNasty(@PathVariable("themeName") String themeName, long commentId) {
        CmsCommentVo comment = cmsCommentService.selectCmsCommentVoById(commentId);
        if (comment == null || comment.getVisible().intValue() != CommentVisibleEnums.PASS.ordinal()) {
            return AjaxResult.error("操作节点不存在或者不存在");
        }
        CmsCommentHand cmsCommentHand = new CmsCommentHand();
        cmsCommentHand.setCommentId(commentId);
        cmsCommentHand.setUserId(SecurityUtils.getUserId());
        CmsCommentHand aHand = cmsCommentHandService.selectCmsCommentHand(cmsCommentHand);
        if (aHand == null) {
            aHand = new CmsCommentHand();
            aHand.setUserId(SecurityUtils.getUserId());
            aHand.setCommentId(commentId);
            aHand.setTypeId(comment.getTypeId());
            aHand.setVisible(CommentHandVisibleEnums.NASTY.ordinal());
            setHand(aHand);
            return AjaxResult.success(cmsCommentHandService.insertCmsCommentHand(aHand));
        } else {
            int vvl = aHand.getVisible().intValue();
            if (vvl == CommentHandVisibleEnums.NASTY.ordinal()) {
                aHand.setVisible(CommentHandVisibleEnums.C_NASTY.ordinal());
            } else if (vvl == CommentHandVisibleEnums.LIKE.ordinal()) {
                return AjaxResult.error("不允许同时赞和踩");
            } else {
                aHand.setVisible(CommentHandVisibleEnums.NASTY.ordinal());
            }
            setHand(aHand);
            return AjaxResult.success(cmsCommentHandService.updateCmsCommentHand(aHand));
        }
    }

    @PostMapping("/{themeName}/hand/comment/selectCommentHand")
    public AjaxResult selectCommentHand(@PathVariable("themeName") String themeName, long commentId) {
        CmsCommentHand cmsCommentHand = new CmsCommentHand();
        cmsCommentHand.setCommentId(commentId);
        cmsCommentHand.setUserId(SecurityUtils.getUserId());
        return AjaxResult.success(cmsCommentHandService.selectCmsCommentHand(cmsCommentHand));
    }

    @GetMapping("/{themeName}/hand/comment/listCommentHandByTypeId/{typeId}")
    public AjaxResult listCommentHand(@PathVariable("typeId") String typeId) {
        CmsCommentHand cmsCommentHand = new CmsCommentHand();
        cmsCommentHand.setTypeId(typeId);
        cmsCommentHand.setUserId(SecurityUtils.getUserId());
        return AjaxResult.success(cmsCommentHandService.listCmsCommentHand(cmsCommentHand));
    }

    /**
     * 获取请求头
     * 
     * @return
     */
    private UserAgent getUserAgent() {
        return UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
    }

    private void setHand(CmsCommentHand commentHand) {
        UserAgent userAgent = getUserAgent();
        String ip = IpUtils.getIpAddr();
        commentHand.setIp(ip);
        commentHand.setUserSystem(userAgent.getOperatingSystem().getName());
        commentHand.setUserBower(userAgent.getBrowser().getName());
    }

}
