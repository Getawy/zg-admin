package com.cms.forum.user.controller.hand;

import com.grace.common.annotation.RepeatSubmit;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.ServletUtils;
import com.grace.common.utils.ip.IpUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oly.cms.common.enums.CommonVisibleEnums;
import com.oly.cms.common.model.param.WebRecordParam;
import com.cms.forum.user.model.enums.RecordTableEnum;
import com.cms.forum.user.service.impl.HandRecordServiceImpl;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 默认小于一分钟为重复提交
 */
@Controller
@RequestMapping
public class HandRecordController {
    @Autowired
    private HandRecordServiceImpl recordService;

    /**
     * 喜欢|点赞记录表 不能同时喜欢 讨厌
     * 
     * @param webRecordParam
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/record/addLikeRecord")
    @ResponseBody
    public AjaxResult addLikeRecord(@PathVariable("themeName") String themeName, WebRecordParam webRecordParam) {
        s(webRecordParam);
        webRecordParam.setRecordTable(RecordTableEnum.LIKE_RECORD.getValue());
        webRecordParam.setScore(null);
        webRecordParam.setShareUrl(null);
        String likeVisible = recordService.selectRecordVisible(RecordTableEnum.LIKE_RECORD,
                webRecordParam.getArticleId(),
                webRecordParam.getCreateBy());
        if ("0".equals(recordService.selectRecordVisible(RecordTableEnum.NASTY_RECORD, webRecordParam.getArticleId(),
                webRecordParam.getCreateBy()))) {
            return AjaxResult.success("你已经踩过了！", "Already nasty it");
        } else if ("0".equals(likeVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.HIDE.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("取消点赞成功！", "cancel");
        } else if ("1".equals(likeVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.SHOW.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("点赞成功！", "update");
        }
        // 没踩 点一次点赞 或者取消再次点赞
        else {
            webRecordParam.setVisible(CommonVisibleEnums.SHOW.ordinal());
            recordService.insertLikeRecord(webRecordParam);
            return AjaxResult.success("点赞成功！", "add");
        }
    }

    /**
     * 不喜欢|踩记录表 不能同时喜欢 讨厌
     * 
     * @param webRecordParam
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/record/addNastyRecord")
    @ResponseBody
    public AjaxResult addNastyRecord(@PathVariable("themeName") String themeName, WebRecordParam webRecordParam) {
        s(webRecordParam);
        webRecordParam.setRecordTable(RecordTableEnum.NASTY_RECORD.getValue());
        webRecordParam.setScore(null);
        webRecordParam.setShareUrl(null);
        String nastyVisible = recordService.selectRecordVisible(RecordTableEnum.NASTY_RECORD,
                webRecordParam.getArticleId(),
                webRecordParam.getCreateBy());
        // 已经点赞了
        if ("0".equals(recordService.selectRecordVisible(RecordTableEnum.LIKE_RECORD, webRecordParam.getArticleId(),
                webRecordParam.getCreateBy()))) {
            return AjaxResult.success("你已经点赞了", "Already liked it");
        }
        // 已经踩了
        else if ("0".equals(nastyVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.HIDE.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("取消踩成功！", "cancel");
        } else if ("1".equals(nastyVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.SHOW.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("踩成功", "update");
        }

        else {
            s(webRecordParam);
            webRecordParam.setVisible(CommonVisibleEnums.SHOW.ordinal());
            recordService.insertNastyRecord(webRecordParam);
            return AjaxResult.success("踩成功！", "add");
        }
    }

    /**
     * 添加评分
     * 
     * @param webRecordParam
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/record/addScoreRecord")
    @ResponseBody
    public AjaxResult addScoreRecord(@PathVariable("themeName") String themeName, WebRecordParam webRecordParam) {
        String scoreVisible = recordService.selectRecordVisible(RecordTableEnum.SCORE_RECORD,
                webRecordParam.getArticleId(),
                webRecordParam.getCreateBy());
        webRecordParam.setShareUrl(null);
        webRecordParam.setRecordTable(RecordTableEnum.SCORE_RECORD.getValue());
        if ("1".equals(scoreVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.HIDE.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("取消评分成功", "cancel");
        } else if ("0".equals(scoreVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.SHOW.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("添加评分成功", "update");
        } else {
            s(webRecordParam);
            recordService.insertScoreRecord(webRecordParam);
            return AjaxResult.success("添加评分成功", "add");
        }
    }

    /**
     * 文章分享
     * 
     * @param webRecordParam
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/record/addShareRecord")
    @ResponseBody
    public AjaxResult addShareRecord(@PathVariable("themeName") String themeName, WebRecordParam webRecordParam) {
        s(webRecordParam);
        webRecordParam.setScore(null);
        webRecordParam.setRecordTable(RecordTableEnum.SHARE_RECORD.getValue());
        return AjaxResult.success(recordService.insertShareRecord(webRecordParam));
    }

    /**
     * 添加收藏
     * 
     * @param webRecordParam
     * @return
     */
    @RepeatSubmit(interval = 60000)
    @PostMapping("/{themeName}/hand/record/addCollectRecord")
    @ResponseBody
    public AjaxResult addCollectRecord(@PathVariable("themeName") String themeName, WebRecordParam webRecordParam) {
        s(webRecordParam);
        webRecordParam.setRecordTable(RecordTableEnum.COLLECT_RECORD.getValue());
        webRecordParam.setScore(null);
        webRecordParam.setShareUrl(null);
        String collectVisible = recordService.selectRecordVisible(RecordTableEnum.COLLECT_RECORD,
                webRecordParam.getArticleId(),
                webRecordParam.getCreateBy());
        if ("1".equals(collectVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.HIDE.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("取消收藏成功", "cancel");
        } else if ("0".equals(collectVisible)) {
            webRecordParam.setVisible(CommonVisibleEnums.SHOW.ordinal());
            recordService.updateCmsArticleRecord(webRecordParam);
            return AjaxResult.success("添加收藏成功", "update");
        } else {

            recordService.insertCollectRecord(webRecordParam);
            return AjaxResult.success("添加收藏成功", "add");
        }
    }

    /**
     *
     * 设置系统标识 浏览器
     */
    @RepeatSubmit(interval = 60000)
    private void s(WebRecordParam webRecordParam) {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        String ip = IpUtils.getIpAddr();
        webRecordParam.setIp(ip);
        try {
            webRecordParam.setCreateBy(SecurityUtils.getUsername());
        } catch (Exception e) {
        }
        webRecordParam.setUserSystem(userAgent.getOperatingSystem().getName());
        webRecordParam.setUserBower(userAgent.getBrowser().getName());
    }

}
