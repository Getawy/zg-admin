package com.cms.forum.user.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import com.grace.common.core.domain.model.LoginUser;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.StringUtils;
import com.cms.forum.user.security.service.WebTokenService;

/**
 * token过滤器 验证token有效性
 * 
 * @author grace
 */
@Component
public class WebJwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Autowired
    private WebTokenService tokenService;

    /*
     * 所有请求都会走这里
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        // 静态资源通通放行
        if (request.getRequestURI().startsWith("/static")) {
            chain.doFilter(request, response);
            return;
        }
        // 判断用户分离或者不分离登录
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser) && StringUtils.isNull(SecurityUtils.getAuthentication())) {
            tokenService.verifyToken(loginUser);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser,
                    null, loginUser.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        chain.doFilter(request, response);
    }
}
