package com.cms.forum.user.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.grace.system.service.impl.SysSearchConfigServiceImpl;
import com.oly.cms.common.model.properties.OlyThemeConfigProperties;
import com.cms.forum.user.security.hand.ThemeUtils;

@Controller
public class PersonalController {

    @Autowired
    private SysSearchConfigServiceImpl configService;
    /**
     * 用户个人中心
     * 
     * @param userId
     * @return
     */
    @GetMapping("/{themeName}/user/personal")
    public String personalPage( @PathVariable("themeName") String themeName) {
       return ThemeUtils.returnUserPage(themeName,"user/personal",Boolean.parseBoolean(configService.selectConfigDefaultValue(themeName,OlyThemeConfigProperties.THEME_INBUILT_USER_PAGE)));
    }
}
