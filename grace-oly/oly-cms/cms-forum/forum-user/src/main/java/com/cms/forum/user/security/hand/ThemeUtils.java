package com.cms.forum.user.security.hand;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.grace.common.utils.StringUtils;
import com.oly.cms.common.model.properties.OlyWebConfigProperties;
import com.oly.cms.general.taglib.ConfigTag;

public class ThemeUtils {

    /**
     * 获取当前请求的主题对应的域名。
     * 
     * @param request   HttpServletRequest对象，用于获取当前请求信息。
     * @param configTag 配置标签，包含域名配置信息。
     * @return 返回根据请求URI匹配到的域名配置。
     */
    protected static String getDomain(HttpServletRequest request, ConfigTag configTag) {
        String path = request.getRequestURI();
        // 截取主题名
        return configTag.getKeyDefaultEnum(path.substring(1, path.lastIndexOf("/")), OlyWebConfigProperties.DOMAIN);
    }

    /**
     * 重定向请求到指定地址。
     * 
     * @param request  HttpServletRequest对象，用于获取请求信息。
     * @param response HttpServletResponse对象，用于发送响应。
     * @param domain   需要重定向到的域名。
     * @throws IOException 如果发送重定向时发生IO错误。
     */
    protected static void sendRedirect(HttpServletRequest request, HttpServletResponse response, String domain)
            throws IOException {
        String fot = request.getParameter("forward");
        if (StringUtils.isEmpty(fot)) {
            // 发送重定向响应
            response.sendRedirect(domain);
        } else {
            // 发送重定向响应
            response.sendRedirect(domain + "/" + fot);
        }

    }

    public static String returnUserPage(String themeName, String defaultPage, Boolean inBuilt) {
        if (inBuilt) {
            return defaultPage;
        }
        return themeName + "/" + defaultPage;
    }

}
