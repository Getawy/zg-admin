package com.cms.forum.user.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.core.domain.AjaxResult;
import com.cms.forum.user.service.tadlib.CommentTag;

@CrossOrigin
@RestController
@RequestMapping
public class CommentApiController {

    @Autowired
    private CommentTag commentTag;

    @GetMapping("/{themeName}/api/comment/pageComment")
    public AjaxResult pageComment(@PathVariable("themeName") String themeName, String typeId, int pageNum,
            int pageSize) {
        return AjaxResult.success(commentTag.pageCommentByTypeId(typeId, pageNum, pageSize));
    }

    @GetMapping("/{themeName}/api/comment/parentPage")
    public AjaxResult listCommentOne(@PathVariable("themeName") String themeName, String typeId, int pageNum,
            int pageSize) {
        return AjaxResult.success(commentTag.pageCommentByTypeId(typeId, pageNum, pageSize));
    }

    @GetMapping("/{themeName}/api/comment/childPage")
    public AjaxResult listCommentTwo(@PathVariable("themeName") String themeName, long parentId, int pageNum,
            int pageSize) {
        return AjaxResult.success(commentTag.pageTwoCommentByParentId(parentId, pageNum, pageSize));
    }

}
