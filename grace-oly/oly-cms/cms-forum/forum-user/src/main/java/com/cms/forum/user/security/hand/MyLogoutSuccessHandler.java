package com.cms.forum.user.security.hand;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson2.JSONObject;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.ServletUtils;
import com.oly.cms.general.taglib.ConfigTag;

@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private ConfigTag configTag;

    /**
     * 当登出成功时的处理逻辑。
     * 
     * @param request        HttpServletRequest对象，代表客户端的请求。
     * @param response       HttpServletResponse对象，用于向客户端发送响应。
     * @param authentication 认证信息，代表当前用户的认证状态。
     * @throws IOException      如果发生输入/输出错误。
     * @throws ServletException 如果处理请求时发生Servlet相关异常。
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        if (ServletUtils.isAjaxRequest(request)) {
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.getWriter().write(JSONObject.toJSONString(AjaxResult.success("退出成功")));
            response.getWriter().flush();
        } else {
            ThemeUtils.sendRedirect(request, response, ThemeUtils.getDomain(request, configTag));
        }

    }

}
