package com.cms.forum.user.service.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cms.forum.comment.domain.enums.CommentTypeEnum;
import com.cms.forum.comment.domain.vo.CmsCommentVo;
import com.cms.forum.user.service.impl.HandCommentServiceImpl;
import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.model.support.PageData;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.COMMENT_CACHE_KEY_PREFIX)
public class HandCommentCacheService {

    @Autowired
    private HandCommentServiceImpl commentService;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsCommentVo> listCommentByTypeId(String typeId, int pageNum, int pageSize) {
        return commentService.listCommentByTypeId(typeId, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsCommentVo> listCommentOneByTypeId(String typeId, int pageNum, int pageSize) {
        return commentService.listCommentOneByTypeId(typeId, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsCommentVo> listCommentTwoByParentId(long parentId, int pageNum, int pageSize) {
        return commentService.listCommentTwoByParentId(parentId, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public PageData pageCommentByTypeId(String typeId, int pageNum, int pageSize) {
        return commentService.pageCommentByTypeId(typeId, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public PageData pageOneCommentByTypeId(String typeId, int pageNum, int pageSize) {
        return commentService.pageCommentByTypeId(typeId, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public PageData pageTwoCommentByParentId(long parentId, int pageNum, int pageSize) {
        return commentService.pageTwoCommentByParentId(parentId, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsCommentVo> recentlyComment(int commentType, int pageNum, int pageSize) {
        return commentService.recentlyComment(commentType, pageNum, pageSize);
    }

    public boolean commentSupport(CommentTypeEnum commentTypeEnum) {
        return commentService.commentSupport(commentTypeEnum);
    }

}
