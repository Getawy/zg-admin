package com.cms.forum.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;
import com.oly.cms.common.model.properties.OlyThemeConfigProperties;
import com.cms.forum.user.security.hand.ThemeUtils;


/**
 * 为啥不使用登录控制器
 * 使用内置登陆器可以
 * 直接区分是否是异步登录
 * 然后做相应处理
 */
@Controller
public class LoginController {

    @Autowired
    private SysSearchConfigServiceImpl configService;
    /**
     * 用户登录
     * 系统内置登录页
     * 实际登录通过异步
     * @param forward 跳转页面
     * @param themeName 主题名称
     * @param modelMap
     * @return
     */
    @GetMapping("/{themeName}/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "") String forward,
            @PathVariable("themeName") String themeName,
            ModelMap modelMap) {
        modelMap.put("forward", forward);
        modelMap.put("themeName", themeName);
        return ThemeUtils.returnUserPage(themeName,"user/login",Boolean.parseBoolean( configService.selectConfigDefaultValue(themeName,OlyThemeConfigProperties.THEME_INBUILT_USER_PAGE)));
    }

    

}
