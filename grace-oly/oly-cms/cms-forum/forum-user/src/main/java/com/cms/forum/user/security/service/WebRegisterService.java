package com.cms.forum.user.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.grace.common.constant.CacheConstants;
import com.grace.common.constant.Constants;
import com.grace.common.constant.UserConstants;
import com.grace.common.core.domain.entity.SysUser;
import com.grace.common.core.domain.model.RegisterBody;
import com.grace.common.core.text.Convert;
import com.grace.common.enums.SysConfigGroups;
import com.grace.common.exception.user.CaptchaException;
import com.grace.common.exception.user.CaptchaExpireException;
import com.grace.common.utils.FormatUtil;
import com.grace.common.utils.MessageUtils;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.StringUtils;

import com.grace.redis.utils.RedisCache;
import com.grace.system.service.ISysUserService;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;
import com.oly.cms.general.manager.GeneralAsyncManager;
import com.cms.forum.user.manager.factory.HandAsyncFactory;

/**
 * 注册校验方法
 * 
 * @author grace
 */
@Component
public class WebRegisterService {
    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysSearchConfigServiceImpl configService;

    @Autowired
    private RedisCache redisCache;

    /**
     * 注册
     */
    public String register(RegisterBody registerBody) {
        String msg = "", username = registerBody.getUsername(), email = registerBody.getEmail(),
                password = registerBody.getPassword();
        SysUser sysUser = new SysUser();
        sysUser.setUserName(username);
        sysUser.setEmail(email);

        // 验证码开关
        boolean captchaEnabled = configService.selectCaptchaEnabled(SysConfigGroups.SYS_CONFIG.getValue());
        if (captchaEnabled) {
            validateCaptcha(username, registerBody.getCode(), registerBody.getUuid());
        }
        if (StringUtils.isEmpty(username)) {
            msg = "用户名不能为空";
        } else if (!FormatUtil.isEmail(email)) {
            msg = "邮箱不正确";
        } else if (StringUtils.isEmpty(password)) {
            msg = "用户密码不能为空";
        } else if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH) {
            msg = "账户长度必须在2到20个字符之间";
        } else if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH) {
            msg = "密码长度必须在5到20个字符之间";
        } else if (!userService.checkUserNameUnique(sysUser)) {
            msg = "保存用户'" + username + "'失败，注册账号已存在";
        } else if (!userService.checkEmailUnique(sysUser)) {
            msg = "保存用户'" + email + "'失败，邮箱已存在";
        } else {

            // 设置默认用户类型
            sysUser.setUserType(1);
            Long deptId = Convert.toLong(
                    configService.selectConfigValueByGk(SysConfigGroups.SYS_CONFIG.getValue(),
                            "sys.account.defaultDept"),
                    null);
            Long[] roleIds = Convert.toLongArray(configService
                    .selectConfigValueByGk(SysConfigGroups.SYS_CONFIG.getValue(), "sys.account.defaultRoleIds"));
            Long[] postIds = Convert.toLongArray(configService
                    .selectConfigValueByGk(SysConfigGroups.SYS_CONFIG.getValue(), "sys.account.defaultPostIds"));
            // 设置用户默认部门
            sysUser.setDeptId(deptId);
            // 设置用户默认角色
            sysUser.setRoleIds(roleIds);
            // 设置用户默认岗位
            sysUser.setPostIds(postIds);
            sysUser.setNickName(username);
            sysUser.setPassword(SecurityUtils.encryptPassword(password));
            boolean regFlag = userService.insertUser(sysUser) > 0;
            if (!regFlag) {
                msg = "注册失败,请联系系统管理人员";
            } else {
                GeneralAsyncManager.me().execute(HandAsyncFactory.recordLogininfor(username,"admin", Constants.REGISTER,
                        MessageUtils.message("user.register.success")));
            }
        }
        return msg;
    }

    /**
     * 校验验证码
     * 
     * @param username 用户名
     * @param code     验证码
     * @param uuid     唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid) {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(uuid, "");
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null) {
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha)) {
            throw new CaptchaException();
        }
    }
}
