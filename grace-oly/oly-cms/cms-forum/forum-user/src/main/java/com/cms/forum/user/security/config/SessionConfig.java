package com.cms.forum.user.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

@Configuration
@EnableRedisHttpSession
public class SessionConfig {

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setDomainName("127.0.0.1"); // 设置域名
        serializer.setCookieName("SESSION"); // 设置 cookie 名称
        serializer.setCookiePath("/"); // 设置 cookie 路径
        serializer.setUseHttpOnlyCookie(true); // 是否设置 HttpOnly 属性
        serializer.setUseSecureCookie(false); // 是否设置 Secure 属性
        serializer.setSameSite("Lax"); // 设置 SameSite 属性
        return serializer;
    }
}
