package com.cms.forum.user.service;

import com.oly.cms.common.model.param.WebRecordParam;
import com.cms.forum.user.model.enums.RecordTableEnum;

public interface IHandRecordService {

    int insertLikeRecord(WebRecordParam recordParam);

    int insertNastyRecord(WebRecordParam recordParam);

    int insertScoreRecord(WebRecordParam recordParam);

    int insertShareRecord(WebRecordParam recordParam);

    int insertCollectRecord(WebRecordParam recordParam);

    String selectRecordVisible(RecordTableEnum recordTable, Long articleId, String createBy);

    int updateCmsArticleRecord(WebRecordParam recordParam);
}
