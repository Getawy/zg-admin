package com.cms.forum.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.forum.user.mapper.RecordStatusMapper;

@Service
public class RecordStatusService {
    @Autowired
    private RecordStatusMapper recordStatusMapper;

    public boolean selectArticleCommentStatus(Long articleId) {
        return recordStatusMapper.selectArticleCommentStatus(articleId);
    }

}
