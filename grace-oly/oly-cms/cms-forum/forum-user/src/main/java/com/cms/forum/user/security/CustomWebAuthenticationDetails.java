package com.cms.forum.user.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {
    // 设置getter方法，以便拿到验证码
    private final String validateCode;

    private final String validateUuid;

    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        // 拿页面传来的验证码
        validateCode = request.getParameter("code");
        validateUuid = request.getParameter("uuid");
    }

    public String getValidateCode() {
        return validateCode;
    }

    public String getValidateUuid() {
        return validateUuid;
    }

}
