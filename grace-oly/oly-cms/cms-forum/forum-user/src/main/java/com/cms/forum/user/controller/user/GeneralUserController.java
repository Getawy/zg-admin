package com.cms.forum.user.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.SecurityUtils;
import com.oly.cms.general.taglib.CommonTag;

@Controller
public class GeneralUserController {
    @Autowired
    private CommonTag commonTag;

    /**
     * 判断用户是否登录
     * 
     * @param themeName
     * @param userName
     * @return
     */
    @GetMapping("/{themeName}/user/userIsLogin/{userName}")
    public @ResponseBody AjaxResult isLogin(@PathVariable("themeName") String themeName,
            @PathVariable("userName") String userName) {
        String loginUserName = SecurityUtils.getUsernameNull();
        if (loginUserName == null) {
            return AjaxResult.error("用户未登录");
        } else if (!loginUserName.equals(userName)) {
            return AjaxResult.error("登陆账号不符");
        } else
            return AjaxResult.success("身份验证成功");
    }

    /**
     * 用户查询
     * 
     * @param themeName
     * @param userName
     * @return
     */
    @GetMapping("/{themeName}/user/getUser/{userName}")
    public AjaxResult selectUser(@PathVariable("themeName") String themeName,
            @PathVariable("userName") String userName) {
        return AjaxResult.success(commonTag.getUserVo(userName));
    }
}
