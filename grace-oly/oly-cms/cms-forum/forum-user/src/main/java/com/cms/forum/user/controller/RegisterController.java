package com.cms.forum.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.domain.model.RegisterBody;
import com.grace.common.enums.SysConfigGroups;
import com.grace.common.utils.StringUtils;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;
import com.oly.cms.common.model.properties.OlyThemeConfigProperties;
import com.cms.forum.user.security.hand.ThemeUtils;
import com.cms.forum.user.security.service.WebRegisterService;

@Controller
public class RegisterController extends BaseController {

    @Autowired
    private WebRegisterService registerService;

    @Autowired
    private SysSearchConfigServiceImpl configService;
    
    /**
     * 用户注册
     * 系统内置用户注册页
     * 
     * @param redirect
     * @param themeName
     * @param modelMap
     * @return
     */
    @GetMapping("/{themeName}/register")
    public String registerPage(@RequestParam(required = false, defaultValue = "") String redirect,
            @PathVariable("themeName") String themeName,
            ModelMap modelMap) {
        modelMap.put("redirect", redirect);
        modelMap.put("themeName", themeName);
        return ThemeUtils.returnUserPage(themeName,"user/register",Boolean.parseBoolean( configService.selectConfigDefaultValue(themeName,OlyThemeConfigProperties.THEME_INBUILT_USER_PAGE)));
    }

    /**
     * 用户注册
     * 
     * @param user
     * @return
     */
    @PostMapping("/{themeName}/register")
    @ResponseBody
    public AjaxResult register(@PathVariable("themeName") String themeName, RegisterBody user) {
        if (!("true".equals(configService.selectConfigValueByGk(SysConfigGroups.SYS_CONFIG.getValue(),
                "sys.account.registerUser")))) {
            return AjaxResult.error("当前系统没有开启注册功能！");
        }
        String msg = registerService.register(user);
        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }
    
}
