package com.cms.forum.user.mapper;

public interface RecordStatusMapper {
    boolean selectArticleCommentStatus(Long articleId);
}
