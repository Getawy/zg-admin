package com.cms.forum.web.service;

import com.oly.cms.query.service.IArticleSearchService;

public interface IArticleForumSearchService extends IArticleSearchService {
    /**
     * 查询文章评论状态
     * 
     * @param articleId
     * @return
     */
    boolean selectArticleCommentStatus(Long articleId);

}
