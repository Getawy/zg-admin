package com.cms.forum.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * 多用户前端启动程序
 * 
 * @author zhiGe
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackages = { "com.grace", "com.oly", "com.cms" })
public class ForumWebApplication {
        public static void main(String[] args) {
                SpringApplication.run(ForumWebApplication.class, args);
                System.out.println("ZZZZZZZZZZZZZZZZZZZ        GGGGGGGGGGGGG\n"
                                + "Z:::::::::::::::::Z     GGG::::::::::::G\n"
                                + "Z:::::::::::::::::Z   GG:::::::::::::::G\n"
                                + "Z:::ZZZZZZZZ:::::Z   G:::::GGGGGGGG::::G\n"
                                + "ZZZZZ     Z:::::Z   G:::::G       GGGGGG\n"
                                + "        Z:::::Z    G:::::G              \n"
                                + "   止  Z:::::Z     G:::::G      戈       \n"
                                + "      Z:::::Z      G:::::G    GGGGGGGGGG\n"
                                + "     Z:::::Z       G:::::G    G::::::::G\n"
                                + "    Z:::::Z        G:::::G    GGGGG::::G\n"
                                + "Z:::::Z        Z:::::::::G        G::::G\n"
                                + "ZZZ:::::Z     ZZZZZ G:::::G       G::::G\n"
                                + "Z::::::ZZZZZZZZ:::Z  G:::::GGGGGGGG::::G\n"
                                + "Z:::::::::::::::::Z   GG:::::::::::::::G\n"
                                + "Z:::::::::::::::::Z     GGG::::::GGG:::G\n"
                                + "ZZZZZZZZ  止戈前端服务启动成功  GGGGGGGGG");

        }

}
