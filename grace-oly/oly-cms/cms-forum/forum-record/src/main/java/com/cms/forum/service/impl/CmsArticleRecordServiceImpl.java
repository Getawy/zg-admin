package com.cms.forum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.forum.domain.CmsCollectRecord;
import com.cms.forum.domain.CmsLikeRecord;
import com.cms.forum.domain.CmsNastyRecord;
import com.cms.forum.domain.CmsScoreRecord;
import com.cms.forum.domain.CmsShareRecord;
import com.cms.forum.mapper.CmsArticleRecordMapper;
import com.cms.forum.service.ICmsArticleRecordService;
import com.oly.cms.common.domain.entity.CmsArticleCount;
import com.oly.cms.common.domain.entity.CmsLookRecord;

/**
 * 文章统计Service业务层处理
 * 
 * @author zg
 * @date 2020-04-06
 */
@Service
public class CmsArticleRecordServiceImpl implements ICmsArticleRecordService {

    @Autowired
    private CmsArticleRecordMapper cmsArticleRecordMapper;

    @Override
    public CmsArticleCount selectCmsArticleCountById(Long articleId) {
        return cmsArticleRecordMapper.selectCmsArticleCountById(articleId);
    }

    /**
     * 查询文章统计列表
     * 
     * @param articleCount 文章统计
     * @return 文章统计
     */
    @Override
    public List<CmsArticleCount> listCmsArticleCount(CmsArticleCount articleCount) {
        return cmsArticleRecordMapper.listCmsArticleCount(articleCount);
    }

    @Override
    public List<CmsCollectRecord> listCollectRecord(CmsCollectRecord collectRecord) {
        return cmsArticleRecordMapper.listCollectRecord(collectRecord);
    }

    @Override
    public List<CmsLikeRecord> listLikeRecord(CmsLikeRecord likeRecord) {
        return cmsArticleRecordMapper.listLikeRecord(likeRecord);
    }

    @Override
    public List<CmsLookRecord> listLookRecord(CmsLookRecord lookRecord) {
        return cmsArticleRecordMapper.listLookRecord(lookRecord);
    }

    @Override
    public List<CmsNastyRecord> listNastyRecord(CmsNastyRecord nastyRecord) {
        return cmsArticleRecordMapper.listNastyRecord(nastyRecord);
    }

    @Override
    public List<CmsScoreRecord> listScoreRecord(CmsScoreRecord scoreRecord) {
        return cmsArticleRecordMapper.listScoreRecord(scoreRecord);
    }

    @Override
    public List<CmsShareRecord> listShareRecord(CmsShareRecord shareRecord) {
        return cmsArticleRecordMapper.listShareRecord(shareRecord);
    }

}
