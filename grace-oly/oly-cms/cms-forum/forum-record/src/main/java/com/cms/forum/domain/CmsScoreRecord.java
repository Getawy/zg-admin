package com.cms.forum.domain;

import com.oly.cms.common.domain.BaseRecordEntity;

/**
 * 评分记录表
 */
public class CmsScoreRecord extends BaseRecordEntity {

    private static final long serialVersionUID = -2765810317266397606L;
    private Byte score;

    public Byte getScore() {
        return score;
    }

    public void setScore(Byte score) {
        this.score = score;
    }
}
