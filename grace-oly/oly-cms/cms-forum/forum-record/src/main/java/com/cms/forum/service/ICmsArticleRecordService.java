package com.cms.forum.service;

import java.util.List;

import com.cms.forum.domain.CmsCollectRecord;
import com.cms.forum.domain.CmsLikeRecord;
import com.cms.forum.domain.CmsNastyRecord;
import com.cms.forum.domain.CmsScoreRecord;
import com.cms.forum.domain.CmsShareRecord;
import com.oly.cms.common.domain.entity.CmsArticleCount;
import com.oly.cms.common.domain.entity.CmsLookRecord;

/**
 * 文章统计Service接口
 * 
 * @author zg
 */
public interface ICmsArticleRecordService {

    /**
     * 查询文章统计
     * 
     * @param articleId 文章统计ID
     * @return 文章统计
     */
    CmsArticleCount selectCmsArticleCountById(Long articleId);

    /**
     * 查询文章统计列表
     * 
     * @param articleCount 文章统计
     * @return 文章统计集合
     */
    public List<CmsArticleCount> listCmsArticleCount(CmsArticleCount articleCount);

    public List<CmsCollectRecord> listCollectRecord(CmsCollectRecord collectRecord);

    public List<CmsLikeRecord> listLikeRecord(CmsLikeRecord likeRecord);

    public List<CmsLookRecord> listLookRecord(CmsLookRecord lookRecord);

    public List<CmsNastyRecord> listNastyRecord(CmsNastyRecord nastyRecord);

    public List<CmsScoreRecord> listScoreRecord(CmsScoreRecord scoreRecord);

    public List<CmsShareRecord> listShareRecord(CmsShareRecord shareRecord);
}
