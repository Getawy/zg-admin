package com.cms.forum.mapper;

import java.util.List;

import com.cms.forum.domain.CmsCollectRecord;
import com.cms.forum.domain.CmsLikeRecord;
import com.cms.forum.domain.CmsNastyRecord;
import com.cms.forum.domain.CmsScoreRecord;
import com.cms.forum.domain.CmsShareRecord;
import com.oly.cms.common.domain.entity.CmsArticleCount;
import com.oly.cms.common.domain.entity.CmsLookRecord;

public interface CmsArticleRecordMapper {
  /**
   * 获取文章统计
   * 
   * @param articleId
   * @return
   */
  CmsArticleCount selectCmsArticleCountById(Long articleId);

  /**
   * 查询文章统计列表
   * 
   * @param articleCount 文章统计
   * @return 文章统计集合
   */
  List<CmsArticleCount> listCmsArticleCount(CmsArticleCount articleCount);

  List<CmsCollectRecord> listCollectRecord(CmsCollectRecord collectRecord);

  List<CmsLikeRecord> listLikeRecord(CmsLikeRecord likeRecord);

  List<CmsLookRecord> listLookRecord(CmsLookRecord lookRecord);

  List<CmsNastyRecord> listNastyRecord(CmsNastyRecord nastyRecord);

  List<CmsScoreRecord> listScoreRecord(CmsScoreRecord scoreRecord);

  List<CmsShareRecord> listShareRecord(CmsShareRecord shareRecord);

}
