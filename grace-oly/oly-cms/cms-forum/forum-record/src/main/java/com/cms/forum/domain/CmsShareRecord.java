package com.cms.forum.domain;

import com.oly.cms.common.domain.BaseRecordEntity;

/**
 * 分享记录表
 */
public class CmsShareRecord extends BaseRecordEntity {

    private static final long serialVersionUID = -386764297935005206L;

    // 分享的地址
    private String shareUrl;

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }
}
