package com.oly.cms.seat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oly.cms.common.domain.entity.CmsWeb;
import com.oly.cms.seat.mapper.WebSearchMapper;
import com.oly.cms.seat.service.IWebSearchService;

@Service
public class WebSearchServiceImpl implements IWebSearchService {
    @Autowired
    private WebSearchMapper webSearchMapper;

    /**
     * 查询站点
     * 
     * @param webId 站点主键
     * @return 站点
     */
    @Override
    public CmsWeb selectCmsWebByWebName(String webName) {
        return webSearchMapper.selectCmsWebByWebName(webName);
    }

    /**
     * 查询站点列表
     * 
     * @param cmsWeb 站点
     * @return 站点
     */
    @Override
    public List<CmsWeb> selectCmsWebList(CmsWeb cmsWeb) {
        return webSearchMapper.selectCmsWebList(cmsWeb);
    }

}
