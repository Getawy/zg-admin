package com.oly.cms.seat.mapper;

import java.util.List;

import com.oly.cms.seat.model.param.ArticleSearchParam;
import com.oly.cms.seat.model.vo.WebArticleVo;

/**
 * 文章列表排序 失效性 发布时间 阅读量 点赞量 讨论量 推荐级别
 *
 */
public interface ArticleVoSearchMapper {

    List<WebArticleVo> listWebArticleVo(ArticleSearchParam bb);

    /**
     * 上一条记录
     * 
     * @param bb
     * @return
     */
    WebArticleVo selectPreArticle(ArticleSearchParam bb);

    /**
     * 下一条记录
     * 
     * @param bb
     * @return
     */
    WebArticleVo selectNextArticle(ArticleSearchParam bb);
}
