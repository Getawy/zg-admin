package com.oly.cms.seat.model.vo;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson2.annotation.JSONField;
import com.oly.cms.common.domain.entity.CmsArticle;
import com.oly.cms.common.domain.entity.CmsArticleCount;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.vo.UserVo;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;

/** 所有文章字段 */
public class WebArticleVo extends CmsArticle {

    private static final long serialVersionUID = 653673311404721257L;

    // 关联文章所有分类列表
    private List<CmsCategory> categories;

    private CmsArticleCount articleCount;

    @JSONField(serialize = false)
    private Map<String, List<CmsCategory>> mapCategory;

    private UserVo createUser;

    public Map<String, List<CmsCategory>> getMapCategory() {
        return mapCategory;
    }

    public CmsArticleCount getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(CmsArticleCount articleCount) {
        this.articleCount = articleCount;
    }

    public List<CmsCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<CmsCategory> categories) {
        this.categories = categories;
        this.mapCategory = categories.stream()
                .collect(Collectors
                        .groupingBy(k -> CategoryNodeTypeEnums.valueOf(k.getNodeType()).getFile().toLowerCase()));
    }

    public UserVo getCreateUser() {
        return createUser;
    }

    public void setCreateUser(UserVo createUser) {
        this.createUser = createUser;
    }

}
