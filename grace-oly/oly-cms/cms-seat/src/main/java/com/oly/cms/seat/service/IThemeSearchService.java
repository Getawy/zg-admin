package com.oly.cms.seat.service;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsTheme;

public interface IThemeSearchService {
    /**
     * 查询主题
     * webName->w
     * themeName->t
     * 
     * @param cmsTheme
     * @return 主题
     */
    public CmsTheme selectCmsThemeByWt(CmsTheme cmsTheme);

    /**
     * 查询主题
     * webName
     * themeName
     * 
     * @param cmsTheme
     * @return 主题
     */
    public CmsTheme selectCmsThemeByWt(String webName, String themeName);

    /**
     * 查询主题
     * webName->w
     * themeName->t
     * 
     * @param cmsTheme
     * @return 主题
     */
    public CmsTheme selectCmsThemeByWt(String wt);

    /**
     * 查询主题列表 通过站点名字
     * 
     * @param webName
     * @return 主题集合
     */
    public List<CmsTheme> listCmsTheme(String webName);
}
