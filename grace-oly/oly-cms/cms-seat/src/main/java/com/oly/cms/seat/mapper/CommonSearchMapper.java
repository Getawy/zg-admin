package com.oly.cms.seat.mapper;

import com.oly.cms.common.domain.vo.UserVo;

/**
 * 通用查询
 */
public interface CommonSearchMapper {

    /**
     * @param userName
     * @return
     */
    UserVo selectUserVo(String userName);
}
