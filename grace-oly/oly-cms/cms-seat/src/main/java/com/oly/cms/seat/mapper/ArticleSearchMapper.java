package com.oly.cms.seat.mapper;

import java.util.List;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.seat.model.param.ArticleSearchParam;
import com.oly.cms.seat.model.vo.WebArticleVo;

public interface ArticleSearchMapper {

    /**
     * 获取文章通过ID
     * 
     * @param articleSearchParam
     * @return
     */
    WebArticleVo selectArticleById(ArticleSearchParam articleSearchParam);

    
    /**
     * 获取文章通过Url
     * 
     * @param articleSearchParam
     * @return
     */
    WebArticleVo selectArticleByUrl(ArticleSearchParam articleSearchParam);

    /**
     * 获取文章列表
     * 
     * @param webArticleSearchParam
     * @return
     */
    List<WebArticleVo> listArticleBySearch(ArticleSearchParam webArticleSearchParam);

    /**
     * 获取文章ID列表
     * 
     * @param categoryId 分类ID
     * @return
     */
    List<Long> listArticleIdsByCategoryId(Long categoryId);

    /**
     * 获取文章关联数
     * 
     * @param webArticleSearchParam
     * @return
     */
    int selectArticleNumUnion(ArticleSearchParam webArticleSearchParam);

    /**
     * 获取文章时间线
     * 
     * @param supportCategoryId
     * @return
     */
    List<TimeNum> listArticleTimeNum(long supportCategoryId);

}
