package com.oly.cms.seat.mapper;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsContact;

/**
 * 反馈|建议Mapper接口
 * 
 * @author zg
 * @date 2020-06-15
 */
public interface ContactSearchMapper {
    /**
     * 查询反馈|建议
     * 
     * @param contactId 反馈|建议ID
     * @return 反馈|建议
     */
    CmsContact selectCmsContactById(Long contactId);

    /**
     * 查询反馈|建议列表
     * 
     * @param cmsContact 反馈|建议
     * @return 反馈|建议集合
     */
    List<CmsContact> listCmsContact(CmsContact cmsContact);

}
