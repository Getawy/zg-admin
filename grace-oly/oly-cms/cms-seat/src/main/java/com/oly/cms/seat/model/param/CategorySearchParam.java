package com.oly.cms.seat.model.param;

import com.oly.cms.common.domain.entity.CmsCategory;

public class CategorySearchParam extends CmsCategory {
    private String themeName;

    private Integer nodeTypes[];

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public Integer[] getNodeTypes() {
        return nodeTypes;
    }

    public void setNodeTypes(Integer[] nodeTypes) {
        this.nodeTypes = nodeTypes;
    }

}
