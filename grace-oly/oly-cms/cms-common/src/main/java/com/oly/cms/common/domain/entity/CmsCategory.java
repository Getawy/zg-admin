package com.oly.cms.common.domain.entity;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

import com.oly.cms.common.domain.SeoBaseEntity;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.common.enums.NodeTypeEnums;

/**
 * 分类
 * 
 * @author 止戈
 */
public class CmsCategory extends SeoBaseEntity {
    private static final long serialVersionUID = 1L;
    /* 分类id */
    private Long categoryId;
    /* 分类名 */
    private String categoryName;
    /* 祖级列表 */
    private String ancestors;
    /* 分类图标 */
    private String categoryIcon;
    /* 固定链接 */
    private String categoryUrl;
    /* 父ID */
    private Long parentId;
    /* 节点类型 */
    private Integer nodeType;
    /* 是否是目录节点 */
    private Boolean nodeRoot;
    /* 关联文章数 */
    private Long categoryCount;

    /* 关联文章数 文章通过审核 */
    private Long categoryPassCount;

    private List<CmsCategory> childList;

    public List<CmsCategory> getChildList() {
        return childList;
    }

    public void setChildList(List<CmsCategory> childList2) {
        this.childList = childList2;
    }

    public void setCategoryCount(Long categoryCount) {

        this.categoryCount = categoryCount == null ? 0 : categoryCount;
    }

    public Long getCategoryCount() {
        return categoryCount;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @NotBlank(message = "分类名不能为空")
    @Length(min = 2, max = 8, message = "分类名限制2到8个字符")
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    @Length(max = 128, message = "分类ICO最多限制128个字符")
    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon == null ? null : categoryIcon.trim();
    }

    @NotNull(message = "父节点ID不能为空")
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Length(max = 64, message = "分类链接最多限制64个字符")
    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl == null ? null : categoryUrl.trim();
    }

    @NotNull(message = "节点类型不能为空")
    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
        nodeRoot = CategoryNodeTypeEnums.valueOf(this.getNodeType()).getNodeTypeEnum().ordinal() == NodeTypeEnums.TRUNK
                .ordinal();
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public Long getCategoryPassCount() {
        return categoryPassCount;
    }

    public void setCategoryPassCount(Long categoryPassCount) {
        this.categoryPassCount = categoryPassCount;
    }

    public Boolean getNodeRoot() {
        return nodeRoot;
    }

}
