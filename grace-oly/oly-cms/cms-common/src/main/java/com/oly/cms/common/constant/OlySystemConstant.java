package com.oly.cms.common.constant;

/**
 * 内容配置常量
 * 
 * @author 止戈
 */
public class OlySystemConstant {
    // 默认顶置
    public static int DEFAULT_POST_TOP = 0;

    // 新建主题文件支持类型
    public static String[] THEME_SUPPORT_PREFIX = { "html", "json", "yaml", "text", "js", "css" };
    
    // 本地主题管理前缀  
    public static final String THEME_MANAGE_URL_PREFIX="/manage/theme";

}
