package com.oly.cms.common.domain.entity;

import java.util.Date;

import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.BaseEntity;
import com.grace.common.core.domain.model.JsonModel;

/**
 * 主题对象 cms_theme
 * 
 * @author 止戈
 */
public class CmsTheme extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 站点ID */
    @Excel(name = "站点名字")
    private String webName;

    /** 主题名 */
    @Excel(name = "主题名")
    private String themeName;

    /** 站点别称 */
    @Excel(name = "站点别称")
    private String siteName;

    /** 主题作者 */
    @Excel(name = "主题作者")
    private String themeAuthor;

    /** 主题介绍 */
    @Excel(name = "主题介绍")
    private String themeInfo;

    /** 主题状态 */
    @Excel(name = "主题状态")
    private Integer themeEnabled;

    /** 更新地址 */
    @Excel(name = "更新地址")
    private String themeUpdate;

    /** 主题类型 */
    @Excel(name = "主题类型")
    private Integer themeType;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private JsonModel[] themeTouch;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private JsonModel[] themeUrl;

    /** 主题版本号 */
    @Excel(name = "主题版本号")
    private String themeVersion;

    private Date articleLastInsert;

    /** 支持主分类ID */
    @Excel(name = "分类ID")
    private Long supportCategoryId;

    /** 支持类型 */
    @Excel(name = "支持类型")
    private String supportArticleType;

    public Date getArticleLastInsert() {
        return articleLastInsert;
    }

    public void setArticleLastInsert(Date articleLastInsert) {
        this.articleLastInsert = articleLastInsert;
    }

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeAuthor(String themeAuthor) {
        this.themeAuthor = themeAuthor;
    }

    public String getThemeAuthor() {
        return themeAuthor;
    }

    public void setThemeInfo(String themeInfo) {
        this.themeInfo = themeInfo;
    }

    public String getThemeInfo() {
        return themeInfo;
    }

    public void setThemeEnabled(Integer themeEnabled) {
        this.themeEnabled = themeEnabled;
    }

    public Integer getThemeEnabled() {
        return themeEnabled;
    }

    public void setThemeUpdate(String themeUpdate) {
        this.themeUpdate = themeUpdate;
    }

    public String getThemeUpdate() {
        return themeUpdate;
    }

    public void setThemeTouch(JsonModel[] themeTouch) {
        this.themeTouch = themeTouch;
    }

    public JsonModel[] getThemeTouch() {
        return themeTouch;
    }

    public JsonModel[] getThemeUrl() {
        return themeUrl;
    }

    public void setThemeUrl(JsonModel[] themeUrl) {
        this.themeUrl = themeUrl;
    }

    public void setThemeVersion(String themeVersion) {
        this.themeVersion = themeVersion;
    }

    public String getThemeVersion() {
        return themeVersion;
    }

    public Integer getThemeType() {
        return themeType;
    }

    public void setThemeType(Integer themeType) {
        this.themeType = themeType;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Long getSupportCategoryId() {
        return supportCategoryId;
    }

    public void setSupportCategoryId(Long supportCategoryId) {
        this.supportCategoryId = supportCategoryId;
    }

    public String getSupportArticleType() {
        return supportArticleType;
    }

    public void setSupportArticleType(String supportArticleType) {
        this.supportArticleType = supportArticleType;
    }

}
