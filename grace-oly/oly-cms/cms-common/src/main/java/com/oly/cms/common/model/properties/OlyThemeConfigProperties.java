package com.oly.cms.common.model.properties;

import com.grace.common.enums.SysConfigGroups;
import com.grace.common.properties.PropertyEnum;

public enum OlyThemeConfigProperties implements PropertyEnum {

    // 配置组
    THEME_CONFIG_GROUP(SysConfigGroups.THEME_CONFIG.getValue(), String.class, SysConfigGroups.THEME_CONFIG.getValue()),
    // 获取主题描述前缀
    THEME_DATA_PREFIX("themeData", String.class, "themeData"),
    // 获取主题配置前缀
    THEME_CONFIG_PREFIX("themeConfig", String.class, "themeConfig"),
    // 主题文件说明及配置文件
    THEME_INFO("oly.theme.info", String.class, "setting/theme.yaml"),
    // 主题支持编辑类型后缀
    THEME_FILE_EDIT_SUPPORT("oly.theme.file.edit", String.class, "html|js|css|txt|json|yaml"),
    // 主题是否使用内置用户表单 
    THEME_INBUILT_USER_PAGE("theme.inbuilt.user.page", Boolean.class, "true"),
    // 主题配置表单
    THEME_CONFIG_FORM("oly.theme.info", String.class, "setting/configForm.json");

    private final String value;

    private final Class<?> type;

    private final String defaultValue;

    OlyThemeConfigProperties(String value, Class<?> type, String defaultValue) {
        this.defaultValue = defaultValue;
        if (!PropertyEnum.isSupportedType(type)) {
            throw new IllegalArgumentException("Unsupported web property type: " + type);
        }

        this.value = value;
        this.type = type;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String defaultValue() {
        return defaultValue;
    }

    @Override
    public String getValue() {
        return value;
    }

}
