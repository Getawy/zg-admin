package com.oly.cms.common.model.support;

import java.io.Serializable;
import java.util.List;

public class ThemeTreeNode implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private String id;

    /** 节点父ID */
    private String pId;

    /** 节点名称 */
    private String name;
    
    /** 是否是父节点 */
    private boolean parent;

    private List<ThemeTreeNode> children;

    public List<ThemeTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<ThemeTreeNode> children) {
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

}
