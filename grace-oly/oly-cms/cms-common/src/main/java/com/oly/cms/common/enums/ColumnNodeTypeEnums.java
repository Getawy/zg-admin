package com.oly.cms.common.enums;

/**
 * 栏目类型enum
 */
public enum ColumnNodeTypeEnums {
    //关联站点
    WEB_TRUCK(NodeTypeEnums.TRUNK),
    //关联主题    如果主题没有关联类别  默认为所有文章
    THEME_TRUCK(NodeTypeEnums.TRUNK),
    // 树干节点
    COLUMN_TRUNK(NodeTypeEnums.TRUNK),
    // 站内导航
    INSITE(NodeTypeEnums.LEAF),
    // 站外导航
    OUTSITE(NodeTypeEnums.LEAF);
    private NodeTypeEnums nodeTypeEnum;
    ColumnNodeTypeEnums(NodeTypeEnums nodeTypeEnum){
        this.nodeTypeEnum=nodeTypeEnum;
    }
    public NodeTypeEnums getNodeTypeEnum() {
           return nodeTypeEnum;
    }

    public static ColumnNodeTypeEnums valueOf(int ordinal) {
           return values()[ordinal];
    }
}
