package com.oly.cms.common.enums;

/**
 * 分类类型enum
 */
public enum CategoryNodeTypeEnums {
       // 关联站点
       WEB_TRUCK(NodeTypeEnums.TRUNK, "web"),
       // 关联主题 如果主题没有关联类别 默认为所有文章
       THEME_TRUCK(NodeTypeEnums.TRUNK, "theme"),
       // 类目节点 允许关联文章
       CLASSIFY_TRUCK(NodeTypeEnums.TRUNK, "classify"),
       // 类目类型
       CLASSIFY(NodeTypeEnums.LEAF, "classify"),
       // 标签节点 不允许关联文章
       TAG_TRUCK(NodeTypeEnums.TRUNK, "tag"),
       // 标签
       TAG(NodeTypeEnums.LEAF, "tag"),
       // 轮播节点 不允许关联文章
       CAROUSEL_TRUCK(NodeTypeEnums.TRUNK, "carousel"),
       // 轮播
       CAROUSEL(NodeTypeEnums.LEAF, "carousel"),
       // 推荐节点 不允许关联文章
       ONSHOW_TRUCK(NodeTypeEnums.TRUNK, "show"),
       // 推荐
       ONSHOW(NodeTypeEnums.LEAF, "show"),
       // 标签和分类
       TC(NodeTypeEnums.LEAF, "show"),
       // 默认
       DEFAULT(NodeTypeEnums.LEAF, "default");

       private NodeTypeEnums nodeTypeEnum;
       private String file;

       CategoryNodeTypeEnums(NodeTypeEnums nodeTypeEnum, String file) {
              this.nodeTypeEnum = nodeTypeEnum;
              this.file = file;
       }

       public NodeTypeEnums getNodeTypeEnum() {
              return nodeTypeEnum;
       }

       public String getFile() {
              return file;
       }

       public static CategoryNodeTypeEnums valueOf(int ordinal) {
              return values()[ordinal];
       }

       public static Integer[] getNodeTypes(CategoryNodeTypeEnums enums) {
              Integer types[];
              switch (enums) {
                     case CLASSIFY_TRUCK:
                            types = new Integer[] { CLASSIFY_TRUCK.ordinal() };
                            break;
                     case CLASSIFY:
                            types = new Integer[] { CLASSIFY_TRUCK.ordinal(), CLASSIFY.ordinal() };
                            break;
                     case TAG_TRUCK:
                            types = new Integer[] { TAG_TRUCK.ordinal() };
                            break;
                     case TAG:
                            types = new Integer[] { TAG_TRUCK.ordinal(), TAG.ordinal() };
                            break;
                     case CAROUSEL_TRUCK:
                            types = new Integer[] { CAROUSEL_TRUCK.ordinal() };
                            break;
                     case CAROUSEL:
                            types = new Integer[] { CAROUSEL_TRUCK.ordinal(), CAROUSEL.ordinal() };
                            break;
                     case ONSHOW_TRUCK:
                            types = new Integer[] { ONSHOW.ordinal(), ONSHOW_TRUCK.ordinal() };
                            break;
                     case ONSHOW:
                            types = new Integer[] { ONSHOW.ordinal() };
                            break;
                     case TC:
                            types = new Integer[] { TAG.ordinal(), CLASSIFY_TRUCK.ordinal(), CLASSIFY.ordinal() };
                            break;
                     case DEFAULT:
                            types = null;
                            break;
                     default:
                            types = null;
                            break;
              }
              return types;
       }

}
