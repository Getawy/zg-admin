package com.oly.cms.common.enums;

public enum ArticleVisibleEnums {
    // 隐藏 相当于回收站
    HIDE,
    // 草稿
    DRAFT,
    // 审核
    CHECK,
    // 通过
    PASS,
    // 不通过
    BELOW,
    // 重新编辑
    REEDIT

}
