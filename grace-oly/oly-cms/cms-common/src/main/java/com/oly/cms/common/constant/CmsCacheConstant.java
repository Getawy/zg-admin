package com.oly.cms.common.constant;

/**
 * 内容配置缓存配置前缀
 * 单个 xxx:item(参数)_类名.方法名
 * 列表 xxx:list(参数)_类名.方法名
 * @author 止戈
 */
public class CmsCacheConstant {

    /* 菜单缓存 */
    public final static String MENU_CACHE_KEY_PREFIX = "cms_column_cache:",
            /* 文章缓存 */
            ARTICLE_CACHE_KEY_PREFIX = "cms_article_cache:",
            /**
             * 链接缓存
             */
            LINKS_CACHE_KEY_PREFIX = "cms_link_cache:",
            /**
             * 链接缓存
             */
            THEME_CACHE_KEY_PREFIX = "cms_theme_cache:",
            /**
             * 分类缓存
             */
            CATEGORIES_CACHE_KEY_PREFIX = "cms_category_cache:",
            /**
             * 评论缓存
             */
            COMMENT_CACHE_KEY_PREFIX = "cms_comment_cache:",
            /**
             * 联盟缓存
             */
            UNION_CACHE_KEY_PREFIX = "cms_union_cache:";

}
