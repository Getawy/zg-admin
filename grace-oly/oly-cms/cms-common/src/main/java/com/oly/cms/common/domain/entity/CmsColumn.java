package com.oly.cms.common.domain.entity;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.oly.cms.common.domain.SeoBaseEntity;
import com.oly.cms.common.enums.ColumnNodeTypeEnums;
import com.oly.cms.common.enums.NodeTypeEnums;

/**
 * 导航|栏目
 * 
 * @author 止戈
 */
public class CmsColumn extends SeoBaseEntity {

    private static final long serialVersionUID = 7493265520804374133L;

    /* 栏目ID */
    private Long columnId;

    /* 栏目icon */
    private String columnIcon;

    /* 名称 */
    private String columnName;

    /* 打开方式 */
    private Integer openType;

    /* 打开链接 */
    private String columnUrl;

    /* 父级栏目ID */
    private Long parentId;

    /* 节点类型 */
    private Integer nodeType;

    /* 是否是目录节点 */
    private Boolean nodeRoot;

    /* 祖级列表 */
    private String ancestors;

    private List<CmsColumn> childList;

    public List<CmsColumn> getChildList() {
        return childList;
    }

    public void setChildList(List<CmsColumn> childList) {
        this.childList = childList;
    }

    public Long getColumnId() {
        return columnId;
    }

    public void setColumnId(Long columnId) {
        this.columnId = columnId;
    }

    public String getColumnIcon() {
        return columnIcon;
    }

    public void setColumnIcon(String columnIcon) {
        this.columnIcon = columnIcon;
    }

    @NotBlank(message = "菜单名不能为空")
    @Length(min = 2, max = 8, message = "菜单名限制2到8个字符")
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @NotNull(message = "打开方式不能为空")
    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    @NotNull(message = "父级ID不能为空")
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @NotNull(message = "节点不能为空")
    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
        nodeRoot = ColumnNodeTypeEnums.valueOf(this.getNodeType()).getNodeTypeEnum().ordinal() == NodeTypeEnums.TRUNK
                .ordinal();
    }

    @Length(max = 128, message = "菜单链接最多限制128个字符")
    public String getColumnUrl() {
        return columnUrl;
    }

    public void setColumnUrl(String columnUrl) {
        this.columnUrl = columnUrl == null ? null : columnUrl.trim();
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public Boolean getNodeRoot() {
        return nodeRoot;
    }

}
