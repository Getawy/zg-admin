package com.oly.cms.common.domain;

import java.io.Serializable;
import com.oly.cms.common.enums.SearchVisibleType;

/** 关联状态参数
 * 默认只获取没有隐藏的
 * 
 * 
 */
public class VisibleParam implements Serializable {
    private static final long serialVersionUID = 1L;

    // 唯一关键ID
    private Long id;
    // 状态 默认通过
    private SearchVisibleType useVisible = SearchVisibleType.DEFAULT;
    // 状态
    private Integer visible = 0;
    // 占位预留
    private String hold;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public String getHold() {
        return hold;
    }

    public void setHold(String hold) {
        this.hold = hold;
    }

    public SearchVisibleType getUseVisible() {
        return useVisible;
    }

    public void setUseVisible(SearchVisibleType useVisible) {
        this.useVisible = useVisible;
    }

}
