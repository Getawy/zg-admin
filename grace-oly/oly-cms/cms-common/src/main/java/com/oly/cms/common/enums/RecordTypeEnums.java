package com.oly.cms.common.enums;

public enum RecordTypeEnums {
    ARTICLE,
    CATEGORY,
    LINK,
    COLUMN,
    CONTACT,
    COMMENT,
    CONFIG

}
