package com.oly.cms.common.domain.entity;

import java.io.Serializable;

public class CmsCategoryTheme implements Serializable {
    private static final long serialVersionUID = 1L;

    private String themeName;

    private Long categoryId;

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
