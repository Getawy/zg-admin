package com.oly.cms.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import com.grace.common.enums.GraceServerRoot;
import com.grace.common.utils.StringUtils;

public class ZipUtils {
    /** 缓存大小 */
    public static final int BUFFER_SIZE = 1024;

    /**
     * 压缩成ZIP 方法1
     * 
     * @param templateDir      压缩文件夹路径
     * @param staticDir        压缩文件夹路径
     * @param Dir              压缩文件夹路径
     * @param outDir           压缩文件输出路径
     * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
     *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws RuntimeException 压缩失败会抛出运行时异常
     */
    public static void themeToZip(String templateDir, String staticDir, String outDir, String olyCode,
            boolean KeepDirStructure)
            throws RuntimeException {
        OutputStream out = null;
        long start = System.currentTimeMillis();
        ZipOutputStream zos = null;
        try {
            File outFile = new File(outDir);
            if (!outFile.exists()) {
                Paths.get(outFile.getParent()).toFile().mkdirs();
            } else {

            }
            out = new FileOutputStream(outFile);
            zos = new ZipOutputStream(out);
            File templateFile = new File(templateDir);
            File staticFile = new File(staticDir);
            compress(templateFile, zos, olyCode +"/"+GraceServerRoot.THEME_DIR.getValue()+"/"+GraceServerRoot.THEME_TEMPLATE_DIR.getValue()+ "/" + templateFile.getName(), KeepDirStructure);
            compress(staticFile, zos, olyCode + "/"+GraceServerRoot.THEME_DIR.getValue()+"/"+GraceServerRoot.THEME_TEMPLATE_DIR.getValue()+ "/" + staticFile.getName(), KeepDirStructure);
            long end = System.currentTimeMillis();
            System.out.println("压缩完成，耗时：" + (end - start) + " ms");
        } catch (Exception e) {
            throw new RuntimeException("zip error from ZipUtils", e);
        } finally {
            if (zos != null) {
                try {
                    // 2022-03-25 xuwei 输出流一定要按顺序关闭，先关闭ZipOutputStream 在关闭OutputStream，不然有问题！！！
                    zos.close();
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * 压缩成ZIP 方法1
     * 
     * @param srcDir           压缩文件夹路径
     * @param outDir           压缩文件输出路径
     * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
     *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws RuntimeException 压缩失败会抛出运行时异常
     */
    public static void toZip(String srcDir, String outDir, boolean KeepDirStructure)
            throws RuntimeException {

        OutputStream out = null;
        long start = System.currentTimeMillis();
        ZipOutputStream zos = null;
        try {
            out = new FileOutputStream(new File(outDir));
            zos = new ZipOutputStream(out);
            File sourceFile = new File(srcDir);
            compress(sourceFile, zos, sourceFile.getName(), KeepDirStructure);
            long end = System.currentTimeMillis();
            System.out.println("压缩完成，耗时：" + (end - start) + " ms");
        } catch (Exception e) {
            throw new RuntimeException("zip error from ZipUtils", e);
        } finally {
            if (zos != null) {
                try {
                    // 2022-03-25 xuwei 输出流一定要按顺序关闭，先关闭ZipOutputStream 在关闭OutputStream，不然有问题！！！
                    zos.close();
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * 递归压缩方法
     * 
     * @param sourceFile       源文件
     * @param zos              zip输出流
     * @param name             压缩后的名称
     * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
     *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws Exception
     */
    private static void compress(File sourceFile, ZipOutputStream zos, String name,
            boolean KeepDirStructure) throws Exception {
        byte[] buf = new byte[BUFFER_SIZE];
        if (sourceFile.isFile()) {
            // 向zip输出流中添加一个zip实体，构造器中name为zip实体的文件的名字
            zos.putNextEntry(new ZipEntry(name));
            // copy文件到zip输出流中
            int len;
            FileInputStream in = new FileInputStream(sourceFile);
            while ((len = in.read(buf)) != -1) {
                zos.write(buf, 0, len);
            }
            // Complete the entry
            zos.closeEntry();
            in.close();
        } else {
            File[] listFiles = sourceFile.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                // 需要保留原来的文件结构时,需要对空文件夹进行处理
                if (KeepDirStructure) {
                    // 空文件夹的处理
                    zos.putNextEntry(new ZipEntry(name + "/"));
                    // 没有文件，不需要文件的copy
                    zos.closeEntry();
                }

            } else {
                for (File file : listFiles) {
                    // 判断是否需要保留原来的文件结构
                    if (KeepDirStructure) {
                        // 注意：file.getName()前面需要带上父文件夹的名字加一斜杠,
                        // 不然最后压缩包中就不能保留原来的文件结构,即：所有文件都跑到压缩包根目录下了
                        compress(file, zos, name + "/" + file.getName(), KeepDirStructure);
                    } else {
                        compress(file, zos, file.getName(), KeepDirStructure);
                    }

                }
            }
        }
    }

    /**
     * 解压zip文件
     *
     * @param sourceFile 待解压的zip文件
     * @param descDir    解压后的存放路径
     * @return 解压后的文件夹 D:\解压后问价夹
     * @throws Exception
     **/
    public static String unZipFiles(String sourceFile, String descDir) throws IOException {

        // 解压目录不存在，就创建
        File descDirFile = new File(descDir);
        if (!descDirFile.exists()) {
            descDirFile.mkdirs();
        }
        // 创建zip压缩对象
        ZipFile zip = new ZipFile(sourceFile, Charset.forName("GBK"));
        // 解压后的文件夹 D:\解压后问价夹
        String orgMkdirs = "";
        for (Enumeration entries = zip.entries(); entries.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            String zipEntryName = entry.getName();
            String outPath = (descDir + "/" + zipEntryName).replaceAll("\\*", "/");
            // 判断路径是否存在,不存在则创建文件路径
            File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
            if (!file.exists()) {
                file.mkdirs();
            }
            orgMkdirs = outPath.substring(0, outPath.lastIndexOf('/'));
            // 判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
            if (new File(outPath).isDirectory()) {
                continue;
            }
            try (InputStream in = zip.getInputStream(entry);
                    OutputStream out = new FileOutputStream(outPath)) {
                byte[] buf1 = new byte[1024];
                int len;
                while ((len = in.read(buf1)) > 0) {
                    out.write(buf1, 0, len);
                }
            } catch (IOException e) {
                // log.error("解压失败!");
                // log.error(e.getMessage(),e);
            }
        }
        zip.close();
        return orgMkdirs;
    }

    /**
     * 解压zip 指定文件
     *
     * @param sourceFile 待解压的zip文件
     * @param descDir    解压后的存放路径
     * @param webName    站点名
     * @param themeName  主题名
     * @return 解压后的文件夹 D:\解压后文件文件夹
     * @throws Exception
     **/
    public static String unZipFilesAppoint(String sourceFile, String descDir,String themeName)
            throws IOException {
        // 解压目录不存在，就创建
        File descDirFile = new File(descDir);
        if (!descDirFile.exists()) {
            descDirFile.mkdirs();
        }
        String themeTemplatePrefix = GraceServerRoot.THEME_TEMPLATE_DIR.getValue() +"/"+ themeName+"/";
        String themeStaticPrefix = GraceServerRoot.THEME_STATIC_DIR.getValue() + "/" + themeName+ "/";
        // 创建zip压缩对象
        ZipFile zip = new ZipFile(sourceFile, Charset.forName("GBK"));
        // 解压后的文件夹 
        String orgMkdirs = "";
        for (Enumeration entries = zip.entries(); entries.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            String zipEntryName = entry.getName();
            System.out.println(zipEntryName);
            if (zipEntryName.startsWith(themeStaticPrefix) || zipEntryName.startsWith(themeTemplatePrefix)) {
                String outPath = (descDir + "/" + zipEntryName).replaceAll("\\*", "/");
                // 判断路径是否存在,不存在则创建文件路径
                File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
                if (!file.exists()) {
                    file.mkdirs();
                }
                orgMkdirs = outPath.substring(0, outPath.lastIndexOf('/'));
                // 判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
                if (new File(outPath).isDirectory()) {
                    continue;
                }
                try (InputStream in = zip.getInputStream(entry);
                        OutputStream out = new FileOutputStream(outPath)) {
                    byte[] buf1 = new byte[1024];
                    int len;
                    while ((len = in.read(buf1)) > 0) {
                        out.write(buf1, 0, len);
                    }
                } catch (IOException e) {
                    // log.error("解压失败!");
                    // log.error(e.getMessage(),e);
                }
            }
        }
        zip.close();
        return orgMkdirs;
    }

    /**
     * 递归删除文件及文件夹
     *
     * @throws Exception
     * @author gaoyan
     *         toFolder,解压后的存放路径
     **/
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

    /**
     * 读取一个目录下所有的Excel文件
     * gaoyan
     *
     * @param path
     */
    public static List<File> readExcelFile(String path) {
        File file = new File(path);
        List<File> resultlist = new ArrayList<>();
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File fi : files) {
                // 对文件进行过滤，只读取Excel文件
                if (fi.getName().contains(".xls") || fi.getName().contains(".xlsx")) {
                    resultlist.add(fi);
                }
            }
        }
        return resultlist;
    }

    /**
     * 根据文件名称读取固定目录下文件
     * gaoyan
     * 
     * @param filename
     */
    public static List<File> readAllFileByFilename(String path, String filename) {
        List<File> resultlist = new ArrayList<>();
        if (StringUtils.isNotEmpty(path)) {
            File file = new File(path);
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File fi : files) {
                    // 对文件进行过滤
                    if (fi.getName().equals(filename)) {
                        resultlist.add(fi);
                    }
                }
            }
        }
        return resultlist;
    }

    /**
     * 根据文件名称读取固定目录下文件
     * gaoyan
     * 
     * @param filename
     */
    public static File readFileByFilename(String path, String filename) {

        if (StringUtils.isNotEmpty(path)) {
            File file = new File(path);
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File fi : files) {
                    // 对文件进行过滤
                    if (fi.getName().equals(filename) && fi.isFile()) {
                        return fi;
                    }
                }
            }
        }
        return null;
    }

}
