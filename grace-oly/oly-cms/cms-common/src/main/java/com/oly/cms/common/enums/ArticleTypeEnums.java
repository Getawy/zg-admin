package com.oly.cms.common.enums;

public enum ArticleTypeEnums {
    // 图文
    ARTICLE,
    // 图集
    PHOTO,
    // 音乐
    MUSIC,
    // 视频
    MOVIE,
    // 下载
    DOWNLOAD,
    // 微言
    WEI,

}
