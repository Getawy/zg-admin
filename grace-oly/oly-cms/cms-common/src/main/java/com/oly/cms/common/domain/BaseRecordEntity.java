package com.oly.cms.common.domain;

import com.oly.cms.common.domain.entity.CmsArticle;
import com.grace.common.annotation.Excel;

/**
 * 文章记录操作
 * 赞，踩，收藏
 * 阅读，评分，分享
 * 
 * @author 止戈
 * 
 */
public class BaseRecordEntity extends CmsArticle {

    private Long recordId;

    /** ip */
    @Excel(name = "ip")
    private String ip;

    /** 使用系统 */
    @Excel(name = "使用系统")
    private String userSystem;

    /** 用户浏览器 */
    @Excel(name = "用户浏览器")
    private String userBower;

    /** 主题名 */
    @Excel(name = "主题名")
    private String themeName;

    /** 站点编码 */
    @Excel(name = "站点编码")
    private String onlyCode;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserSystem() {
        return userSystem;
    }

    public void setUserSystem(String userSystem) {
        this.userSystem = userSystem;
    }

    public String getUserBower() {
        return userBower;
    }

    public void setUserBower(String userBower) {
        this.userBower = userBower;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getOnlyCode() {
        return onlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        this.onlyCode = onlyCode;
    }

}
