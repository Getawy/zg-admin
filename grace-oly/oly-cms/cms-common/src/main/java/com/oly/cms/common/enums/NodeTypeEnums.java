package com.oly.cms.common.enums;
/**
 * 节点类型
 */
public enum NodeTypeEnums {
    // 树干节点
    TRUNK,
    // 叶子节点
    LEAF
}
