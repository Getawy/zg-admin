package com.oly.cms.common.model.param;

import com.oly.cms.common.domain.BaseRecordEntity;

/**
 * 记录参数基本
 */
public class WebRecordParam extends BaseRecordEntity {
    private String createBy;
    private Byte score;
    private String shareUrl;
    private String recordTable;

    public String getRecordTable() {
        return recordTable;
    }

    public void setRecordTable(String recordTable) {
        this.recordTable = recordTable;
    }

    public Byte getScore() {
        return score;
    }

    public void setScore(Byte score) {
        this.score = score;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

}
