package com.oly.cms.common.domain;

import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.model.JsonModel;

/**
 * Seo基类
 * 导航Menu
 * 类目Category
 * 文章Article
 * 
 * @author 止戈
 */
public class SeoBaseEntity extends CmsBaseEntity {
    private static final long serialVersionUID = 1L;
    /** 关键词 */
    @Excel(name = "关键词")
    private String keywords;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 页面类型 页面风格选择 */
    @Excel(name = "页面类型")
    private String pageType;

    /** 额外字段 */
    @Excel(name = "额外字段")
    private JsonModel extra[];

    // 支持的主题分类Id
    private Long supportCategoryId = -1L;

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSupportCategoryId() {
        return supportCategoryId;
    }

    public void setSupportCategoryId(Long supportCategoryId) {
        this.supportCategoryId = supportCategoryId;
    }

    public JsonModel[] getExtra() {
        return extra;
    }

    public void setExtra(JsonModel[] extra) {
        this.extra = extra;
    }

}
