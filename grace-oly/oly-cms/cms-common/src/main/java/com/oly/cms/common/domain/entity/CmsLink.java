package com.oly.cms.common.domain.entity;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.grace.common.annotation.Excel;
import com.oly.cms.common.domain.SeoBaseEntity;

import org.hibernate.validator.constraints.Length;

/**
 * 链接
 * 
 * @author 止戈
 */
public class CmsLink extends SeoBaseEntity {
    private static final long serialVersionUID = 1L;

    /** 链接id */
    @Excel(name = "链接Id")
    private Long linkId;

    /** 链接地址 */
    @Excel(name = "链接地址")
    private String linkUrl;

    /** 链接名 */
    @Excel(name = "链接名")
    private String linkName;

    /** 打开方式 */
    @Excel(name = "打开方式")
    private Integer openType;

    /** 链接LOGO */
    @Excel(name = "链接LOGO")
    private String linkIco;

    /** 状态 */
    @Excel(name = "状态")
    private Integer visible;

    /** 节点类型 */
    @Excel(name = "节点类型")
    private String nodeType;

    /** 父节点 */
    @Excel(name = "父节点")
    private Long parentId;

    private List<CmsLink> children;

    public List<CmsLink> getChildren() {
        return children;
    }

    public void setChildren(List<CmsLink> children) {
        this.children = children;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    @NotBlank(message = "链接名不能为空")
    @Length(min = 2, max = 8, message = "链接名长度仅允许2-8个字符")
    public String getLinkName() {
        return linkName;
    }

    @NotNull(message = "打开方式不能为空")
    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public void setLinkIco(String linkIco) {
        this.linkIco = linkIco;
    }

    public String getLinkIco() {
        return linkIco;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeType() {
        return nodeType;
    }

}
