package com.oly.cms.common.domain.vo;

import com.oly.cms.common.domain.entity.CmsTheme;


public class ThemeVo extends CmsTheme {
    private static final long serialVersionUID = 1L;

    private Long themeCategoryId;

    public Long getThemeCategoryId() {
        return themeCategoryId;
    }

    public void setThemeCategoryId(Long themeCategoryId) {
        this.themeCategoryId = themeCategoryId;
    }
  
}
