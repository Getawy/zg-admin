package com.oly.cms.common.model.support;

import java.io.Serializable;
import java.util.List;

import com.github.pagehelper.PageInfo;

/**
 * @author grace
 *
 *         分页数据
 */
public class PageData implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 总记录数 */
    private long total;
    /** 消息状态码 */
    private int code;
    /** 当前页 */
    private long pageNum;
    /** 每页数据量 */
    private long pageSize;
    /** 总页数 */
    private long pages;
    /** 列表数据 */
    private List<?> rows;
    public long getPages() {
        return pages;
    }

    public void setPages(long pages) {
        this.pages = pages;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getPageNum() {
        return pageNum;
    }

    public void setPageNum(long pageNum) {
        this.pageNum = pageNum;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public PageData() {
        super();
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }

    public PageData(long total, int code, long pages, long pageNum, long pageSize, List<?> rows) {
        super();
        this.total = total;
        this.code = code;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.pages = pages;
        this.rows = rows;
    }

    /**
     * 响应请求分页数据 普通分页
     *
     * @return
     */
    public static PageData getData(List<?> list, int code) {
        PageInfo<?> page = new PageInfo(list);
        PageData rspData = new PageData(page.getTotal(), code, page.getPages(), page.getPageNum(), page.getPageSize(),
                list);
        return rspData;
    }

}
