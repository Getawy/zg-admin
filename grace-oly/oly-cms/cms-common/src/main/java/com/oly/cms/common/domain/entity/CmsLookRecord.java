package com.oly.cms.common.domain.entity;

import com.oly.cms.common.domain.BaseRecordEntity;

/**
 * 阅读记录表
 * 
 * @author 止戈
 */
public class CmsLookRecord extends BaseRecordEntity {

    private static final long serialVersionUID = 1L;
}
