package com.oly.cms.common.domain.vo;

import javax.validation.constraints.Size;

import com.oly.cms.common.domain.entity.CmsArticle;

public class ArticleVo extends CmsArticle {
	private static final long serialVersionUID = 1L;

	// 类目分类Ids
	private Long[] categoryIds;

	// 类目标签Ids
	private Long[] tagIds;

	// 用于查询
	private Long categoryId;

	// 主题
	private String syncThemeName;

	public Long[] getTagIds() {
		return tagIds;
	}

	public void setTagIds(Long[] tagIds) {
		this.tagIds = tagIds;
	}

	@Size(min = 1, message = "请至少关联一个分类")
	public Long[] getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(Long[] categoryIds) {
		this.categoryIds = categoryIds;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getSyncThemeName() {
		return syncThemeName;
	}

	public void setSyncThemeName(String syncThemeName) {
		this.syncThemeName = syncThemeName;
	}

}
