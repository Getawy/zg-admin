package com.oly.cms.common.domain.entity;

import java.util.Date;

import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.BaseEntity;

/**
 * 内容请求记录对象 cms_log_record
 * 
 * @author 止戈
 */
public class CmsLogRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    private Long logId;

    /** 模块标题 */
    @Excel(name = "模块标题")
    private String title;

    /** 业务类型（0其它 1新增 2修改 3删除） */
    @Excel(name = "业务类型", readConverterExp = "0=其它,1=新增,2=修改,3=删除")
    private Integer businessType;

    /** 方法名称 */
    @Excel(name = "方法名称")
    private String method;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String operName;

    /** 请求方式 */
    @Excel(name = "请求方式")
    private String requestMethod;

    /** 操作类别（0其它 1用户） */
    @Excel(name = "操作类别", readConverterExp = "0=其它,1=用户")
    private Integer operatorType;

    /** 记录类型 */
    @Excel(name = "记录类型")
    private Integer logType;

    /** 请求URL */
    @Excel(name = "请求URL")
    private String operUrl;

    /** 主机地址 */
    @Excel(name = "主机地址")
    private String operIp;

    /** 操作地点 */
    @Excel(name = "操作地点")
    private String operLocation;

    /** 请求参数 */
    @Excel(name = "请求参数")
    private String operParam;

    /** 返回参数 */
    @Excel(name = "返回参数")
    private String jsonResult;

    /** 操作状态（0正常 1异常） */
    @Excel(name = "操作状态", readConverterExp = "0=正常,1=异常")
    private Integer status;

    /** 错误消息 */
    @Excel(name = "错误消息")
    private String errorMsg;

    /** 操作时间 */
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operTime;

    /** 使用浏览器 */
    @Excel(name = "使用浏览器")
    private String operBower;

    /** 使用系统 */
    @Excel(name = "使用系统")
    private String operSystem;

    /** 消耗时间 */
    @Excel(name = "消耗时间", suffix = "毫秒")
    private Long costTime;

    /** 主题名 */
    @Excel(name = "主题名")
    private String themeName;

    /** 站点编码 */
    @Excel(name = "站点编码")
    private String onlyCode;

    public Long getCostTime() {
        return costTime;
    }

    public void setCostTime(Long costTime) {
        this.costTime = costTime;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getLogId() {
        return logId;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Integer getLogType() {
        return logType;
    }

    public void setLogType(Integer logType) {
        this.logType = logType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setOperatorType(Integer operatorType) {
        this.operatorType = operatorType;
    }

    public Integer getOperatorType() {
        return operatorType;
    }

    public void setOperUrl(String operUrl) {
        this.operUrl = operUrl;
    }

    public String getOperUrl() {
        return operUrl;
    }

    public void setOperIp(String operIp) {
        this.operIp = operIp;
    }

    public String getOperIp() {
        return operIp;
    }

    public void setOperLocation(String operLocation) {
        this.operLocation = operLocation;
    }

    public String getOperLocation() {
        return operLocation;
    }

    public void setOperParam(String operParam) {
        this.operParam = operParam;
    }

    public String getOperParam() {
        return operParam;
    }

    public void setJsonResult(String jsonResult) {
        this.jsonResult = jsonResult;
    }

    public String getJsonResult() {
        return jsonResult;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperBower(String operBower) {
        this.operBower = operBower;
    }

    public String getOperBower() {
        return operBower;
    }

    public void setOperSystem(String operSystem) {
        this.operSystem = operSystem;
    }

    public String getOperSystem() {
        return operSystem;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getOnlyCode() {
        return onlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        this.onlyCode = onlyCode;
    }

}
