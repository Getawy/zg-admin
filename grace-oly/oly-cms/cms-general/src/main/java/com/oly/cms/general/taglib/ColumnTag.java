package com.oly.cms.general.taglib;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsColumn;
import com.oly.cms.general.cache.GeneralColumnCacheService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 菜单标签
 */
@Service("columnTag")
public class ColumnTag {
    @Autowired
    private GeneralColumnCacheService cmsColumnService;

    /**
     * 不涉及分页（树形菜单）
     * 
     * @param columnId
     * @return
     */
    public CmsColumn treeCmsColumnById(long columnId) {
        return cmsColumnService.treeCmsColumnById(columnId);
    }

    /**
     * 通过id获取导航信息
     * 
     * @param columnId
     * @return
     */
    public CmsColumn selectCmsColumnByIdItem(long columnId) {
        return cmsColumnService.selectCmsColumnByIdItem(columnId);
    }

    /**
     * 通过id获取导航列表
     * 
     * @param columnId
     * @return
     */
    public List<CmsColumn> listCmsColumnById(long columnId) {
        return cmsColumnService.listCmsColumnsById(columnId);
    }

    /**
     * 获取所有菜单列表
     * 
     * @return
     */
    public List<CmsColumn> listCmsColumnAll() {
        return cmsColumnService.listCmsColumnsById(0L);
    }

    /**
     * 获取所有菜单列表
     * 
     * @return
     */
    public List<CmsColumn> listCmsColumn(CmsColumn cmsColumn) {
        return cmsColumnService.listCmsColumn(cmsColumn);
    }

}
