package com.oly.cms.general.taglib;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.grace.common.config.GraceConfig;
import com.grace.common.properties.PropertyEnum;
import com.grace.common.utils.StringUtils;
import com.grace.system.domain.SysConfig;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

@Service("configTag")
public class ConfigTag {
    @Autowired
    private SysSearchConfigServiceImpl configService;

    /**
     * 根据Gk查询参数配置组
     * 
     * @param configGroup 参数组
     * @param configKey   参数键名
     * @return 参数键值
     */
    public String getKey(String configGroup, String configKey) {
        return configService.selectConfigValueByGk(getConfigName(configGroup), configKey);
    }

    /**
     * 根据Gk查询参数配置组
     * 
     * @param configGroup 参数组
     * @param configKey   参数键名
     * @param def         默认值
     * @return 参数键值
     */
    public String getKeyDefault(String configGroup, String configKey, String def) {
        String v = configService.selectConfigValueByGk(getConfigName(configGroup), configKey);
        return StringUtils.isEmpty(v) ? def : v;
    }

    public String getKeyDefaultEnum(String configGroup, PropertyEnum propertyEnum) {
        return configService.selectConfigDefaultValue(getConfigName(configGroup), propertyEnum);
    }

    /**
     * 查询参数配置列表转Map
     * 
     * @param configGroup 参数配置组
     * @return 参数配置集合
     */
    public Map<String, SysConfig> getConfigMap(String configGroup) {
        return configService.selectConfigMapByGroupName(getConfigName(configGroup));
    }

    /**
     * 查询参数配置列表转Map
     * 
     * @param configGroup 参数配置组
     * @return 参数配置集合
     */
    public Map<String, String> getConfigValueMap(String configGroup) {
        return configService.selectConfigValueMapByGroupName(getConfigName(configGroup));
    }

    /**
     * 获取分组列表
     * 
     * 
     * @return
     */
    public List<String> getConfigGroups() {
        return configService.selectConfigGroupList();
    }

    /**
     * 获取真实的分组名
     * 
     * @param configGroup
     * @return
     */
    private String getConfigName(String configGroup) {

        return GraceConfig.getOnlyCode() + "_" + configGroup;
    }
}
