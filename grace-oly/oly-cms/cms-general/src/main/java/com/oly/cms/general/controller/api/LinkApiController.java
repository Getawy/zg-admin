package com.oly.cms.general.controller.api;

import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.oly.cms.general.cache.GeneralLinkCacheService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping
public class LinkApiController extends BaseController {

  @Autowired
  private GeneralLinkCacheService linkService;

  /**
   * 综合查询
   * 
   * @param cmsLink
   * @return
   */
  @GetMapping("/{themeName}/api/link/list/{parentId}/{nodeType}")
  public AjaxResult list(@PathVariable("parentId") Long parentId, @PathVariable("nodeType") String nodeType) {

    return AjaxResult.success(linkService.listCmsLinks(parentId, nodeType));
  }

  /**
   * 获取链接列表
   * 
   * @param groupName
   * @return
   */
  @GetMapping("/{themeName}/api/link/tree/{parentId}/{nodeType}")
  public AjaxResult listLinkByKey(@PathVariable("parentId") Long parentId, @PathVariable("nodeType") String nodeType) {
    return AjaxResult.success(linkService.treeCmsLinks(parentId, nodeType));
  }

  /**
   * 获取链接通过ID
   * 
   * @param linkId
   * @return
   */
  @GetMapping("/{themeName}/api/link/get/{linkId}")
  public AjaxResult getLinkById(@PathVariable("linkId") Long linkId) {
    return AjaxResult.success(linkService.selectCmsLinkByIdItem(linkId));
  }

}
