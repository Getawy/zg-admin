package com.oly.cms.general.cache;

import java.util.List;

import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.general.service.search.GeneralArticleServiceImpl;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.ARTICLE_CACHE_KEY_PREFIX)
public class GeneralArticleCacheService {

    @Autowired
    private GeneralArticleServiceImpl articleServiceImpl;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public WebArticleVo selectWebArticleByIdItem(Long articleId, String themeName, Boolean useMd, Boolean useContent) {

        return articleServiceImpl.selectArticleById(articleId, themeName, useMd, useContent);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public WebArticleVo selectWebArticleByUrlItem(String articleUrl, String themeName, Boolean useMd,
            Boolean useContent) {

        return articleServiceImpl.selectArticleByUrl(articleUrl, themeName, useMd, useContent);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public PageData selectPageData(String themeName, Integer articleType, Long categoryId, int num,
            int size, String order) {
        return articleServiceImpl.selectPageData(themeName, articleType, categoryId, num, size, order);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public PageArticleTimeLine groupByTime(int pageNum, int pageSize, String themeName, String crTime) {
        return articleServiceImpl.groupByTime(themeName, crTime, pageNum, pageSize);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public int selectArticleNum(String themeName, ArticleTypeEnums articleTypeEnums) {
        return articleServiceImpl.selectArticleNum(themeName, articleTypeEnums);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public int selectArticleNum(String themeName) {
        return articleServiceImpl.selectArticleNum(themeName);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<TimeNum> listArticleTimeNum(String themeName, int pageNum, int pageSize) {
        return articleServiceImpl.listArticleTimeNum(themeName, pageNum, pageSize);
    }

    /**
     * 综合查询
     * 
     * @param bb
     * @return
     */
    public List<WebArticleVo> listWebArticles(ArticleSearchParam bb) {

        return articleServiceImpl.listArticleBySearch(bb);
    }

}
