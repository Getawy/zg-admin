package com.oly.cms.general.taglib;

import java.util.List;
import com.oly.cms.common.domain.entity.CmsLink;
import com.oly.cms.general.cache.GeneralLinkCacheService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("linkTag")
public class LinkTag {
  @Autowired
  private GeneralLinkCacheService cmsLinkService;

  /**
   * 获取链接列表
   * 
   * @param parentId 父亲节点
   * @param nodeType 节点类型
   * @return
   */
  public List<CmsLink> listLink(long parentId, String nodeType) {
    return cmsLinkService.listCmsLinks(parentId, nodeType);
  }

  /**
   * 获取链接通过ID
   * 
   * @param linkId
   * @return
   */
  public CmsLink selectCmsLinkByIdItem(long linkId) {
    return cmsLinkService.selectCmsLinkByIdItem(linkId);
  }

  /**
   * 
   * @param parentId
   * @param nodeType
   * @return
   */
  public List<CmsLink> linkTree(long parentId, String nodeType) {
    return cmsLinkService.treeCmsLinks(parentId, nodeType);
  }

  public List<CmsLink> listLinkOrderByParentId(Long parentId, Long orderNum) {

    return cmsLinkService.hotCmsLinksByParentId(parentId, orderNum);
  }

  public List<CmsLink> listLinkOrderByNodeType(String nodeType, Long orderNum) {

    return cmsLinkService.hotCmsLinksByNodeType(nodeType, orderNum);
  }

}
