package com.oly.cms.general.constant;

/**
 * 基本页面配置浅醉前缀
 *
 */
public class BaseWebPagePrefix {
    public static final String INDEX_PAGE = "";
    public static final String ARTICLES_PAGES = "/article";
    public static final String ARTICLE_PAGE = "/article/";
    public static final String CATEGORIES_PAGES = "/category";
    public static final String CATEGORY_PAGE = "/category/";
    public static final String ABOUT_PAGE = "/about";
    public static final String RANK_PAGE = "/rank";
    public static final String LINKS_PAGE = "/link";
    public static final String LINK_PAGE = "/link/";
    public static final String CONTACT_PAGE = "/contact";
    public static final String TIME_LINE_PAGE = "/timeLine";  
}
