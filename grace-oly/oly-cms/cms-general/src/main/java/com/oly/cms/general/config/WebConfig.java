package com.oly.cms.general.config;

import com.grace.common.enums.GraceServerRoot;
import com.grace.common.enums.GraceStorageRoot;
import com.oly.cms.general.interceptor.IPInterceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

        @Autowired
        private IPInterceptor ipInterceptor;

        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
                String path = ("file:"
                                + GraceServerRoot.THEME_DIR.getWorkRoot(GraceServerRoot.THEME_STATIC_DIR.getValue())
                                + "/").replace("\\", "/");
                /** 主题静态资源配置 */
                registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/")
                                .addResourceLocations(path);
                /** 文件资源映射 */
                registry.addResourceHandler("/server/oss/download/**").addResourceLocations(
                                ("file:" + GraceStorageRoot.UPLOAD_DIR.getWorkRoot() + "/").replace("\\", "/"));
                /** 头像资源 */
                registry.addResourceHandler("/server/oss/download/" + GraceStorageRoot.AVATAR_DIR.getValue() + "/**")
                                .addResourceLocations(
                                                ("file:" + GraceStorageRoot.AVATAR_DIR.getWorkRoot() + "/").replace(
                                                                "\\",
                                                                "/"));
        }

        /**
         * 自定义拦截规则
         * 主要用于拦截非指定IP对本地文件的操作
         */
        @Override
        public void addInterceptors(InterceptorRegistry registry) {
                // 指定某些管理IP才能访问
                registry.addInterceptor(ipInterceptor).addPathPatterns("/web/theme/**");
        }

}