package com.oly.cms.general.service.search;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.general.cache.GeneralThemeCacheService;
import com.oly.cms.general.service.IGeneralSearchService;
import com.oly.cms.query.mapper.ArticleVoSearchMapper;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.grace.common.config.GraceConfig;

/**
 * 文章排序相关
 */
@Service
public class GeneralArticleVoServiceImpl implements IGeneralSearchService {

    @Autowired
    private ArticleVoSearchMapper webArticleSortMapper;
    @Autowired
    private GeneralThemeCacheService themeService;

    public List<WebArticleVo> listArticleVo(ArticleSearchParam bb) {

        return webArticleSortMapper.listWebArticleVo(bb);
    }

    public List<WebArticleVo> listArticleVoOrder(int num, int size, ArticleSearchParam bb,
            String order) {
        PageHelper.startPage(num, size, order);
        return webArticleSortMapper.listWebArticleVo(bb);
    }

    public List<WebArticleVo> listArticleVoOrder(int num, int size, Integer articleType, Long categoryId,
            String themeName,
            String order) {
        ArticleSearchParam bb = getArticleSearchParam(themeName);
        ;
        bb.setArticleType(articleType);
        bb.setCategoryId(categoryId);
        return this.listArticleVoOrder(num, size, bb, order);
    }

    public WebArticleVo selectPreArticle(Long articleId, String themeName) {
        ArticleSearchParam bb = getArticleSearchParam(themeName);
        bb.setArticleId(articleId);
        return webArticleSortMapper.selectPreArticle(bb);
    }

    public WebArticleVo selectNextArticle(Long articleId, String themeName) {
        ArticleSearchParam bb = getArticleSearchParam(themeName);
        bb.setArticleId(articleId);
        return webArticleSortMapper.selectNextArticle(bb);
    }

    private ArticleSearchParam getArticleSearchParam(String themeName) {
        ArticleSearchParam articleSearchParam = new ArticleSearchParam();
        CmsTheme cmsTheme = themeService.selectCmsThemeItem(GraceConfig.getOnlyCode(), themeName);
        articleSearchParam.setSupportCategoryId(cmsTheme.getSupportCategoryId());
        articleSearchParam.setSupportArticleType(cmsTheme.getSupportArticleType());
        return articleSearchParam;
    }

}
