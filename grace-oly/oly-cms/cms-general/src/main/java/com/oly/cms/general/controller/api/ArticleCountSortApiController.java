package com.oly.cms.general.controller.api;

import java.util.List;

import com.oly.cms.common.enums.OrderEnums;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.cache.GeneralThemeCacheService;
import com.oly.cms.general.model.enums.ArticleCountSortEnum;
import com.oly.cms.general.taglib.ArticleCountSortTag;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping
public class ArticleCountSortApiController extends BaseController {

    @Autowired
    private ArticleCountSortTag articleCountService;

    @Autowired
    private GeneralThemeCacheService themeService;

    @GetMapping("/{themeName}/api/articleSort/list")
    public AjaxResult list(@PathVariable("themeName") String themeName,
            ArticleSearchParam bb) {
        bb.setSupportCategoryId(themeService.selectCmsThemeItem(themeName).getSupportCategoryId());
        startPage();
        List<WebArticleVo> list = articleCountService.listArticleVo(bb);

        return AjaxResult.success(PageData.getData(list, 200));
    }

    @GetMapping("/{themeName}/api/articleSort/getPreAndNextArticle/{articleId}")
    public AjaxResult selectPreAndNextArticle(@PathVariable(value = "themeName") String themeName,
            @PathVariable("articleId") long articleId) {
        return AjaxResult.success(articleCountService.selectPreAndNextArticle(articleId, themeName));
    }

    @GetMapping("/{themeName}/api/articleSort/getWebArticleOrderByCategoryId/{categoryId}/{pageNum}/{pageSize}/{sort}/{order}")
    public AjaxResult selectWebArticleOrderByCategoryId(@PathVariable(value = "themeName") String themeName,
            @PathVariable("categoryId") long categoryId,
            @PathVariable("pageNum") Integer pageNum,
            @PathVariable("pageSize") Integer pageSize,
            @PathVariable("sort") ArticleCountSortEnum sort,
            @PathVariable("order") OrderEnums order) {
        return AjaxResult
                .success(articleCountService.listWebArticleOrderByCategoryId(themeName, categoryId, pageNum, pageSize,
                        sort, order));
    }

}
