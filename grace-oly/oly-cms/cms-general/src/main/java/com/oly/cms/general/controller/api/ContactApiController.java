package com.oly.cms.general.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;

import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.general.service.IGeneralContactService;

@CrossOrigin
@RestController
@RequestMapping
public class ContactApiController extends BaseController {
    @Autowired
    private IGeneralContactService cmsContactService;

    /**
     * 获取反馈
     * 
     * @param contactId
     * @return
     */
    @GetMapping("/{themeName}/api/contact/getContact/{contactId}")
    public AjaxResult getContact(@PathVariable("themeName") String themeName,
            @PathVariable("contactId") Long contactId) {

        return success(cmsContactService.selectCmsContactById(contactId));
    }

    /**
     * 反馈列表
     * 
     * @param cmsContact
     * @return
     */
    @GetMapping("/{themeName}/api/contact/listContact")
    public AjaxResult listContacts(@PathVariable("themeName") String themeName, CmsContact cmsContact) {
        return success(cmsContactService.listCmsContact(cmsContact));
    }
}
