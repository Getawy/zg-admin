package com.oly.cms.general.model.enums;

/**
 * 内容操作类型
 *
 * 页面
 * 评论
 * 反馈
 */
public enum WebBusinessType {
    
     /**
     * 查询|请求
     */
    SELECT,
    /**
     * 新增
     */
    INSERT,
    /**
     * 修改
     */
    UPDATE,
    /**
     * 删除 取消
     */
    DELETE,
    /**
     * 其它
     */
    OTHER,

}
