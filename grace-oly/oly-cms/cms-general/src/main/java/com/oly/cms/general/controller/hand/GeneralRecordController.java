package com.oly.cms.general.controller.hand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oly.cms.common.domain.entity.CmsLookRecord;
import com.oly.cms.general.annotation.WebLog;
import com.oly.cms.general.model.enums.WebLogType;
import com.oly.cms.general.service.impl.GeneralRecordServiceImpl;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.ServletUtils;
import com.grace.common.utils.ip.IpUtils;

import eu.bitwalker.useragentutils.UserAgent;

@Controller
@RequestMapping
public class GeneralRecordController {
    @Autowired
    private GeneralRecordServiceImpl recordService;

    /**
     * 文章点击记录
     * 
     * @param cmsLookRecord
     * @return
     */
    @PostMapping("/{themeName}/general/record/addLookRecord")
    @WebLog(title = "主页请求", logType = WebLogType.PAGE)
    @ResponseBody
    public AjaxResult addLookRecord(@PathVariable("themeName") String themeName, CmsLookRecord cmsLookRecord) {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        String ip = IpUtils.getIpAddr();
        cmsLookRecord.setIp(ip);
        cmsLookRecord.setCreateBy(SecurityUtils.getUsernameNull());
        cmsLookRecord.setUserSystem(userAgent.getOperatingSystem().getName());
        cmsLookRecord.setUserBower(userAgent.getBrowser().getName());
        return AjaxResult.success(recordService.insertCmsLookRecord(cmsLookRecord));
    }

}
