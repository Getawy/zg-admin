package com.oly.cms.general.service;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.general.mapper.GeneralContactMapper;

/**
 * 反馈|建议Service接口
 * 
 * @author zg
 */
public interface IGeneralContactService extends GeneralContactMapper {
    /**
     * 查询反馈|建议
     * 
     * @param contactId 反馈|建议ID
     * @return 反馈|建议
     */
    public CmsContact selectCmsContactById(Long contactId);

    /**
     * 查询反馈|建议列表
     * 
     * @param cmsContact 反馈|建议
     * @return 反馈|建议集合
     */
    public List<CmsContact> listCmsContact(CmsContact cmsContact);

}
