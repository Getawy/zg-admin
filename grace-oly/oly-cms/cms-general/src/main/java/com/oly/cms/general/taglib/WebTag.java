package com.oly.cms.general.taglib;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grace.common.config.GraceConfig;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.domain.entity.CmsWeb;
import com.oly.cms.general.cache.GeneralThemeCacheService;

@Service("webTag")
public class WebTag {

    @Autowired
    private GeneralThemeCacheService themeSearchService;

    public CmsTheme selectCmsThemeByThemeName(String themeName) {
        return themeSearchService.selectCmsThemeItem(GraceConfig.getOnlyCode(), themeName);
    }

    public Map<String, CmsTheme> mapTheme() {
        return themeSearchService.mapTheme(GraceConfig.getOnlyCode());
    }

    public CmsWeb selectCmsWebByWebName(String webName) {
        return themeSearchService.selectCmsWebItem(webName);

    }

}
