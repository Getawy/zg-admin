package com.oly.cms.general.taglib;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oly.cms.common.domain.entity.CmsUnion;
import com.oly.cms.common.enums.OrderEnums;
import com.oly.cms.common.enums.UnionSortEnums;
import com.oly.cms.general.cache.GeneralUnionCacheService;

@Service("unionTag")
public class UnionTag {
    @Autowired
    private GeneralUnionCacheService unionCacheService;

    public CmsUnion getCmsUnionByShopId(String shopId) {
        return unionCacheService.selectCmsUnionByShopIdItem(shopId);
    }

    public List<CmsUnion> listCmsUnions(String unionType, int pageNum, int pageSize) {
        return unionCacheService.listCmsUnions(unionType, pageNum, pageSize,
                UnionSortEnums.SHOP_PRICE.getOrder(OrderEnums.DESC));
    }

}
