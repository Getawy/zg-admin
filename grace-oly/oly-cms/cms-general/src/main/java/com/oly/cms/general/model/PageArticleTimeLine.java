package com.oly.cms.general.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson2.annotation.JSONField;
import com.github.pagehelper.PageInfo;
import com.grace.common.utils.DateUtils;
import com.oly.cms.query.model.vo.WebArticleVo;

/**
 * 时间线配置
 */
public class PageArticleTimeLine implements Serializable {

    private static final long serialVersionUID = 653673311404721257L;
    /** 总记录数 */
    private long total;
    /** 消息状态码 */
    private int code;
    /** 当前页 */
    private long pageNum;
    /** 每页数据量 */
    private long pageSize;
    /** 总页数 */
    private long pages;

    @JSONField(serialize = false)
    private Map<String, List<WebArticleVo>> map;

    private List<WebArticleVo> listArticleVo;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getPageNum() {
        return pageNum;
    }

    public void setPageNum(long pageNum) {
        this.pageNum = pageNum;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getPages() {
        return pages;
    }

    public void setPages(long pages) {
        this.pages = pages;
    }

    public Map<String, List<WebArticleVo>> getMap() {
        return map;
    }

    public void setMap(Map<String, List<WebArticleVo>> map) {
        this.map = map;
    }

    public List<WebArticleVo> getListArticleVo() {
        return listArticleVo;
    }

    public void setListArticleVo(List<WebArticleVo> listArticleVo) {
        this.listArticleVo = listArticleVo;
        this.map = listArticleVo.stream()
                .collect(Collectors.groupingBy(webArticle -> DateUtils.neData(webArticle.getCreateTime())));
    }

    /**
     * 响应请求分页数据 普通分页
     *
     * @return
     */
    public static PageArticleTimeLine getData(List<WebArticleVo> list) {
        PageArticleTimeLine rspData = new PageArticleTimeLine();
        PageInfo<?> page = new PageInfo<>(list);
        rspData.setCode(0);
        rspData.setListArticleVo(list);
        rspData.setTotal(page.getTotal());
        rspData.setPageNum(page.getPageNum());
        rspData.setPageSize(page.getPageSize());
        rspData.setPages(page.getPages());
        return rspData;
    }
}
