package com.oly.cms.general.model.enums;

public enum WebLogType {
    /** 页面日志 */
    PAGE,
    /** 文章日志 */
    ARTICLE,
    /** 评论日志 */
    COMMENT,
    /** 反馈日志 */
    CONTACT,
    /** 链接日志 */
    LINK,
    /** 分类日志 */
    CATEFORY,
    /** 其它日志 */
    OTHER
}
