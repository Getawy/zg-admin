package com.oly.cms.general.controller.api;

import com.oly.cms.common.domain.entity.CmsColumn;
import com.oly.cms.general.taglib.ColumnTag;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping
public class ColumnApiController extends BaseController {

  @Autowired
  private ColumnTag columnService;

  /**
   * 不涉及分页（树形菜单）
   * 
   * @param id
   * @return
   */
  @GetMapping("/{themeName}/api/column/tree/{id}")
  public AjaxResult treeCmsColumnById(@PathVariable("id") Long id) {
    return AjaxResult.success(columnService.treeCmsColumnById(id));
  }

  /**
   * 通过id获取导航列表
   * 
   * @param columnId
   * @return
   */
  @GetMapping("/{themeName}/api/column/get/{columnId}")
  public AjaxResult getCmsColumnId(@PathVariable("columnId") Long columnId) {
    return AjaxResult.success(columnService.selectCmsColumnByIdItem(columnId));
  }

  /**
   * 通过Id获取导航列表
   * 
   * @param columnId
   * @return
   */
  @GetMapping("/{themeName}/api/column/list/{columnId}")
  public AjaxResult listCmsColumnById(@PathVariable("columnId") Long columnId) {
    return AjaxResult.success(columnService.listCmsColumnById(columnId));
  }

  /**
   * 综合查询
   * 
   * @param cmsColumn
   * @return
   */
  @GetMapping("/{themeName}/api/column/list")
  public AjaxResult listCmsColumn(CmsColumn cmsColumn) {
    return AjaxResult.success(columnService.listCmsColumn(cmsColumn));
  }

}
