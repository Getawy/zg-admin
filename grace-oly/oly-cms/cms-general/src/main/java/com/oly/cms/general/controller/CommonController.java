package com.oly.cms.general.controller;

import com.oly.cms.common.domain.entity.CmsColumn;
import com.oly.cms.common.enums.ThemeEnabledEnums;
import com.oly.cms.general.cache.GeneralColumnCacheService;
import com.oly.cms.general.taglib.ConfigTag;
import com.oly.cms.general.taglib.WebTag;
import com.grace.common.core.controller.BaseController;
import com.grace.common.properties.PropertyEnum;
import com.grace.common.utils.ServletUtils;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

/** 通用 */
public class CommonController extends BaseController {

    @Autowired
    protected GeneralColumnCacheService cmsColumnService;

    @Autowired
    protected ConfigTag configService;

    @Autowired
    private WebTag webTag;

    /**
     * 默认转发
     * 并附带参数
     * 
     * @param page
     * @return
     */
    protected String getPrefix(String themeName, String page, ModelMap mp) {
        if (webTag.mapTheme().containsKey(themeName)) {
            int status = webTag.mapTheme().get(themeName).getThemeEnabled();
            if (status == ThemeEnabledEnums.CLOSE.ordinal()) {
                return "/default/pages/web/close";
            } else if (status == ThemeEnabledEnums.UPHOLD.ordinal()) {
                return "/default/pages/web/uphold";
            } else {
                mp.putAll(ServletUtils.getRequest().getParameterMap());
                return themeName + page;
            }
        } else {
            return "/default/pages/web/absent";
        }

    }

    /**
     * 获取导航信息
     * 
     * @param themeName
     * @param propertyEnum
     * @return
     */
    protected CmsColumn getCmsColumn(String themeName, PropertyEnum propertyEnum) {
        CmsColumn cmsColumn = cmsColumnService.selectCmsColumnByIdItem(
                Long.parseLong(configService.getKeyDefaultEnum(themeName, propertyEnum)));
        return cmsColumn == null ? new CmsColumn() : cmsColumn;
    }

    /**
     * 获取导航信息
     * 
     * @param themeName 配置组别
     * @param key       配置key
     * @return
     */
    protected CmsColumn selectCmsColumnByIdItem(String themeName, String key) {
        CmsColumn cmsColumn = cmsColumnService.selectCmsColumnByIdItem(
                NumberUtils.toLong(configService.getKey(themeName, key), 0));
        return cmsColumn == null ? new CmsColumn() : cmsColumn;
    }

}
