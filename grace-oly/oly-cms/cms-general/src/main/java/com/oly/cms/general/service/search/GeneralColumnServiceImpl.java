package com.oly.cms.general.service.search;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsColumn;
import com.oly.cms.common.enums.CommonVisibleEnums;

import com.oly.cms.general.service.IGeneralSearchService;
import com.oly.cms.general.utils.tree.MenuTreeUtils;
import com.oly.cms.query.mapper.ColumnSearchMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneralColumnServiceImpl implements IGeneralSearchService {
    @Autowired
    private ColumnSearchMapper columnSearchMapper;

    public CmsColumn selectCmsColumnByIdItem(long columnId) {

        return columnSearchMapper.selectCmsColumnById(columnId);
    }

    public List<CmsColumn> listCmsColumn(CmsColumn cmsColumn) {

        return columnSearchMapper.listCmsColumns(cmsColumn);
    }

    /**
     * 获取节点及所有子节点
     * 
     * @param columnId
     * @return
     */
    public List<CmsColumn> listCmsColumnById(long columnId) {
        CmsColumn cmsColumn = new CmsColumn();
        cmsColumn.setVisible(CommonVisibleEnums.SHOW.ordinal());
        cmsColumn.setColumnId(columnId);
        return this.listCmsColumn(cmsColumn);
    }

    /**
     * 转化为树
     * 
     * @param columnId
     * @return
     */
    public CmsColumn treeCmsColumnById(long columnId) {
        return MenuTreeUtils.getColumnTree(this.listCmsColumnById(columnId), columnId);
    }

    public CmsColumn listCmsColumnTree(CmsColumn cmsColumn) {
        if (cmsColumn == null || cmsColumn.getColumnId() == null) {
            cmsColumn = new CmsColumn();
            cmsColumn.setColumnId(0L);
        }
        return MenuTreeUtils.getColumnTree(this.listCmsColumn(cmsColumn), cmsColumn.getColumnId());
    }

}
