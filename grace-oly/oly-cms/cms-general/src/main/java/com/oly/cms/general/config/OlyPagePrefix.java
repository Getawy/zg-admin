package com.oly.cms.general.config;

public class OlyPagePrefix {
    public static final String indexPage = "";
    public static final String articlesPage = "/article";
    public static final String articlePage = "/article/";
    public static final String categoriesPage = "/category";
    public static final String categoryPage = "/category/";
    public static final String aboutPage = "/about";
    public static final String rankPage = "/rank";
    public static final String linksPage = "/link";
    public static final String linkPage = "/link/";
    public static final String contactPage = "/contact";
    public static final String timeLinePage = "/timeLine";
}
