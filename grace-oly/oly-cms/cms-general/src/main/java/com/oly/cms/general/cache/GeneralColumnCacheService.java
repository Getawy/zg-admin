package com.oly.cms.general.cache;

import java.util.List;

import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.domain.entity.CmsColumn;
import com.oly.cms.general.service.search.GeneralColumnServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.MENU_CACHE_KEY_PREFIX)
public class GeneralColumnCacheService {

    @Autowired
    private GeneralColumnServiceImpl columnService;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsColumn selectCmsColumnByIdItem(long columnId) {
        return columnService.selectCmsColumnByIdItem(columnId);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsColumn> listCmsColumnsById(long columnId) {
        return columnService.listCmsColumnById(columnId);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsColumn treeCmsColumnById(long columnId) {

        return columnService.treeCmsColumnById(columnId);
    }

    public List<CmsColumn> listCmsColumn(CmsColumn cmsColumn) {

        return columnService.listCmsColumn(cmsColumn);
    }

}
