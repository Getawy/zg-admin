
package com.oly.cms.general.cache;

import java.util.List;
import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.domain.entity.CmsLink;
import com.oly.cms.general.service.search.GeneralLinkServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.LINKS_CACHE_KEY_PREFIX)
public class GeneralLinkCacheService {

    @Autowired
    private GeneralLinkServiceImpl linkServiceImpl;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsLink selectCmsLinkByIdItem(Long linkId) {
        return linkServiceImpl.selectCmsLinkById(linkId);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsLink> listCmsLinks(Long parentId, String nodeType) {
        CmsLink cmsLink = new CmsLink();
        cmsLink.setParentId(parentId);
        cmsLink.setNodeType(nodeType);
        return linkServiceImpl.listCmsLinks(cmsLink);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsLink> treeCmsLinks(Long parentId, String nodeType) {
        CmsLink cmsLink = new CmsLink();
        cmsLink.setNodeType(nodeType);
        List<CmsLink> cmsLinks = linkServiceImpl.listCmsLinks(cmsLink);
        return linkServiceImpl.linkTree(parentId, cmsLinks);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsLink> hotCmsLinksByParentId(Long parentId, Long orderNum) {
        CmsLink cmsLink = new CmsLink();
        cmsLink.setOrderNum(orderNum);
        cmsLink.setParentId(parentId);
        return linkServiceImpl.listCmsLinks(cmsLink);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<CmsLink> hotCmsLinksByNodeType(String nodeType, Long orderNum) {
        CmsLink cmsLink = new CmsLink();
        cmsLink.setOrderNum(orderNum);
        cmsLink.setNodeType(nodeType);
        return linkServiceImpl.listCmsLinks(cmsLink);
    }

}
