package com.oly.cms.general.service.impl;

import java.util.List;
import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.general.mapper.GeneralContactMapper;
import com.oly.cms.general.service.IGeneralContactService;
import com.oly.cms.query.mapper.ContactSearchMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 反馈|建议Service业务层处理
 * 
 * @author ZG
 * @date 2020-06-15
 */
@Service
public class GeneralContactServiceImpl implements IGeneralContactService {
    @Autowired
    private ContactSearchMapper contactSearchMapper;

    @Autowired
    private GeneralContactMapper contactMapper;

    /**
     * 查询反馈|建议
     * 
     * @param contactId 反馈|建议ID
     * @return 反馈|建议
     */
    @Override
    public CmsContact selectCmsContactById(Long contactId) {
        return contactSearchMapper.selectCmsContactById(contactId);
    }

    /**
     * 查询反馈|建议列表
     * 
     * @param cmsContact 反馈|建议
     * @return 反馈|建议
     */
    @Override
    public List<CmsContact> listCmsContact(CmsContact cmsContact) {
        return contactSearchMapper.listCmsContact(cmsContact);
    }

    @Override
    public int insertCmsContact(CmsContact cmsContact) {
        return contactMapper.insertCmsContact(cmsContact);
    }

    @Override
    public int updateCmsContact(CmsContact cmsContact) {
        return contactMapper.updateCmsContact(cmsContact);
    }

    @Override
    public int deleteCmsContactByIds(Long[] contactIds) {
        return contactMapper.deleteCmsContactByIds(contactIds);
    }
}
