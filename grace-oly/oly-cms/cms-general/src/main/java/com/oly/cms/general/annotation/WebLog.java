package com.oly.cms.general.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.oly.cms.general.model.enums.WebBusinessType;
import com.oly.cms.general.model.enums.WebLogType;
import com.oly.cms.general.model.enums.WebOperatorType;

/**
 * 站点系统日志注解
 * @author zg
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebLog {
    /**
     * 标题模块
     */
    public String title() default "";

    /**
     * 操作类型
     */
    public WebBusinessType businessType() default WebBusinessType.OTHER;

    /**
     * 操作人类别
     */
    public WebOperatorType operatorType() default WebOperatorType.OTHER;

    /**
     * 记录类型
     */
    public WebLogType logType() default WebLogType.OTHER;

    /**
     * 记录访问主题
     */
    public String themeName() default "";

    /**
     * 是否保存响应的参数
     */
    public boolean isSaveResponseData() default true;

    /**
     * 排除指定的请求参数
     */
    public String[] excludeParamNames() default {};
}
