package com.oly.cms.general.service.search;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.grace.common.config.GraceConfig;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.cache.GeneralThemeCacheService;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.general.service.IGeneralSearchService;
import com.oly.cms.query.mapper.ArticleSearchMapper;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;

@Service
public class GeneralArticleServiceImpl implements IGeneralSearchService {

    @Autowired
    private ArticleSearchMapper webSearchMapper;

    @Autowired
    private GeneralThemeCacheService themeService;

    /**
     * 综合查询
     * 
     * @param bb
     * @param themeName
     * @return
     */
    public List<WebArticleVo> listArticleBySearch(ArticleSearchParam bb) {
        return webSearchMapper.listArticleBySearch(bb);
    }

    /**
     * 
     * @param articleId  文章ID
     * @param useMd
     * @param useContent
     * @return
     */
    public WebArticleVo selectArticleById(Long articleId, String themeName, Boolean useMd, Boolean useContent) {
        ArticleSearchParam articleSearchParam = getArticleSearchParam(themeName);
        articleSearchParam.setArticleId(articleId);
        articleSearchParam.setUseArticleContent(useContent);
        articleSearchParam.setUseArticleMd(useMd);
        return webSearchMapper.selectArticleById(articleSearchParam);
    }

    /**
     * 完成
     * 
     * @param articleUrl
     * @param useMd
     * @param useContent
     * @return
     */
    public WebArticleVo selectArticleByUrl(String articleUrl, String themeName, Boolean useMd, Boolean useContent) {
        ArticleSearchParam articleSearchParam = getArticleSearchParam(themeName);
        articleSearchParam.setArticleUrl(articleUrl);
        articleSearchParam.setUseArticleContent(useContent);
        articleSearchParam.setUseArticleMd(useMd);
        return webSearchMapper.selectArticleById(articleSearchParam);
    }

    /**
     * 完成
     * 
     * @param themeName
     * @param articleType
     * @param categoryId
     * @param num
     * @param size
     * @param order
     * @return
     */
    public PageData selectPageData(String themeName, Integer articleType, Long categoryId, int num,
            int size, String order) {
        ArticleSearchParam ba = getArticleSearchParam(themeName);
        ba.setArticleType(articleType);
        ba.setCategoryId(categoryId);
        PageHelper.startPage(num, size, order);
        return PageData.getData(this.listArticleBySearch(ba), 200);
    }

    /**
     * 完成
     * 
     * @param themeName 主题名
     * @param pageNum   页数
     * @param pageSize  页大小
     * @param crTime    时间 2020-10
     * @return
     */
    public PageArticleTimeLine groupByTime(String themeName, String crTime, int pageNum, int pageSize) {
        ArticleSearchParam bb = getArticleSearchParam(themeName);
        bb.setCrTime(crTime);
        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<WebArticleVo> list = this.listArticleBySearch(bb);
        return PageArticleTimeLine.getData(list);
    }

    /**
     * 完成
     * 获取主题文章数
     * 
     * @param themeName
     * @param articleTypeEnums
     * @return
     */
    public int selectArticleNum(String themeName, ArticleTypeEnums articleTypeEnums) {
        ArticleSearchParam articleSearchParam = getArticleSearchParam(themeName);
        articleSearchParam.setArticleType(articleTypeEnums.ordinal());
        return webSearchMapper.selectArticleNumUnion(articleSearchParam);
    }

    /**
     * 完成
     * 获取主题文章数
     * 
     * @param themeName
     * @return
     */
    public int selectArticleNum(String themeName) {
        return webSearchMapper.selectArticleNumUnion(getArticleSearchParam(themeName));
    }

    /**
     * 完成
     * 获取文章发布年月列表
     * 
     * @param themeName 主题名
     * @param pageNum   页数
     * @param pageSize  每页大小
     * @return
     */
    public List<TimeNum> listArticleTimeNum(String themeName, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return webSearchMapper.listArticleTimeNum(
                themeService.selectCmsThemeItem(GraceConfig.getOnlyCode(), themeName).getSupportCategoryId());
    }

    private ArticleSearchParam getArticleSearchParam(String themeName) {
        ArticleSearchParam articleSearchParam = new ArticleSearchParam();
        CmsTheme cmsTheme = themeService.selectCmsThemeItem(GraceConfig.getOnlyCode(), themeName);
        articleSearchParam.setSupportCategoryId(cmsTheme.getSupportCategoryId());
        articleSearchParam.setSupportArticleType(cmsTheme.getSupportArticleType());
        return articleSearchParam;
    }
}
