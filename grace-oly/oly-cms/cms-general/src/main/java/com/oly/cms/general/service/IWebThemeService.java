package com.oly.cms.general.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import com.oly.cms.common.model.support.ThemeTreeNode;

public interface IWebThemeService {
    /**
     * 删除主题 通过主题名字
     * 
     * @param themeName
     * @return
     * @throws FileNotFoundException
     */
    boolean deleteThemeByName(String themeName) throws FileNotFoundException;

    /**
     * 获取主题配置及主题信息
     * 
     * @param themeName
     * @return
     */
    Object selectThemeConfigSetting(String themeName);

    /**
     * 获取主题配置表单
     * 
     * @param themeName
     * @return
     * @throws IOException
     */
    Object selectThemeConfigForm(String themeName) throws IOException;

    /**
     * 
     * @param file  文件
     * @param cover 是否覆盖上传
     * @return
     * @throws IOException
     * @throws Throwable
     */
    boolean uploadTheme(MultipartFile file, boolean cover) throws Throwable, IOException;

    List<ThemeTreeNode> selectThemeFileTree(String path);

}
