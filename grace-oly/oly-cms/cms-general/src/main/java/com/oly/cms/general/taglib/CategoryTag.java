package com.oly.cms.general.taglib;

import java.util.List;

import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.general.cache.GeneralCategoryCacheService;
import com.oly.cms.query.model.param.CategorySearchParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 分类标签
 */
@Service("categoryTag")
public class CategoryTag {

    @Autowired
    private GeneralCategoryCacheService categoryCacheService;

    /**
     * 获取分类
     * 
     * @param categoryId
     * @return
     */
    public CmsCategory selectCmsCategoryById(Long categoryId, String themeName) {
        return categoryCacheService.selectCmsCategoryByIdItem(categoryId, themeName);
    }

    /**
     * 
     * @param nodeType  类型
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCmsCategory(CategoryNodeTypeEnums nodeType,
            String themeName) {
        return categoryCacheService.listCmsCategory(null, nodeType, null, themeName);
    }

    /**
     * 
     * @param orderNum  排序
     * @param nodeType  类型
     * @param themeName 主题名字
     * @return
     */
    public List<CmsCategory> listCmsCategoryByOrder(CategoryNodeTypeEnums nodeType,
            String themeName, Long orderNum) {
        return categoryCacheService.listCmsCategory(orderNum, nodeType, null, themeName);
    }

    /**
     * 
     * @param nodeType   标签类型
     * @param categoryId 标签Id
     * 
     * @param themeName  主题名字
     * @return
     */
    public List<CmsCategory> listCmsCategory(CategoryNodeTypeEnums nodeType, Long categoryId, String themeName) {

        return categoryCacheService.listCmsCategory(null, nodeType, categoryId, themeName);
    }

    /**
     * 获取分类树
     * 通过排序
     * 
     * @param orderNum
     * @param nodeType
     * @param categoryId
     * @param themeName
     * @return
     */
    public CmsCategory treeCmsCategoryByOrderNum(Long orderNum, CategoryNodeTypeEnums nodeType, Long categoryId,
            String themeName) {
        return categoryCacheService.treeCmsCategory(orderNum, nodeType, categoryId, themeName);
    }

    /**
     * 获取分类树
     * 
     * @param orderNum
     * @param nodeType
     * @param categoryId
     * @param themeName
     * @return
     */
    public CmsCategory treeCmsCategory(CategoryNodeTypeEnums nodeType, Long categoryId,
            String themeName) {
        return categoryCacheService.treeCmsCategory(null, nodeType, categoryId, themeName);
    }

    /**
     * 儿子节点
     * 
     * @param categoryId
     * @param nodeType   节点类型
     * @param themeName  主题名字
     * @return
     */
    public List<CmsCategory> listCategoryByParentId(long parentId, CategoryNodeTypeEnums nodeType, String themeName) {
        return categoryCacheService.listCmsCategory(null, nodeType, parentId, themeName);
    }

    /**
     * 
     * @param nodeType   类型
     * @param categoryId 分类Id
     * @return
     */
    public List<CmsCategory> listCategoryByType(String themeName, CategoryNodeTypeEnums nodeType, Long categoryId) {
        return categoryCacheService.listCmsCategory(null, nodeType, categoryId, themeName);
    }

    /**
     * 
     * @param orderNum   排序
     * @param categoryId 分类Id
     * @param themeName  主题名字
     * @return
     */
    public List<CmsCategory> listCategoryByOrderNum(String themeName, Long orderNum, long categoryId) {
        return categoryCacheService.listCmsCategory(orderNum, CategoryNodeTypeEnums.CLASSIFY, categoryId, themeName);
    }

    /**
     * 获取数量
     * 
     * @param themeName
     * @return
     */
    public int selectCategoryNum(String themeName, CategoryNodeTypeEnums nodeTypeEnums) {
        return categoryCacheService.selectCategoryNum(themeName, nodeTypeEnums);
    }

    public boolean checkSupportCategory(String themeName, long categoryId) {
        return categoryCacheService.checkSupportCategory(themeName, categoryId);

    }

    public List<CmsCategory> listCmsCategory(CategorySearchParam param) {

        return categoryCacheService.listCmsCategory(param);
    }

    public CmsCategory treeCategoryBySearchParam(CategorySearchParam param) {

        return categoryCacheService.treeCmsCategoryBySearchParam(param);
    }

}
