package com.oly.cms.general.service.search;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grace.common.config.GraceConfig;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.common.enums.CommonVisibleEnums;
import com.oly.cms.general.service.IGeneralSearchService;
import com.oly.cms.general.taglib.WebTag;
import com.oly.cms.general.utils.tree.CategoryTreeUtils;
import com.oly.cms.query.mapper.CategorySearchMapper;
import com.oly.cms.query.model.param.CategorySearchParam;

@Service
public class GeneralCategoryServiceImpl implements IGeneralSearchService {
    @Autowired
    private CategorySearchMapper categorySearchMapper;

    @Autowired
    private WebTag webTag;

    /**
     * 获取分类
     * 
     * @param param
     * @return
     */
    public CmsCategory selectCmsCategoryById(CategorySearchParam param) {
        return categorySearchMapper.selectCmsCategoryById(param);
    }

    /**
     * 获取分类列表
     * 
     * @param param
     * @return
     */
    public List<CmsCategory> listCmsCategory(CategorySearchParam param) {
        return categorySearchMapper.listCmsCategory(param);
    }

    /**
     * 
     * @param orderNum
     * @param categoryId
     * @param nodeType
     * @param themeName
     * @return
     */
    public List<CmsCategory> listCmsCategory(Long orderNum, CategoryNodeTypeEnums nodeType, Long categoryId,
            String themeName) {
        CategorySearchParam param = getCategorySearchParam(themeName);
        param.setOrderNum(orderNum);
        param.setCategoryId(categoryId);
        param.setNodeTypes(CategoryNodeTypeEnums.getNodeTypes(nodeType));
        return this.listCmsCategory(param);
    }

    /**
     * 获取分类树
     * 
     * @param param
     * @return
     */
    public CmsCategory treeCmsCategoryBySearchParam(CategorySearchParam param) {
        if (param == null || param.getCategoryId() == null) {
            param = new CategorySearchParam();
            param.setCategoryId(0L);
        }
        return CategoryTreeUtils.getCategoryTree(this.listCmsCategory(param), param.getCategoryId());
    }

    /**
     * 完成
     * 
     * @param orderNum
     * @param categoryId
     * @param nodeType   节点类型
     * @param themeName
     * @return
     */
    public CmsCategory treeCmsCategory(Long orderNum, CategoryNodeTypeEnums nodeType, Long categoryId,
            String themeName) {
        return CategoryTreeUtils.getCategoryTree(this.listCmsCategory(orderNum, nodeType, categoryId, themeName),
                categoryId == null ? 0L : categoryId);
    }

    /**
     * 完成
     * 获取分类数目
     * 
     * @param themeName
     * @param nodeTypeEnums
     * @return
     */
    public int selectCategoryNum(String themeName, CategoryNodeTypeEnums nodeTypeEnums) {
        CategorySearchParam param = getCategorySearchParam(themeName);
        param.setNodeTypes(CategoryNodeTypeEnums.getNodeTypes(nodeTypeEnums));
        return categorySearchMapper.selectCategoryNum(param);
    }

    /**
     * 完成
     * 确定主题知否支持当前类
     * 
     * @param param
     * @return
     */
    public boolean checkSupportCategory(CategorySearchParam param) {
        CmsCategory category = categorySearchMapper.selectCmsCategoryById(param);
        if (category != null) {
            return true;
        }
        return false;
    }

    private CategorySearchParam getCategorySearchParam(String themeName) {
        CategorySearchParam param = new CategorySearchParam();
        param.setSupportCategoryId(webTag.selectCmsThemeByThemeName(themeName).getSupportCategoryId());
        param.setVisible(CommonVisibleEnums.SHOW.ordinal());
        param.setThemeName(GraceConfig.getOnlyCode() + "_" + themeName);
        return param;
    }

}
