package com.oly.cms.general.taglib;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson2.JSON;
import com.grace.common.utils.StringUtils;
import com.oly.cms.common.domain.vo.UserVo;
import com.oly.cms.query.mapper.CommonSearchMapper;

/**
 * 通用标签
 */
@Service("commonTag")
public class CommonTag {

    @Autowired
    private CommonSearchMapper commonSearchMapper;

    /**
     * 字符串分割
     * 
     * @param s
     * @return
     */
    public String[] split(String s) {
        if (StringUtils.isEmpty(s.trim())) {
            return null;
        }
        return s.split(",");
    }

    /**
     * 字符串转json
     * 
     * @param s
     * @return
     */
    public Object toJson(String s) {
        return JSON.parse(s);
    }

    /**
     * 获取用户Vo
     * 
     * @param userName
     * @return
     */
    public UserVo getUserVo(String userName) {

        return commonSearchMapper.selectUserVo(userName);
    }

}
