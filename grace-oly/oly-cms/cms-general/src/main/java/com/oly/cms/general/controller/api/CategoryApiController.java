package com.oly.cms.general.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.taglib.CategoryTag;
import com.oly.cms.query.model.param.CategorySearchParam;
import com.grace.common.config.GraceConfig;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;

@CrossOrigin
@RestController
@RequestMapping
public class CategoryApiController extends BaseController {

  @Autowired
  private CategoryTag categoryService;

  /**
   * 获取分类信息
   * 
   * @param themeName
   * @param categoryId
   * @return
   */
  @GetMapping("/{themeName}/api/category/getCategory/{categoryId}")
  public AjaxResult selectCategoryById(@PathVariable("themeName") String themeName,
      @PathVariable("categoryId") Long categoryId) {

    return AjaxResult.success(categoryService.selectCmsCategoryById(categoryId, themeName));

  }

  /**
   * 获取分类数量
   * 
   * @param themeName
   * @return
   */
  @GetMapping("/{themeName}/api/category/getCategoryNum/{nodeType}")
  public AjaxResult getCategoryNum(@PathVariable("themeName") String themeName,
      @PathVariable("nodeType") CategoryNodeTypeEnums nodeType) {
    return AjaxResult.success(categoryService.selectCategoryNum(themeName, nodeType));
  }

  /**
   * 通过类型获取分类列表
   * 
   * @param themeName  主题名
   * @param type       类型
   * @param categoryId 上层节点categoryId
   * @return
   */
  @GetMapping("/{themeName}/api/category/list/nodeType/{nodeType}/{categoryId}")
  public AjaxResult listCmsCategoryByType(@PathVariable("themeName") String themeName,
      @PathVariable("nodeType") CategoryNodeTypeEnums nodeType, @PathVariable("categoryId") Long categoryId) {
    return AjaxResult.success(categoryService.listCategoryByType(themeName, nodeType, categoryId));
  }

  @GetMapping("/{themeName}/api/category/list/nodeType/{nodeType}")
  public AjaxResult listCmsCategoryByType(@PathVariable("themeName") String themeName,
      @PathVariable("nodeType") CategoryNodeTypeEnums nodeType) {
    return AjaxResult.success(categoryService.listCategoryByType(themeName, nodeType, null));
  }

  @GetMapping("/{themeName}/api/category/list/orderNum/{orderNum}/{categoryId}")
  public AjaxResult listCmsCategoryByOrderNum(@PathVariable("themeName") String themeName,
      @PathVariable("orderNum") long orderNum, @PathVariable("categoryId") long categoryId) {
    return AjaxResult.success(categoryService.listCategoryByOrderNum(themeName, orderNum, categoryId));
  }

  // @GetMapping("/{themeName}/api/category/list/{categoryId}")
  // public AjaxResult listCmsCategoryById(@PathVariable("categoryId") Long
  // categoryId,
  // @PathVariable(value = "themeName", required = true) String themeName) {
  // return AjaxResult.success(categoryService.listCategoryById(categoryId,
  // themeName));
  // }

  // @GetMapping("/{themeName}/api/category/tree/{categoryId}")
  // public AjaxResult getCategoryTreeById(@PathVariable("categoryId") Long
  // categoryId,
  // @PathVariable("themeName") String themeName) {
  // return AjaxResult.success(categoryService.listCategoryById(categoryId,
  // themeName));
  // }

  @GetMapping("/{themeName}/category/list")
  public AjaxResult listCategory(CategorySearchParam param,
      @PathVariable("themeName") String themeName) {
    param.setThemeName(GraceConfig.getOnlyCode() + "_" + themeName);
    return AjaxResult.success(PageData.getData(categoryService.listCmsCategory(param), 200));
  }

  @GetMapping("/{themeName}/api/category/tree/{nodeType}/{categoryId}")
  public AjaxResult treeCategory(@PathVariable("themeName") String themeName,
      @PathVariable("nodeType") CategoryNodeTypeEnums nodeType,
      @PathVariable("categoryId") Long categoryId) {
    return AjaxResult.success(categoryService.treeCmsCategory(nodeType, categoryId, themeName));
  }

  @GetMapping("/{themeName}/category/tree")
  public AjaxResult treeCategory(CategorySearchParam param, @PathVariable("themeName") String themeName) {
    param.setThemeName(GraceConfig.getOnlyCode() + "_" + themeName);
    return AjaxResult.success(categoryService.treeCategoryBySearchParam(param));
  }

}