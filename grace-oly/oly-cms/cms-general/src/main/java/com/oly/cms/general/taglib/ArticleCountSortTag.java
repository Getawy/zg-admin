package com.oly.cms.general.taglib;

import java.util.List;
import java.util.Map;

import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.enums.OrderEnums;
import com.oly.cms.general.cache.GeneralArticleVoCacheService;
import com.oly.cms.general.model.enums.ArticleCountSortEnum;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 获取文章基本信息
 */
@Service("articleCountSortTag")
public class ArticleCountSortTag {

        @Autowired
        private GeneralArticleVoCacheService webArticleSortService;

        /**
         * 综合
         * 
         * @param num         页数
         * @param size        页面大小
         * @param articleType 文章类型
         * @param categoryId  文章ID
         * @param themeName   主题名字
         * @param sort        排序字段
         * @param order       排序方式
         * @return
         */
        public List<WebArticleVo> listArticlesVo(int num, int size, ArticleTypeEnums articleType, Long categoryId,
                        String themeName,
                        ArticleCountSortEnum sort, OrderEnums order) {
                return webArticleSortService.listArticlesVoOrder(num, size, articleType.ordinal(), categoryId,
                                themeName,
                                sort.getOrder(order));
        }

        /**
         * 文章类型
         * 
         * @param num         页数
         * @param size        页面大小
         * @param articleType 文章类型
         * @param themeName   主题名字
         * @param sort        排序字段
         * @param order       排序方式
         * @return
         */
        public List<WebArticleVo> listArticlesVoByType(int num, int size, ArticleTypeEnums articleType,
                        String themeName,
                        ArticleCountSortEnum sort, OrderEnums order) {
                return webArticleSortService.listArticlesVoOrder(num, size, articleType.ordinal(), null,
                                themeName,
                                sort.getOrder(order));
        }

        /**
         * 文章排序
         * 
         * @param pageNum
         * @param pageSize
         * @param sort     排序字段
         * @param order    排序方式
         * @return
         */
        public List<WebArticleVo> listWebArticleOrder(int pageNum, int pageSize, ArticleCountSortEnum sort,
                        OrderEnums order) {
                return webArticleSortService.listArticlesVoOrder(pageNum, pageSize, null, null, null,
                                sort.getOrder(order));
        }

        /**
         * 通过主题名
         * 
         * @param pageNum
         * @param pageSize
         * @param themeName
         * @param sort
         * @param order
         * @return
         */
        public List<WebArticleVo> listWebArticleOrder(int pageNum, int pageSize, String themeName,
                        ArticleCountSortEnum sort, OrderEnums order) {
                return webArticleSortService.listArticlesVoOrder(pageNum, pageSize, null, null, themeName,
                                sort.getOrder(order));
        }

        /**
         * 通过文章类型
         * 
         * @param pageNum
         * @param pageSize
         * @param articleType
         * @param themeName
         * @param sort
         * @param order
         * @return
         */
        public List<WebArticleVo> listWebArticleOrderByType(int pageNum, int pageSize, Integer articleType,
                        String themeName,
                        ArticleCountSortEnum sort, OrderEnums order) {
                return webArticleSortService.listArticlesVoOrder(pageNum, pageSize, articleType, null, themeName,
                                sort.getOrder(order));
        }

        /**
         * 通过分类ID
         * 
         * @param themeName
         * @param categoryId
         * @param pageNum
         * @param pageSize
         * @param sort
         * @param order
         * @return
         */
        public List<WebArticleVo> listWebArticleOrderByCategoryId(String themeName, long categoryId, int pageNum,
                        int pageSize, ArticleCountSortEnum sort, OrderEnums order) {
                return webArticleSortService.listArticlesVoOrder(pageNum, pageSize, null, categoryId, themeName,
                                sort.getOrder(order));
        }

        /**
         * 上一条
         * 
         * @param themeName
         * @param articleId
         * @return
         */
        public WebArticleVo selectPreArticle(long articleId, String themeName) {
                return this.selectPreAndNextArticle(articleId, themeName).get("pre");
        }

        /**
         * 下一条
         * 
         * @param articleId
         * @param themeName
         * @return
         */
        public WebArticleVo selectNextArticle(long articleId, String themeName) {
                return this.selectPreAndNextArticle(articleId, themeName).get("next");
        }

        /**
         * 上下
         * 
         * @param articleId
         * @param themeName
         * @return
         */
        public Map<String, WebArticleVo> selectPreAndNextArticle(long articleId, String themeName) {
                return webArticleSortService.selectPreAndNextArticle(articleId, themeName);
        }

        public List<WebArticleVo> listArticleVo(ArticleSearchParam bb) {
                return webArticleSortService.listArticleVo(bb);
        }

        public List<WebArticleVo> listArticleVoOrder(int num, int size, ArticleSearchParam bb,
                        String order) {
                return webArticleSortService.listArticleVoOrder(num, size, bb, order);
        }

}
