package com.oly.cms.general.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.oly.cms.general.taglib.UnionTag;

@CrossOrigin
@RestController
@RequestMapping
public class UnionApiController extends BaseController {

    @Autowired
    private UnionTag unionTag;

    /**
     * 获取标签
     * 
     * @param shopId
     * @return
     */
    @GetMapping("/{themeName}/api/union/get/{shopId}")
    public AjaxResult getCmsUnionByShopId(String shopId) {
        return AjaxResult.success(unionTag.getCmsUnionByShopId(shopId));
    }

    /**
     * @param cat
     * @param tag
     * @param unionType
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/{themeName}/api/union/list")
    public AjaxResult listCmsUnions(
            @RequestParam(required = false) String unionType, int pageNum,
            int pageSize) {
        return AjaxResult.success(unionTag.listCmsUnions(unionType, pageNum, pageSize));
    }

}
