package com.oly.cms.general.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.general.service.search.GeneralArticleVoServiceImpl;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.ARTICLE_CACHE_KEY_PREFIX)
public class GeneralArticleVoCacheService {

    @Autowired
    private GeneralArticleVoServiceImpl webSortService;

    public List<WebArticleVo> listArticleVo(ArticleSearchParam bb) {
        return webSortService.listArticleVo(bb);
    }

    public List<WebArticleVo> listArticleVoOrder(int num, int size, ArticleSearchParam bb,
            String order) {

        return webSortService.listArticleVoOrder(num, size, bb, order);
    }

    // 只涉及文章表和统计表
    @Cacheable(keyGenerator = "myKeyGenerator")
    public List<WebArticleVo> listArticlesVoOrder(int num, int size, Integer articleType, Long categoryId,
            String themeName,
            String order) {
        return webSortService.listArticleVoOrder(num, size, articleType, categoryId, themeName, order);
    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public Map<String, WebArticleVo> selectPreAndNextArticle(long articleId, String themeName) {
        Map<String, WebArticleVo> mp = new HashMap<>();
        mp.put("next", webSortService.selectNextArticle(articleId, themeName));
        mp.put("pre", webSortService.selectPreArticle(articleId, themeName));
        return mp;
    }

}
