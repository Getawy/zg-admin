package com.oly.cms.general.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.oly.cms.general.taglib.WebTag;

@CrossOrigin
@RestController
@RequestMapping
public class WebApiController extends BaseController {
    @Autowired
    private WebTag webTag;

    @GetMapping("/{themeName}/api/web/info")
    public AjaxResult listCmsUnions(@PathVariable("themeName") String themeName) {
        return AjaxResult.success(webTag.selectCmsThemeByThemeName(themeName));
    }
}
