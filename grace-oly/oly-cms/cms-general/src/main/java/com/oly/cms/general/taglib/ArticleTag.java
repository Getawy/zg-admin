package com.oly.cms.general.taglib;

import java.util.List;

import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.enums.OrderEnums;
import com.oly.cms.common.model.TimeNum;
import com.oly.cms.common.model.support.PageData;
import com.oly.cms.general.cache.GeneralArticleCacheService;
import com.oly.cms.general.model.PageArticleTimeLine;
import com.oly.cms.query.model.param.ArticleSearchParam;
import com.oly.cms.query.model.vo.WebArticleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文章标签 涉及文章表的操作
 */
@Service("articleTag")
public class ArticleTag {
    @Autowired
    private GeneralArticleCacheService articleService;

    /**
     * 获取文章数量
     * 
     * @param themeName
     * @param articleTypeEnums
     * @return
     */
    public int selectArticleNumByType(String themeName, ArticleTypeEnums articleTypeEnums) {
        return articleService.selectArticleNum(themeName, articleTypeEnums);
    }

    /**
     * 获取文章数量
     * 
     * @param themeName
     * @return
     */
    public int selectArticleNum(String themeName) {
        return articleService.selectArticleNum(themeName);
    }

    /**
     * 依据年月获取文章数量
     * 
     * @param themeName
     * @param pageNum
     * @param pageSize
     * @return
     */
    public List<TimeNum> listArticleTimeNum(String themeName, int pageNum, int pageSize) {
        return articleService.listArticleTimeNum(themeName, pageNum, pageSize);
    }

    /**
     * 获取文章Html
     * 
     * @param articleId
     * @return
     */
    public WebArticleVo selectWebArticleHtmlById(Long articleId, String themeName) {
        return articleService.selectWebArticleByIdItem(articleId, themeName, false, true);
    }

    /**
     * 获取文章MarkDown
     * 
     * @param articleId
     * @return
     */
    public WebArticleVo selectWebArticleMdById(Long articleId, String themeName) {
        return articleService.selectWebArticleByIdItem(articleId, themeName, true, false);
    }

    /**
     * 获取文章Html
     * 
     * @param articleUrl
     * @return
     */
    public WebArticleVo selectWebArticleHtmlByUrl(String articleUrl, String themeName) {
        return articleService.selectWebArticleByUrlItem(articleUrl, themeName, false, true);
    }

    /**
     * 获取文章MarkDown
     * 
     * @param articleUrl
     * @return
     */
    public WebArticleVo selectWebArticleMdByUrl(String articleUrl, String themeName) {
        return articleService.selectWebArticleByUrlItem(articleUrl, themeName, true, false);
    }

    /**
     * 依据分类
     * 
     * @param categoryId
     * @param num
     * @param size
     * @return
     */
    public PageData listWebArticlesByCategoryId(String themeName, long categoryId, int num, int size) {
        return articleService.selectPageData(themeName, null, categoryId, num, size, getOrder(OrderEnums.DESC));
    }

    /**
     * 依据分类
     * 
     * @param categoryId
     * @param num
     * @param size
     * @param order
     * @return
     */
    public PageData selectPageDataByCategoryId(String themeName, long categoryId, int num, int size,
            OrderEnums order) {

        return articleService.selectPageData(themeName, null, categoryId, num, size, getOrder(order));
    }

    /**
     * 依据类型
     * 
     * @param themeName
     * @param articleType
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageData listWebArticleByType(String themeName, int articleType, int pageNum, int pageSize) {
        return articleService.selectPageData(themeName, articleType, null, pageNum, pageSize,
                getOrder(OrderEnums.DESC));
    }

    /**
     * 
     * @param articleType
     * @param pageNum
     * @param pageSize
     * @param order
     * @return
     */
    public PageData listWebArticleByType(String themeName, int articleType, int pageNum, int pageSize,
            OrderEnums order) {
        return articleService.selectPageData(themeName, articleType, null, pageNum, pageSize, getOrder(order));
    }

    /**
     * 文章列表
     * 
     * @param themeName
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageData pageArticles(String themeName, int pageNum, int pageSize) {

        return articleService.selectPageData(themeName, null, null, pageNum, pageSize,
                getOrder(OrderEnums.DESC));
    }

    /**
     * 文章列表通过分类ID
     * 
     * @param themeName
     * @param pageNum
     * @param pageSize
     * @param categoryId
     * @return
     */
    public PageData pageArticleByCategoryId(String themeName, long categoryId, int pageNum, int pageSize) {

        return articleService.selectPageData(themeName, null, categoryId, pageNum, pageSize,
                getOrder(OrderEnums.DESC));
    }

    /**
     * 综合查询
     * 
     * @param bb
     * @return
     */
    public List<WebArticleVo> listWebArticles(ArticleSearchParam bb) {
        return articleService.listWebArticles(bb);
    }

    /**
     * 时间线
     * 
     * @param pageNum
     * @param pageSize
     * @param themeName
     * @param crTime
     * @return
     */
    public PageArticleTimeLine groupByTime(int pageNum, int pageSize, String themeName, String crTime) {
        return articleService.groupByTime(pageNum, pageSize, themeName, crTime);
    }

    private String getOrder(OrderEnums order) {
        return "article_top desc,create_time " + order.name();
    }

}
