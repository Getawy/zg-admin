package com.oly.cms.general.manager.factory;

import java.util.TimerTask;
import com.grace.common.utils.ip.AddressUtils;
import com.grace.common.utils.spring.SpringUtils;
import com.oly.cms.common.domain.entity.CmsLogRecord;
import com.oly.cms.general.service.impl.GeneralRecordServiceImpl;

/**
 * 异步工厂（产生任务用）
 * 
 * @author grace
 */
public class AsyncFactory {

    /**
     * 记录请求记录
     * 
     * @param operLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordWebOper(final CmsLogRecord operLog) {
        return new TimerTask() {
            @Override
            public void run() {
                // 远程查询操作地点
                operLog.setOperLocation(AddressUtils.getRealAddressByIP(operLog.getOperIp()));
                SpringUtils.getBean(GeneralRecordServiceImpl.class).insertWebLogRecord(operLog);
            }
        };
    }
}
