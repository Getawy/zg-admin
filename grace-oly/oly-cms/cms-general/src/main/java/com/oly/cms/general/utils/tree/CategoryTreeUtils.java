package com.oly.cms.general.utils.tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.oly.cms.common.domain.entity.CmsCategory;

/** 分类工具 */
public class CategoryTreeUtils {
    /**
     * 根据父节点的ID获取所有子节点
     * 
     * @param list     分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public static CmsCategory getCategoryTree(List<CmsCategory> list, Long parentId) {
        CmsCategory re = new CmsCategory();
        if (parentId == null || list.size() == 0) {
            return re;
        } else if (parentId == 0L) {
            re.setCategoryId(0L);
            re.setCategoryName("根目录");
        }
        List<CmsCategory> returnList = new ArrayList<CmsCategory>();
        for (Iterator<CmsCategory> iterator = list.iterator(); iterator.hasNext();) {
            CmsCategory t = (CmsCategory) iterator.next();
            // 获取根节点
            if (t.getCategoryId() == parentId) {
                re = t;
            }
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId) {
                // 遍历第一层
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        re.setChildList(returnList);
        return re;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private static void recursionFn(List<CmsCategory> list, CmsCategory t) {
        // 得到子节点列表
        List<CmsCategory> childList = getChildList(list, t);
        // 添加子节点列表
        t.setChildList(childList);
        for (CmsCategory tChild : childList) {
            if (hasChild(list, tChild)) {
                // 判断是否有子节点
                Iterator<CmsCategory> it = childList.iterator();
                while (it.hasNext()) {
                    CmsCategory n = (CmsCategory) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }

    /**
     *
     * @param list 遍历的数据
     * @param t    父节点
     * @return
     */
    private static List<CmsCategory> getChildList(List<CmsCategory> list, CmsCategory t) {
        List<CmsCategory> tlist = new ArrayList<CmsCategory>();
        Iterator<CmsCategory> it = list.iterator();
        while (it.hasNext()) {
            CmsCategory n = (CmsCategory) it.next();
            if (n.getParentId().longValue() == t.getCategoryId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private static boolean hasChild(List<CmsCategory> list, CmsCategory t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }
}
