package com.oly.cms.general.controller.page;

import javax.servlet.http.HttpServletResponse;

import com.oly.cms.common.enums.ArticleTypeEnums;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.general.annotation.WebLog;
import com.oly.cms.general.constant.BaseWebPagePrefix;
import com.oly.cms.general.model.enums.WebLogType;
import com.oly.cms.general.service.page.WebPageService;
import com.oly.cms.query.model.param.ArticleSearchParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 前端视图
 */
@Controller
@RequestMapping
public class WebPageController {
    @Autowired
    private WebPageService webPageService;

    /**
     * 主页
     * 
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "主页请求", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}")
    public String index(@PathVariable("themeName") String themeName,ModelMap mp) {
        return webPageService.index(themeName, mp);
    }

    /**
     * 请求指定文章
     *
     * @param articleId
     * @param mp
     * @return
     */
    @WebLog(title = "获取文章", logType = WebLogType.PAGE)
    @GetMapping(value = { "/{themeName}"+BaseWebPagePrefix.ARTICLE_PAGE+"/{articleId}" })
    public String article(@PathVariable("articleId") Long articleId,
            @PathVariable("themeName") String themeName,
            ModelMap mp) {
        return webPageService.selectArticleById(themeName, articleId, mp);
    }

    /**
     * 文章列表
     *
     * @param themeName 
     * @param mp
     * @param parm
     * @return
     */
    @WebLog(title = "获取文章列表", logType = WebLogType.PAGE)
    @GetMapping({ "/{themeName}"+BaseWebPagePrefix.ARTICLE_PAGE, "/{themeName}"+BaseWebPagePrefix.ARTICLE_PAGE+"/type/{type}" })
    public String listArticle(@PathVariable("themeName") String themeName,
            @PathVariable(value = "type", required = false) ArticleTypeEnums type, ModelMap mp,
            ArticleSearchParam parm) {
        return webPageService.listWebArticle(themeName, type, mp, parm);
    }

    /**
     * 分类列表
     *
     * @param themeName
     * @param category
     * @param param
     * @return
     */
    @WebLog(title = "获取分类列表", logType = WebLogType.PAGE)
    @GetMapping({ "/{themeName}"+BaseWebPagePrefix.CATEGORIES_PAGES, "/{themeName}"+BaseWebPagePrefix.CATEGORIES_PAGES+"/type/{nodeType}" })
    public String categories(@PathVariable("themeName") String themeName,
            @PathVariable(value = "nodeType", required = false) CategoryNodeTypeEnums nodeType,
            ModelMap mp) {
        return webPageService.listCmsCategory(themeName, nodeType, mp);

    }

    /**
     * 分类详情
     *
     * @param themeName  主题名
     * @param categoryId 分类ID
     * @param mp
     * @return
     */
    @WebLog(title = "获取分类", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.CATEGORY_PAGE+"{categoryId}")
    public String category(@PathVariable("themeName") String themeName,
            @PathVariable("categoryId") Long categoryId, ModelMap mp) {
        return webPageService.selectCmsCategoryById(themeName, categoryId, mp);
    }

    /**
     * 友情链接
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "友情链接", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.LINKS_PAGE)
    public String links(@PathVariable("themeName") String themeName, ModelMap mp) {
        return webPageService.links(themeName, mp);
    }

    /**
     * 友情链接
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "友情链接", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.LINK_PAGE+"{linkId}")
    public String link(@PathVariable("themeName") String themeName, @PathVariable("linkId") Long linkId, ModelMap mp) {
        return webPageService.link(themeName, linkId, mp);
    }

    /**
     * 关于本站
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "关于本站", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.ABOUT_PAGE)
    public String about(@PathVariable("themeName") String themeName, ModelMap mp) {
        return webPageService.about(themeName, mp);
    }

    /**
     * 排行榜
     *
     * @param themeName
     * @param modelMap
     * @return
     */
    @WebLog(title = "排行榜", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.RANK_PAGE)
    public String rank(@PathVariable("themeName") String themeName, ModelMap modelMap) {
        return webPageService.rank(themeName, modelMap);
    }

    /**
     * 时间线
     *
     * @param themeName
     * @param modelMap
     * @return
     */
    @WebLog(title = "时间线", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.TIME_LINE_PAGE)
    public String timeLine(@PathVariable("themeName") String themeName,
            ModelMap modelMap) {
        return webPageService.timeLine(themeName, modelMap);
    }

    /**
     * 反馈
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "反馈", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}"+BaseWebPagePrefix.CONTACT_PAGE)
    public String contact(@PathVariable("themeName") String themeName, ModelMap mp) {
        return webPageService.contact(themeName, mp);
    }

    /**
     * 联盟页面
     *
     * @param themeName
     * @param mp
     * @return
     */
    @WebLog(title = "联盟页", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/union")
    public String Union(@PathVariable("themeName") String themeName, ModelMap mp) {
        return webPageService.union(themeName, mp);
    }

    /**
     * 自定义页面
     *
     * @param page
     * @return
     */
    @WebLog(title = "自定义页面", logType = WebLogType.PAGE)
    @GetMapping("/{themeName}/custom/{page}")
    public String customPage(@PathVariable("themeName") String themeName, ModelMap mp,
            @PathVariable("page") String page) {
        return webPageService.custom(themeName, mp, page);
    }

    /**
     * robots.txt
     * 爬虫规则
     * @param themeName
     * @param response
     * @param mp
     */
    @GetMapping("/{themeName}/robots.txt")
    public void robots(@PathVariable("themeName") String themeName,
            HttpServletResponse response, ModelMap mp) {
        webPageService.robots(themeName, response, mp);
    }

    /**
     * sitemap.xml
     * @param themeName
     * @param response
     * @param mp
     */
    @GetMapping("/{themeName}/sitemap.xml")
    public void siteMapIndex(@PathVariable("themeName") String themeName,
            HttpServletResponse response, ModelMap mp) {
        webPageService.siteMapIndex(themeName, response, mp);
    }


    /**
     * 站点地图
     * @param themeName
     * @param response
     * @param mp
     */
    @GetMapping("/site/{themeName}/{file:.+}")
    public void siteMap(@PathVariable("themeName") String themeName,
            @PathVariable("file") String fileName,
            HttpServletResponse response, ModelMap mp) {
        webPageService.siteMap(themeName, fileName, response, mp);
    }

}
