package com.oly.cms.general.cache;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.oly.cms.common.constant.CmsCacheConstant;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.domain.entity.CmsWeb;
import com.oly.cms.query.service.impl.ThemeSearchServiceImpl;
import com.oly.cms.query.service.impl.WebSearchServiceImpl;

@Service
@CacheConfig(cacheNames = CmsCacheConstant.THEME_CACHE_KEY_PREFIX)
public class GeneralThemeCacheService {
    @Autowired
    private ThemeSearchServiceImpl themeSearchService;

    @Autowired
    private WebSearchServiceImpl webSearchService;

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsTheme selectCmsThemeItem(String s) {

        return themeSearchService.selectCmsThemeByWt(s);

    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsTheme selectCmsThemeItem(String webName, String themeName) {

        return themeSearchService.selectCmsThemeByWt(webName, themeName);

    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public Map<String, CmsTheme> mapTheme(String olyCode) {
        return themeSearchService.listCmsTheme(olyCode).stream()
                .collect(Collectors.toMap(CmsTheme::getThemeName, Function.identity(), (key1, key2) -> key2));

    }

    @Cacheable(keyGenerator = "myKeyGenerator")
    public CmsWeb selectCmsWebItem(String webName) {
        return webSearchService.selectCmsWebByWebName(webName);

    }
}
