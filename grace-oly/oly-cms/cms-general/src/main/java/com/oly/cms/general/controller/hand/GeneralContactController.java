package com.oly.cms.general.controller.hand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.common.domain.entity.CmsContact;
import com.oly.cms.general.service.IGeneralContactService;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.SecurityUtils;

@RestController
@RequestMapping
public class GeneralContactController extends BaseController {

    @Autowired
    private IGeneralContactService cmsContactService;

    /**
     * 获取反馈
     * 
     * @param contactId
     * @return
     */
    @GetMapping("/{themeName}/general/contact/getContact/{contactId}")
    public AjaxResult getContact(@PathVariable("contactId") Long contactId) {

        return success(cmsContactService.selectCmsContactById(contactId));
    }

    /**
     * 添加反馈
     * 支持匿名添加反馈
     *
     * @param cmsContact
     * @return
     */
    @PostMapping("/{themeName}/general/contact/addContact")
    public AjaxResult getContact(CmsContact cmsContact) {
        cmsContact.setCreateBy(SecurityUtils.getUsernameNull());
        cmsContact.setVisible(0);
        cmsContact.setContactType(cmsContact.getContactType() == null ? 0 : cmsContact.getContactType());
        return success(cmsContactService.insertCmsContact(cmsContact));
    }

}
