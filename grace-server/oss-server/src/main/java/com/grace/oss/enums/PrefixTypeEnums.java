package com.grace.oss.enums;

public enum PrefixTypeEnums {
    NO_COVER,
    COVER,
    TIMEYMD,
    TIMEYM
}
