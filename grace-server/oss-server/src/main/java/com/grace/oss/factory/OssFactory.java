package com.grace.oss.factory;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.grace.oss.enums.OssEnum;
import com.grace.oss.enums.properties.OssConfigProperties;
import com.grace.oss.service.OssHandler;
import com.grace.oss.service.impl.NativeOssHandler;
import com.grace.oss.service.impl.QiNiuOssHander;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class OssFactory {
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private SysSearchConfigServiceImpl configService;
    private Map<String, OssHandler> fileRepoMap = new HashMap<>();

    @PostConstruct
    public void init() {
        fileRepoMap.put(OssEnum.OSS_TYPE_NATIVE.getValue(), applicationContext.getBean(NativeOssHandler.class));
        fileRepoMap.put(OssEnum.OSS_TYPE_QN.getValue(), applicationContext.getBean(QiNiuOssHander.class));
    }

    public OssHandler get() {
        String scheme = configService.selectConfigDefaultValue(OssConfigProperties.OSS_CONFIG_GROUP.getValue(),
                OssConfigProperties.OSS_USED);
        return fileRepoMap.get(scheme);
    }
}
