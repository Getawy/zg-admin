package com.grace.oss.domain;

import com.grace.common.core.domain.BaseEntity;

/**
 * 文件存储对象 oly_oss
 * 
 * @author zg
 * @date 2021-02-26
 */
public class OlyOss extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 文件名 */
    private String fileName;

    /** 文件大小 */
    private Long size;

    /** 唯一路径 */
    private String fk;

    /** 文件类型 */
    private String fileType;

    /** 域名 */
    private String domain;

    /** 储存在哪里 */
    private String ossType;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFk() {
        return fk;
    }

    public void setFk(String fk) {
        this.fk = fk;
    }

    public String getOssType() {
        return ossType;
    }

    public void setOssType(String ossType) {
        this.ossType = ossType;
    }

}
