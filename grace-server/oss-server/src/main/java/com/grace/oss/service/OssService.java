package com.grace.oss.service;

import java.util.List;

import com.grace.oss.domain.OlyOss;
import com.grace.oss.mapper.OssServerMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OssService {

    @Autowired
    private OssServerMapper ossMapper;

    /**
     * 查询文件存储
     * 
     * @param id 文件存储ID
     * @return 文件存储
     */
    public OlyOss selectOlyOssById(Long id) {
        return ossMapper.selectOlyOssById(id);
    }

    /**
     * 查询文件存储列表
     * 
     * @param olyOss 文件存储
     * @return 文件存储集合
     */
    public List<OlyOss> selectOlyOssList(OlyOss olyOss) {
        return ossMapper.selectOlyOssList(olyOss);
    }

}