package com.grace.oss.utils;

import com.grace.common.utils.DateUtils;
import com.grace.common.utils.PinYinUtils;
import com.grace.common.utils.StringUtils;
import com.grace.common.utils.file.FileUploadUtils;
import com.grace.common.utils.file.MimeTypeUtils;
import com.grace.common.utils.uuid.Seq;
import com.grace.oss.enums.PrefixTypeEnums;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.web.multipart.MultipartFile;

public class OssUtils {
    private OssUtils() {
    }

    /**
     * 获取文件名,个别浏览器文件名是全路径 edge包含全路径
     * 
     * @return
     */
    public static String getFileName(String originalBasename) {

        if (originalBasename.contains("\\")) {
            originalBasename = originalBasename.substring(originalBasename.lastIndexOf('\\') + 1);
        }
        return originalBasename;
    }

    /**
     * 获取文件真实名
     * 
     * @param file
     * @return
     */
    public static String realFileName(MultipartFile file) {
        // 后缀
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        // 文件名
        String baseFileName = FilenameUtils.getBaseName(file.getOriginalFilename());
        if (StringUtils.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(file.getContentType());
        }
        return baseFileName + "." + extension;
    }

    public static boolean checkAllowFileName(String name) {
        // 禁止目录上跳级别
        if (StringUtils.contains(name, "..")) {
            return false;
        }
        return true;
    }

    /**
     * 获取文件类型表达
     * 
     * @param suffix
     * @return
     */
    public static String getFileTypeExtension(String suffix) {
        if (ArrayUtils.contains(MimeExtensionEnum.IMAGE.getType(), suffix)) {
            return MimeExtensionEnum.IMAGE.name().toLowerCase();
        } else if (ArrayUtils.contains(MimeExtensionEnum.DOCUMENT.getType(), suffix)) {
            return MimeExtensionEnum.DOCUMENT.name().toLowerCase();
        } else if (ArrayUtils.contains(MimeExtensionEnum.COMPRESS.getType(), suffix)) {
            return MimeExtensionEnum.COMPRESS.name().toLowerCase();
        } else if (ArrayUtils.contains(MimeExtensionEnum.MUSIC.getType(), suffix)) {
            return MimeExtensionEnum.MUSIC.name().toLowerCase();
        } else if (ArrayUtils.contains(MimeExtensionEnum.VIDEO.getType(), suffix)) {
            return MimeExtensionEnum.VIDEO.name().toLowerCase();
        } else {
            return "file";
        }
    }

    public static final String getKey(MultipartFile file, PrefixTypeEnums prefixTypeEnums,boolean chan) {
        String valueKey;
        String fileName=FilenameUtils.getBaseName(file.getOriginalFilename());
        if (chan) {
            fileName = PinYinUtils.converterToFirstSpell(fileName);
        }
        switch (prefixTypeEnums) {
            case NO_COVER:
                valueKey = StringUtils.format("{}_{}.{}", fileName,
                        Seq.getId(Seq.uploadSeqType),
                        FileUploadUtils.getExtension(file));
                break;
            case TIMEYMD:
                valueKey = StringUtils.format("{}/{}_{}.{}", DateUtils.datePath(),
                fileName,Seq.getId(Seq.uploadSeqType),
                        FileUploadUtils.getExtension(file));
                break;
            case TIMEYM:
                valueKey = StringUtils.format("{}/{}_{}.{}", DateUtils.datePathYn(),
                fileName, Seq.getId(Seq.uploadSeqType),
                        FileUploadUtils.getExtension(file));
                break;
            default:
                valueKey = StringUtils.format("{}.{}",fileName,
                        FileUploadUtils.getExtension(file));
                break;
        }
       
        return valueKey;
    }

}
