package com.grace.mail.domain;

public enum MailUsedEnums {
    // 正常邮件
    NORMAL,
    // 发送验证码
    CODE,
    // 注册
    REGISTER,
    // 登录
    LOGIN,
    // 备份
    BACK_UP,
    // 评论
    COMMENT
}
