package com.grace.redis.config;

/**
 * 缓存类型
 * 使用注解时
 */
public class CacheTypeConstant {
    //单个缓存
    public static final String ITEM_PREFIX = "Item";
    //依赖缓存前缀 比如阅读量变化了,新发布文章,点赞量变化了缓存更新
    public static final String DEPEND_PREFIX = "Depend";
    
}
