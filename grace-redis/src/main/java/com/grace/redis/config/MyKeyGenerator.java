package com.grace.redis.config;

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.ClassUtils;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter.Feature;

@Component
public class MyKeyGenerator implements KeyGenerator {
    /**
     * 获取缓存的key
     * target.method(params)
     * item-> prefix:item(..参数).包名.类名.方法名
     * depend-> prefix:depend(..参数).包名.类名.方法名
     * 
     * @param target
     * @param method
     * @param params
     * @return
     */
    @Override
    public Object generate(Object target, Method method, Object... params) {
        StringBuilder strBuilder = new StringBuilder(30);
        if (params.length > 0) {
            //方法名以Item结尾
            if (method.getName().endsWith(CacheTypeConstant.ITEM_PREFIX)) {
                strBuilder.append(CacheTypeConstant.ITEM_PREFIX);
            } else {
                strBuilder.append(CacheTypeConstant.DEPEND_PREFIX);
            }
            strBuilder.append('(');
            // 参数值
            for (int index = 0; index < params.length; index++) {
                Object object = params[index];
                try {
                    if (isSimpleValueType(object.getClass())) {
                        strBuilder.append(object);
                    } else {
                        strBuilder.append(toJson(object).hashCode());
                    }
                    if (index != params.length - 1) {
                        strBuilder.append(',');
                    }

                } catch (Exception e) {
                }
            }
            strBuilder.append(")_");
            // 类名
            strBuilder.append(target.getClass().getCanonicalName());
            strBuilder.append('.');
            // 方法名
            strBuilder.append(method.getName());
        }
        return strBuilder.toString();
    }

    /**
     * 判断是否是简单值类型.包括：基础数据类型、CharSequence、Number、Date、URL、URI、Locale、Class;
     *
     * @param clazz
     * @return
     */
    public static boolean isSimpleValueType(Class<?> clazz) {
        return (ClassUtils.isPrimitiveOrWrapper(clazz) || clazz.isEnum() || CharSequence.class.isAssignableFrom(clazz)
                || Number.class.isAssignableFrom(clazz) || Date.class.isAssignableFrom(clazz) || URI.class == clazz
                || URL.class == clazz || Locale.class == clazz || Class.class == clazz);
    }

    /**
     * Java对象序列化为JSON字符串
     *
     * @param obj Java对象
     * @return json字符串
     */
    public static String toJson(Object obj) {
        return JSON.toJSONString(obj, Feature.WriteMapNullValue);
    }

}