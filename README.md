<h4 align="center">基于SpringBoot+Vue3的内容管理系统</h4>

## 系统说明

本系统基于若依前后端分离二次开发的内容管理系统，为了更好管理后台，引入配置组，同时通过配置组方式可视化管理后台。
在原有基础添加如下功能：
* 动态资源权限加载可选配置，字典
* 扩展储存，储存方面统一接口，文件可视化管理，方便后续的维护以及扩展。
* 邮件扩展，邮件支持在线配置，发送，支持多种类型邮件发送，支持定时发送。
* 用户端后端采用Spring Boot、Spring Security、Redis、Thymeleaf。
* 多站点，支持后台统一站点管理多站点，一个站点可运行多个网站（主题）
* 用户展示方面，支持按需打包。
* 栏目，前端菜单管理，支持多级树，可以应付复杂的导航，已支持额外SEO字段。
* 分类，支持多级树，完全可以应付复杂分类，同时支持多种不同分类标识，已支持额外SEO字段。
* 文章，支持不同类型，特色推荐，多关键字的文章发布，支持额外SEO字段。
* 链接，支持多级树，方便做导航类网站。
* 文章，包含评论，浏览记录，评分，收藏等。
* 反馈， 支持不同类型反馈。

## 结构说明

```
ZG-ADMIN
├─bin
├─doc
├─grace-admin 后台启动入口
├─grace-common 通用部分
├─grace-flyway 管理数据库
├─grace-framework 框架核心
├─grace-generator  代码生成
├─grace-mybatis    操作数据库
├─grace-oly   oly聚合
│  └─oly-cms   内容聚合
│      ├─cms-admin  内容管理
│      ├─cms-comment 评论
│      ├─cms-common  内容通用工具
│      ├─cms-general 内容通用入口 前台
│      ├─cms-hander  内容控制 非单站点 可选
│      ├─cms-deal   通用操作  前后台公用 非单站点
│      └─cms-web  内容前台启动入口
├─grace-quartz  定时任务
├─grace-redis    redis缓存
├─grace-server    系统服务
│  ├─mail-server  邮件服务
│  └─oss-server   储存服务
├─grace-system    系统访问
│  ├─system-core  系统代码
│  ├─system-current  通用部分
│  └─system-user  用户相关
├─sql
```

具体代码结构请自行研究

## 支持

- 多页网站开发
- 单页站点开发
- 小程序后台开发
- 等

## 主题制作

需要懂一些Thymeleaf的语法知识，主题支持动态配置，可玩性比较强

## 系统启动

- 启动之前请确保安装Redis以及Mysql
- 分离管理前端地址[zg-vue3](https://gitee.com/Getawy/zg-vue3)
- 分离后台位于grace-admin模块下的GraceApplication.java
- 用户端grace-oly/oly-cms/cms-web模块下的ZhiGeWebApplication.java,主题目前需要自己制作或者自行安装示例主题
- 为了系统简洁，演示主题未包含至cms-web,[示例主题地址](https://gitee.com/Getawy/themes)

## 其他说明

* 系统支持开箱即用，需要手动配置文件

  # 优先级如下:
  1. 第一种是在执行命令的目录下建config文件夹，然后把配置文件放到这个文件夹下。(在jar包的同一个目录下建config文件夹，执行命令需要在jar包所在目录下才行)
  2. 第二种是直接把配置文件放到jar包的同级目录
  3. 第三种在classpath下建一个config文件夹，然后把配置文件放进去。
  4. 第四种是在classpath下直接放配置文件。

  springboot默认是优先读取它本身同级目录下的一个config/application.properties文件的。在src/main/resource文件夹下创建的application.properties文件的优先级是最低的

  所以springboot启动读取外部配置文件，只需要在外面加一层配置文件覆盖默认的即可，不用修改代码

  ```java
  grace:
    # 名称
    name: Grace
    # 版本
    version: 3.8.4
    # 版权年份
    copyrightYear: 2022
    # 服务工作目录
    workServerDir: ${user.home}/.tzg/server/${grace.onlyCode}
    # 本地存储工作目录
    workStorageDir: ${user.home}/.tzg/storage
    # 获取ip地址开关
    addressEnabled: false
    # 验证码类型 math 数字计算 char 字符验证
    captchaType: math
    # 站点唯一编码
    onlyCode: admin
  ```
* 关于储存问题
目前使用本地存储的话，管理后台admin与web未部署在一台服务使用同样的工作目录的话，web端无法读出文件。
