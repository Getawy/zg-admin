package com.grace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * 服务启动入口
 * 启动之前请确保安装redis
 * 
 * @author grace
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
// 基本扫描路径
@ComponentScan(basePackages = { "com.grace", "com.oly", "com.cms" })
public class GraceApplication {
    public static void main(String[] args) {
        SpringApplication.run(GraceApplication.class, args);
        System.out.println(
                "ZZZZZZZZZZZZZZZZZZZ        GGGGGGGGGGGGG\n"
                        + "Z:::::::::::::::::Z     GGG::::::::::::G\n"
                        + "Z:::::::::::::::::Z   GG:::::::::::::::G\n"
                        + "Z:::ZZZZZZZZ:::::Z   G:::::GGGGGGGG::::G\n"
                        + "ZZZZZ     Z:::::Z   G:::::G       GGGGGG\n"
                        + "        Z:::::Z    G:::::G              \n"
                        + "   止  Z:::::Z     G:::::G      戈       \n"
                        + "      Z:::::Z      G:::::G    GGGGGGGGGG\n"
                        + "     Z:::::Z       G:::::G    G::::::::G\n"
                        + "    Z:::::Z        G:::::G    GGGGG::::G\n"
                        + "Z:::::Z        Z:::::::::G        G::::G\n"
                        + "ZZZ:::::Z     ZZZZZ G:::::G       G::::G\n"
                        + "Z::::::ZZZZZZZZ:::Z  G:::::GGGGGGGG::::G\n"
                        + "Z:::::::::::::::::Z   GG:::::::::::::::G\n"
                        + "Z:::::::::::::::::Z     GGG::::::GGG:::G\n"
                        + "ZZZZZZZZZZ  止戈后端启动成功 GGGGGG   GGGG");
    }
}
