package com.grace.web.controller.cms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cms.forum.domain.CmsCollectRecord;
import com.cms.forum.domain.CmsLikeRecord;
import com.cms.forum.domain.CmsNastyRecord;
import com.cms.forum.domain.CmsScoreRecord;
import com.cms.forum.domain.CmsShareRecord;
import com.cms.forum.service.impl.CmsArticleRecordServiceImpl;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.page.TableDataInfo;
import com.grace.common.utils.poi.ExcelUtil;
import com.oly.cms.common.domain.entity.CmsArticleCount;
import com.oly.cms.common.domain.entity.CmsLookRecord;

/**
 * 文章统计Controller
 * 
 */
@Controller
@RequestMapping("/cms/articleCount")
public class CmsArticleRecordController extends BaseController {

    @Autowired
    private CmsArticleRecordServiceImpl recordService;

    /**
     * 
     * @param cmsArticleCount
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleCount:list')")
    @GetMapping("/article")
    @ResponseBody
    public TableDataInfo list(CmsArticleCount cmsArticleCount) {
        startPage();
        List<CmsArticleCount> list = recordService.listCmsArticleCount(cmsArticleCount);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('cms:articleCount:export')")
    @GetMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsArticleCount cmsArticleCount) {
        List<CmsArticleCount> list = recordService.listCmsArticleCount(cmsArticleCount);
        ExcelUtil<CmsArticleCount> util = new ExcelUtil<CmsArticleCount>(CmsArticleCount.class);
        return util.exportExcel(list, "articleCount");
    }

    /**
     * 
     * @param lookRecord
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleLook:list')")
    @GetMapping("/look")
    @ResponseBody
    public TableDataInfo lookRecord(CmsLookRecord lookRecord) {
        startPage();
        List<CmsLookRecord> list = recordService.listLookRecord(lookRecord);
        return getDataTable(list);

    }

    /**
     * 
     * @param likeRecord
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleLike:list')")
    @GetMapping("/like")
    @ResponseBody
    public TableDataInfo likeRecord(CmsLikeRecord likeRecord) {
        startPage();
        List<CmsLikeRecord> list = recordService.listLikeRecord(likeRecord);
        return getDataTable(list);
    }

    /**
     * 
     * @param collectRecord
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleCollect:list')")
    @GetMapping("/collect")
    @ResponseBody
    public TableDataInfo collectRecord(CmsCollectRecord collectRecord) {
        startPage();
        List<CmsCollectRecord> list = recordService.listCollectRecord(collectRecord);
        return getDataTable(list);
    }

    /**
     * 
     * @param shareRecord
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleShare:list')")
    @GetMapping("/share")
    @ResponseBody
    public TableDataInfo shareRecord(CmsShareRecord shareRecord) {
        startPage();
        List<CmsShareRecord> list = recordService.listShareRecord(shareRecord);
        return getDataTable(list);
    }

    /**
     * 
     * @param nastyRecord
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleNasty:list')")
    @GetMapping("/nasty")
    @ResponseBody
    public TableDataInfo nastyRecord(CmsNastyRecord nastyRecord) {
        startPage();
        List<CmsNastyRecord> list = recordService.listNastyRecord(nastyRecord);
        return getDataTable(list);
    }

    /**
     * 
     * @param scoreRecord
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:articleScore:list')")
    @GetMapping("/score")
    @ResponseBody
    public TableDataInfo scoreRecord(CmsScoreRecord scoreRecord) {
        startPage();
        List<CmsScoreRecord> list = recordService.listScoreRecord(scoreRecord);
        return getDataTable(list);
    }

}
