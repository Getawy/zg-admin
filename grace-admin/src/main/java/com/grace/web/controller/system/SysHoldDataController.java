package com.grace.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.enums.BusinessType;
import com.grace.system.domain.SysHoldData;
import com.grace.system.service.ISysHoldDataService;
import com.grace.common.utils.poi.ExcelUtil;
import com.grace.common.core.page.TableDataInfo;

/**
 * 资源数据映射Controller
 * 
 * @author grace
 * @date 2023-09-07
 */
@RestController
@RequestMapping("/system/hold")
public class SysHoldDataController extends BaseController
{
    @Autowired
    private ISysHoldDataService sysHoldDataService;

    /**
     * 查询资源数据映射列表
     */
    @PreAuthorize("@ss.hasPermi('system:hold:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysHoldData sysHoldData)
    {
        startPage();
        List<SysHoldData> list = sysHoldDataService.selectSysHoldDataList(sysHoldData);
        return getDataTable(list);
    }

    /**
     * 导出资源数据映射列表
     */
    @PreAuthorize("@ss.hasPermi('system:hold:export')")
    @Log(title = "资源数据映射", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysHoldData sysHoldData)
    {
        List<SysHoldData> list = sysHoldDataService.selectSysHoldDataList(sysHoldData);
        ExcelUtil<SysHoldData> util = new ExcelUtil<SysHoldData>(SysHoldData.class);
        util.exportExcel(response, list, "资源数据映射数据");
    }

    /**
     * 获取资源数据映射详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:hold:query')")
    @GetMapping(value = "/{holdType}/{holdCode}")
    public AjaxResult getInfo(@PathVariable("holdType") Integer holdType,@PathVariable("holdCode") String holdCode)
    {   SysHoldData sysHoldData=new SysHoldData();
        sysHoldData.setHoldType(holdType);
        sysHoldData.setHoldCode(holdCode);
        return success(sysHoldDataService.selectSysHoldData(sysHoldData));
    }

    /**
     * 新增资源数据映射
     */
    @PreAuthorize("@ss.hasPermi('system:hold:add')")
    @Log(title = "资源数据映射", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysHoldData sysHoldData)
    {   
        return toAjax(sysHoldDataService.insertSysHoldData(sysHoldData));
    }

    /**
     * 修改资源数据映射
     */
    @PreAuthorize("@ss.hasPermi('system:hold:edit')")
    @Log(title = "资源数据映射", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysHoldData sysHoldData)
    {
        return toAjax(sysHoldDataService.updateSysHoldData(sysHoldData));
    }

    /**
     * 删除资源数据映射
     */
    @PreAuthorize("@ss.hasPermi('system:hold:remove')")
    @Log(title = "资源数据映射", businessType = BusinessType.DELETE)
	@DeleteMapping("/{holdType}/{holdCode}")
    public AjaxResult remove(@PathVariable Integer holdType,@PathVariable String holdCode)
    {   SysHoldData sysHoldData=new SysHoldData();
        sysHoldData.setHoldType(holdType);
        sysHoldData.setHoldCode(holdCode);
        return toAjax(sysHoldDataService.deleteSysHoldData(sysHoldData));
    }
}
