package com.grace.web.controller.monitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.common.constant.CmsCacheConstant;
import com.grace.common.constant.CacheConstants;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.utils.StringUtils;
import com.grace.system.domain.SysCache;

/**
 * 缓存监控
 * 
 * @author grace
 */
@RestController
@RequestMapping("/monitor/cache")
public class CacheController {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private final static List<SysCache> sysCaches = new ArrayList<SysCache>();
    {
        sysCaches.add(new SysCache(CacheConstants.LOGIN_TOKEN_KEY, "用户信息"));
        sysCaches.add(new SysCache(CacheConstants.SYS_CONFIG_KEY, "配置信息"));
        sysCaches.add(new SysCache(CacheConstants.SYS_DICT_KEY, "数据字典"));
        sysCaches.add(new SysCache(CacheConstants.CAPTCHA_CODE_KEY, "验证码"));
        sysCaches.add(new SysCache(CacheConstants.REPEAT_SUBMIT_KEY, "防重提交"));
        sysCaches.add(new SysCache(CacheConstants.RATE_LIMIT_KEY, "限流处理"));
        sysCaches.add(new SysCache(CacheConstants.PWD_ERR_CNT_KEY, "密码错误次数"));
    }

    private final static List<SysCache> cmsCaches = new ArrayList<SysCache>();
    {
        cmsCaches.add(new SysCache(CmsCacheConstant.CATEGORIES_CACHE_KEY_PREFIX, "分类缓存"));
        cmsCaches.add(new SysCache(CmsCacheConstant.THEME_CACHE_KEY_PREFIX, "主题缓存"));
        cmsCaches.add(new SysCache(CmsCacheConstant.ARTICLE_CACHE_KEY_PREFIX, "文章缓存"));
        cmsCaches.add(new SysCache(CmsCacheConstant.LINKS_CACHE_KEY_PREFIX, "外链缓存"));
        cmsCaches.add(new SysCache(CmsCacheConstant.MENU_CACHE_KEY_PREFIX, "导航缓存"));
        cmsCaches.add(new SysCache(CmsCacheConstant.UNION_CACHE_KEY_PREFIX, "联盟缓存"));
        cmsCaches.add(new SysCache(CmsCacheConstant.COMMENT_CACHE_KEY_PREFIX, "评论缓存"));

    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping()
    public AjaxResult getInfo() throws Exception {
        Properties info = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info());
        Properties commandStats = (Properties) redisTemplate
                .execute((RedisCallback<Object>) connection -> connection.info("commandstats"));
        Object dbSize = redisTemplate.execute((RedisCallback<Object>) connection -> connection.dbSize());

        Map<String, Object> result = new HashMap<>(3);
        result.put("info", info);
        result.put("dbSize", dbSize);

        List<Map<String, String>> pieList = new ArrayList<>();
        if (commandStats != null) {
            commandStats.stringPropertyNames().forEach(key -> {
                Map<String, String> data = new HashMap<>(2);
                String property = commandStats.getProperty(key);
                data.put("name", StringUtils.removeStart(key, "cmdstat_"));
                data.put("value", StringUtils.substringBetween(property, "calls=", ",usec"));
                pieList.add(data);
            });
        }
        result.put("commandStats", pieList);
        return AjaxResult.success(result);
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/getNames")
    public AjaxResult cache() {
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("sysCaches", sysCaches);
        ajaxResult.put("cmsCaches", cmsCaches);
        return ajaxResult;
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/getKeys/{cacheName}")
    public AjaxResult getCacheKeys(@PathVariable String cacheName) {
        Set<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        return AjaxResult.success(new TreeSet<>(cacheKeys));
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/getValue/{cacheName}/{cacheKey}")
    public AjaxResult getCacheValue(@PathVariable String cacheName, @PathVariable String cacheKey) {
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        SysCache sysCache = new SysCache(cacheName, cacheKey, cacheValue);
        return AjaxResult.success(sysCache);
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @DeleteMapping("/clearCacheName/{cacheName}")
    public AjaxResult clearCacheName(@PathVariable String cacheName) {
        Collection<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        redisTemplate.delete(cacheKeys);
        return AjaxResult.success();
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @DeleteMapping("/clearCacheKey/{cacheKey}")
    public AjaxResult clearCacheKey(@PathVariable String cacheKey) {
        redisTemplate.delete(cacheKey);
        return AjaxResult.success();
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @DeleteMapping("/clearCacheAll/{type}")
    public AjaxResult clearCacheAll(@PathVariable("type") String type) {
        Collection<String> cacheKeys = redisTemplate.keys(type+"*");
        redisTemplate.delete(cacheKeys);
        return AjaxResult.success();
    }
}
