package com.grace.web.controller.cms;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.admin.service.ICmsContactService;
import com.oly.cms.common.domain.entity.CmsContact;

import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.poi.ExcelUtil;
import com.grace.common.core.page.TableDataInfo;

/**
 * 反馈|建议Controller
 * 
 * @author zg
 * @date 2022-11-13
 */
@RestController
@RequestMapping("/cms/contact")
public class CmsContactController extends BaseController {
    @Autowired
    private ICmsContactService cmsContactService;

    /**
     * 查询反馈|建议列表
     */
    @PreAuthorize("@ss.hasPermi('cms:contact:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsContact cmsContact) {
        startPage();
        List<CmsContact> list = cmsContactService.listCmsContact(cmsContact);
        return getDataTable(list);
    }

    /**
     * 导出反馈|建议列表
     */
    @PreAuthorize("@ss.hasPermi('cms:contact:export')")
    @Log(title = "反馈|建议", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsContact cmsContact) {
        List<CmsContact> list = cmsContactService.listCmsContact(cmsContact);
        ExcelUtil<CmsContact> util = new ExcelUtil<CmsContact>(CmsContact.class);
        util.exportExcel(response, list, "反馈|建议数据");
    }

    /**
     * 获取反馈|建议详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:contact:query')")
    @GetMapping(value = "/{contactId}")
    public AjaxResult getInfo(@PathVariable("contactId") Long contactId) {
        return success(cmsContactService.selectCmsContactById(contactId));
    }

    /**
     * 新增反馈|建议
     */
    @PreAuthorize("@ss.hasPermi('cms:contact:add')")
    @Log(title = "反馈|建议", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsContact cmsContact) {
        cmsContact.setCreateBy(getUsername());
        return toAjax(cmsContactService.insertCmsContact(cmsContact));
    }

    /**
     * 修改反馈|建议
     */
    @PreAuthorize("@ss.hasPermi('cms:contact:edit')")
    @Log(title = "反馈|建议", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsContact cmsContact) {
        cmsContact.setHandler(getUsername());
        return toAjax(cmsContactService.updateCmsContact(cmsContact));
    }

    /**
     * 删除反馈|建议
     */
    @PreAuthorize("@ss.hasPermi('cms:contact:remove')")
    @Log(title = "反馈|建议", businessType = BusinessType.DELETE)
    @DeleteMapping("/{contactIds}")
    public AjaxResult remove(@PathVariable Long[] contactIds) {
        return toAjax(cmsContactService.deleteCmsContactByIds(contactIds));
    }
}
