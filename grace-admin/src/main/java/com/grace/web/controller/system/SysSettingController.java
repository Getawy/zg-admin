package com.grace.web.controller.system;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.domain.entity.SysDept;
import com.grace.common.enums.BusinessType;
import com.grace.system.service.ISysDeptService;
import com.grace.system.service.ISysPostService;
import com.grace.system.service.ISysRoleService;
import com.grace.system.service.impl.SysConfigServiceImpl;

/**
 * 设置控制器
 * 
 * @author grace
 */
@RestController
@RequestMapping("/system/setting")
public class SysSettingController extends BaseController {

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private SysConfigServiceImpl sysConfigService;

    /**
     * 修改保存参数配置 通过key批量保存 默认设置为字符串类型
     */

    @Log(title = "系统配置", businessType = BusinessType.UPDATE)
    @PostMapping("/updateConfig")
    @PreAuthorize("@ss.hasPermi('system:setting:config')")
    public AjaxResult updateConfig(@RequestBody Map<String, Object> mp) {
        mp.put("configGroup", "sysConfig");
        int num = sysConfigService.updatesByMap(mp, getUsername());
        return toAjax(num);
    }

    /**
     * 选择部门树
     * 
     * @param dept
     */
    @GetMapping("/selectOptions")
    public AjaxResult selectOptions() {
        SysDept dept = new SysDept();
        AjaxResult ajax = AjaxResult.success();
        ajax.put("deptTreeList", deptService.selectDeptTreeList(dept));
        ajax.put("roleList", roleService.selectRoleAll());
        ajax.put("postList", postService.selectPostAll());
        return ajax;
    }

}
