package com.grace.web.controller.cms;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.domain.TreeSelect;
import com.grace.common.core.page.TableDataInfo;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.TreeUtils;
import com.grace.common.utils.poi.ExcelUtil;
import com.oly.cms.admin.model.param.ArticleBenchParam;
import com.oly.cms.admin.service.ICmsArticleService;
import com.oly.cms.admin.service.ICmsCategoryService;
import com.oly.cms.admin.service.ICmsThemeService;
import com.oly.cms.common.domain.VisibleParam;
import com.oly.cms.common.domain.entity.CmsArticle;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.domain.vo.ArticleVo;
import com.oly.cms.common.enums.CategoryNodeTypeEnums;
import com.oly.cms.query.model.param.CategorySearchParam;

/**
 * 文章Controller
 * 
 * @author zhige
 * @date 2022-11-20
 */
@RestController
@RequestMapping("/cms/article")
public class CmsArticleController extends BaseController {

    @Autowired
    private ICmsArticleService cmsArticleService;

    @Autowired
    private ICmsCategoryService cmsCategoryService;

    @Autowired
    private ICmsThemeService cmsThemeService;

    /**
     * 查询文章列表
     * 
     * @param cmsArticle
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(ArticleVo cmsArticle) {
        startPage();
        List<CmsArticle> list = cmsArticleService.listCmsArticle(cmsArticle);
        return getDataTable(list);
    }

    /**
     * 查询文章列表
     * 依据权限查询
     */
    @PreAuthorize("@ss.hasPermi('theme:permssion:#themeName')")
    @GetMapping("/list/{themeName}")
    public TableDataInfo listPermssion(ArticleVo cmsArticle, @PathVariable("themeName") String themeName) {
        startPage();
        List<CmsArticle> list = cmsArticleService.listCmsArticle(cmsArticle);
        return getDataTable(list);
    }

    /**
     * 查询分类树列表
     */
    @PreAuthorize("@ss.hasPermi('cms:article:list')")
    @GetMapping("/categoryTree")
    public AjaxResult categoryTree(CategorySearchParam param) {
        List<CmsCategory> list = cmsCategoryService.listCmsCategory(param);
        List<TreeSelect> ts = list.stream().map(v -> {
            return new TreeSelect(v.getParentId(), v.getCategoryId(), v.getCategoryName(), v.getVisible());
        }).collect(Collectors.toList());
        return success(TreeUtils.buildTree(ts));
    }

    /**
     * 查询表标签列表
     */
    @PreAuthorize("@ss.hasPermi('cms:article:list')")
    @GetMapping("/listTag")
    public AjaxResult listTag(CategorySearchParam param) {
        List<CmsCategory> list = cmsCategoryService.listCmsCategory(param);
        return success(list);
    }

    /**
     * 主题Map
     */
    @PreAuthorize("@ss.hasPermi('cms:article:list')")
    @GetMapping("/themeMap")
    public AjaxResult themeMap() {
        List<CmsTheme> list = cmsThemeService.listCmsTheme(null);
        return success(list.stream().collect(Collectors.groupingBy(CmsTheme::getWebName)));
    }

    /**
     * 验证文章
     */
    @PreAuthorize("@ss.hasPermi('cms:article:add')")
    @GetMapping("/check")
    public AjaxResult articleCheck(CmsArticle cmsArticle) {
        return AjaxResult.success("check", cmsArticleService.checkArticleUnique(cmsArticle));
    }

    /**
     * 批量文章顶置操作
     * 
     * @param articleIds
     * @return
     */
    @PutMapping("/batchArticleTop/{articleIds}/{articleTop}")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('cms:article:edit')")
    public AjaxResult batchArticleTop(@PathVariable Long[] articleIds, @PathVariable int articleTop) {
        ArticleBenchParam articleBenchParam = new ArticleBenchParam();
        articleBenchParam.setArticleIds(articleIds);
        articleBenchParam.setArticleTop(articleTop);
        articleBenchParam.setUpdateBy(getUsername());
        return toAjax(cmsArticleService.batchArticleTop(articleBenchParam));
    }

    /**
     * 文章状态操作
     * 
     * @param articleIds
     * @param visible
     * @return
     */
    @PutMapping("/batchArticleVisible/{articleIds}/{visible}")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('cms:article:edit')")
    public AjaxResult batchArticleVisible(@PathVariable Long[] articleIds, @PathVariable int visible) {
        ArticleBenchParam articleBenchParam = new ArticleBenchParam();
        articleBenchParam.setArticleIds(articleIds);
        articleBenchParam.setVisible(visible);
        articleBenchParam.setUpdateBy(getUsername());
        return toAjax(cmsArticleService.batchArticleVisible(articleBenchParam));
    }

    /**
     * 分类批量关联文章
     * 
     * @param articleIds
     * @return
     */
    @PutMapping("/batchArticleCategory/{articleIds}/{categoryIds}")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('cms:article:edit')")
    public AjaxResult batchArticleCategory(@PathVariable Long[] articleIds, @PathVariable Long[] categoryIds) {
        ArticleVo articleVo = new ArticleVo();
        articleVo.setCategoryIds(categoryIds);
        articleVo.setUpdateBy(getUsername());
        for (Long articleId : articleIds) {
            articleVo.setArticleId(articleId);
            cmsArticleService.batchArticleCategory(articleVo);
        }
        return toAjax(articleIds.length);
    }

    /**
     * 导出文章列表
     */
    @PreAuthorize("@ss.hasPermi('cms:article:export')")
    @Log(title = "文章", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ArticleVo cmsArticle) {
        List<CmsArticle> list = cmsArticleService.listCmsArticle(cmsArticle);
        ExcelUtil<CmsArticle> util = new ExcelUtil<CmsArticle>(CmsArticle.class);
        util.exportExcel(response, list, "文章数据");
    }

    /**
     * 获取文章详细信息
     * 
     * 一篇文章只能属于一个主题
     */
    @PreAuthorize("@ss.hasPermi('cms:article:query')")
    @GetMapping(value = "/{articleId}")
    public AjaxResult getInfo(@PathVariable("articleId") Long articleId) {
        ArticleVo articleVo = cmsArticleService.selectCmsArticleById(articleId);
        VisibleParam vParam = new VisibleParam();
        vParam.setId(articleId);
        List<Long> c = new ArrayList<>(), t = new ArrayList<>();
        List<CmsCategory> categories = cmsCategoryService.listCmsCategoryByArticleId(vParam);
        List<CmsTheme> cmsThemes = null;

        for (CmsCategory item : categories) {
            if (item.getNodeType() == CategoryNodeTypeEnums.CLASSIFY_TRUCK.ordinal()
                    || item.getNodeType() == CategoryNodeTypeEnums.CLASSIFY.ordinal()) {
                c.add(item.getCategoryId());
            } else if (item.getNodeType() == CategoryNodeTypeEnums.TAG.ordinal()) {
                t.add(item.getCategoryId());
            } else if (item.getNodeType() == CategoryNodeTypeEnums.THEME_TRUCK.ordinal()) {
                if (articleVo.getSupportCategoryId() != null) {
                    articleVo.setSupportCategoryId(item.getCategoryId());
                    CmsTheme cmsTheme = new CmsTheme();
                    cmsTheme.setSupportCategoryId(item.getCategoryId());
                    cmsThemes = cmsThemeService.listCmsTheme(cmsTheme);
                }
            }
        }
        articleVo.setCategoryIds(c.toArray(new Long[c.size()]));
        articleVo.setTagIds(t.toArray(new Long[t.size()]));
        AjaxResult success = success(articleVo);
        if (cmsThemes != null) {
            success.put(
                    "theme", cmsThemes.get(0));
        }
        return success;
    }

    /**
     * 新增文章
     */
    @PreAuthorize("@ss.hasPermi('cms:article:add')")
    @Log(title = "文章", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody ArticleVo cmsArticle) {
        cmsArticle.setCreateBy(getUsername());
        return toAjax(cmsArticleService.insertCmsArticle(cmsArticle));
    }

    /**
     * 修改文章
     */
    @PreAuthorize("@ss.hasPermi('cms:article:edit')")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody ArticleVo cmsArticle) {
        cmsArticle.setUpdateBy(getUsername());
        return toAjax(cmsArticleService.updateCmsArticleById(cmsArticle));
    }

    /**
     * 删除文章
     */
    @PreAuthorize("@ss.hasPermi('cms:article:remove')")
    @Log(title = "文章", businessType = BusinessType.DELETE)
    @DeleteMapping("/{articleIds}")
    public AjaxResult remove(@PathVariable Long[] articleIds) {
        return toAjax(cmsArticleService.deleteCmsArticleByIds(articleIds));
    }
}
