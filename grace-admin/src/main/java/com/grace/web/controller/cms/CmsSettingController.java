package com.grace.web.controller.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.common.model.properties.OlyCmsConfigProperties;
import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.enums.BusinessType;
import com.grace.system.service.impl.SysConfigServiceImpl;

/**
 * 内容配置控制器
 */ 
@RestController
@RequestMapping("/cms/setting")
public class CmsSettingController extends BaseController {

    @Autowired
    private SysConfigServiceImpl configService;

    /**
     * 修改保存参数配置
     * 通过key批量保存
     * 默认设置为字符串类型
     * 
     * @param mp
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:setting:edit')")
    @PostMapping("/updateConfig")
    @Log(title = "内容配置", businessType = BusinessType.UPDATE)
    public AjaxResult updateConfig(@RequestBody Map<String, Object> mp) {
        mp.put("configGroup", OlyCmsConfigProperties.CMS_CONFIG_GROUP.defaultValue());
        return toAjax(configService.updatesByMap(mp, getUsername()));
    }

}
