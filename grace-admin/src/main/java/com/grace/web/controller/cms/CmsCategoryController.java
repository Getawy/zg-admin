package com.grace.web.controller.cms;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.domain.TreeSelect;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.TreeUtils;
import com.grace.common.utils.poi.ExcelUtil;
import com.oly.cms.admin.service.ICmsCategoryService;
import com.oly.cms.common.domain.entity.CmsCategory;
import com.oly.cms.common.domain.entity.CmsCategoryTheme;
import com.oly.cms.query.model.param.CategorySearchParam;

/**
 * 分类Controller
 * 
 * @author zhige
 * @date 2022-11-18
 */
@RestController
@RequestMapping("/cms/category")
public class CmsCategoryController extends BaseController {
    @Autowired
    private ICmsCategoryService cmsCategoryService;

    /**
     * 数量很多的情况
     * 下动态加载数据
     * 查询分类列表
     * 除管理员以外
     * 其它用户可依据站点类型查看
     */
    @PreAuthorize("@ss.hasPermi('cms:category:list')")
    @GetMapping("/list")
    public AjaxResult list(CategorySearchParam param) {
        List<CmsCategory> list = cmsCategoryService.listCmsCategory(param);
        return success(list);
    }

    @PreAuthorize("@ss.hasPermi('cms:category:list')")
    @GetMapping("/listCategoryIds")
    public AjaxResult listCategoryIds(CategorySearchParam param) {
        List<Long> ids = cmsCategoryService.listCmsCategory(param).stream().map(category -> category.getCategoryId())
                .collect(Collectors.toList());
        return success(ids);
    }

    /**
     * 导出分类列表
     */
    @PreAuthorize("@ss.hasPermi('cms:category:export')")
    @Log(title = "分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CategorySearchParam param) {
        List<CmsCategory> list = cmsCategoryService.listCmsCategory(param);
        ExcelUtil<CmsCategory> util = new ExcelUtil<CmsCategory>(CmsCategory.class);
        util.exportExcel(response, list, "分类数据");
    }

    /**
     * 获取分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:category:query')")
    @GetMapping(value = "/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId) {
        CategorySearchParam param = new CategorySearchParam();
        param.setCategoryId(categoryId);
        return success(cmsCategoryService.selectCmsCategoryById(param));
    }

    /**
     * 新增分类
     */
    @PreAuthorize("@ss.hasPermi('cms:category:add')")
    @Log(title = "分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody CategorySearchParam param) {
        param.setCreateBy(getUsername());
        return toAjax(cmsCategoryService.insertCmsCategory(param));
    }

    /**
     * 修改分类
     */
    @PreAuthorize("@ss.hasPermi('cms:category:edit')")
    @Log(title = "分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody CategorySearchParam param) {
        param.setUpdateBy(getUsername());
        return toAjax(cmsCategoryService.updateCmsCategory(param));
    }

    /**
     * 查询分类树列表
     * 
     * @param param
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:article:list')")
    @GetMapping("/categoryTree")
    public AjaxResult categoryTree(CategorySearchParam param) {
        List<CmsCategory> list = cmsCategoryService.listCmsCategory(param);
        List<TreeSelect> ts = list.stream().map(v -> {
            return new TreeSelect(v.getParentId(), v.getCategoryId(), v.getCategoryName(), v.getVisible());
        }).collect(Collectors.toList());
        return success(TreeUtils.buildTree(ts));
    }

    /**
     * 删除分类
     */
    @PreAuthorize("@ss.hasPermi('cms:category:remove')")
    @Log(title = "分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{categoryId}")
    public AjaxResult remove(@PathVariable Long categoryId) {
        return toAjax(cmsCategoryService.deleteCmsCategoryById(categoryId));
    }

    /**
     * 新增分类
     */
    @PreAuthorize("@ss.hasPermi('cms:category:add')")
    @Log(title = "分类", businessType = BusinessType.INSERT)
    @PostMapping("/addCategoryTheme/{themeName}/{categoryIds}")
    public AjaxResult addCategoryTheme(@PathVariable String themeName, @PathVariable List<Long> categoryIds) {

        return toAjax(cmsCategoryService.insertCategoryTheme(themeName, categoryIds));
    }

    /**
     * 删除分类
     */
    @PreAuthorize("@ss.hasPermi('cms:category:remove')")
    @Log(title = "分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/removeCategoryTheme/{themeName}/{categoryIds}")
    public AjaxResult removeCategoryTheme(@PathVariable String themeName, @PathVariable List<Long> categoryIds) {
        return toAjax(cmsCategoryService.deleteCategoryTheme(themeName, categoryIds));
    }

    @PreAuthorize("@ss.hasPermi('cms:category:list')")
    @GetMapping("/listCategoryTheme/{categoryId}")
    public AjaxResult listCategoryTheme(@PathVariable("categoryId") Long categoryId) {
        CmsCategoryTheme categoryTheme = new CmsCategoryTheme();
        categoryTheme.setCategoryId(categoryId);
        return success(cmsCategoryService.listCategoryTheme(categoryTheme));
    }

}
