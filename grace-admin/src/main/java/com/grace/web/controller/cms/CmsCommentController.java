package com.grace.web.controller.cms;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cms.forum.comment.domain.CmsComment;
import com.cms.forum.comment.domain.enums.CommentVisibleEnums;
import com.cms.forum.comment.domain.properties.OlyCommentProperties;
import com.cms.forum.comment.domain.vo.CmsCommentVo;
import com.cms.forum.comment.service.ICmsCommentService;
import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.page.TableDataInfo;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.SecurityUtils;
import com.grace.common.utils.ServletUtils;
import com.grace.common.utils.html.EscapeUtil;
import com.grace.common.utils.ip.IpUtils;
import com.grace.common.utils.poi.ExcelUtil;
import com.grace.system.service.impl.SysSearchConfigServiceImpl;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 评论Controller
 * 
 * @author 止戈
 */
@RestController
@RequestMapping("/cms/comment")
public class CmsCommentController extends BaseController {
    @Autowired
    private ICmsCommentService cmsCommentService;
    @Autowired
    private SysSearchConfigServiceImpl configService;

    /**
     * 查询评论列表
     */
    @PreAuthorize("@ss.hasPermi('cms:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsComment cmsComment) {
        startPage();
        List<CmsCommentVo> list = cmsCommentService.listCmsCommentVo(cmsComment);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @PreAuthorize("@ss.hasPermi('cms:comment:export')")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsComment cmsComment) {
        List<CmsCommentVo> list = cmsCommentService.listCmsCommentVo(cmsComment);
        ExcelUtil<CmsCommentVo> util = new ExcelUtil<CmsCommentVo>(CmsCommentVo.class);
        util.exportExcel(response, list, "评论数据");
    }

    /**
     * 获取评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:comment:query')")
    @GetMapping(value = "/{commentId}")
    public AjaxResult getInfo(@PathVariable("commentId") Long commentId) {
        return success(cmsCommentService.selectCmsCommentVoById(commentId));
    }

    /**
     * 回复新增加 类型ID parentId 被回复人name 内容
     */
    @PreAuthorize("@ss.hasPermi('cms:comment:add')")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult addReply(@RequestBody CmsComment cmsComment) {
        String maxSize = configService.selectConfigDefaultValue(
                OlyCommentProperties.COMMENT_CONFIG_GROUP.defaultValue(),
                OlyCommentProperties.COMMENT_MAX_SIZE);
        if (Integer.parseInt(maxSize) < EscapeUtil.clean(cmsComment.getContent()).length()) {
            return AjaxResult.error("内容超出大上限:" + maxSize + "个字符");
        }
        if (cmsComment.getParentId() == null) {
            return AjaxResult.error("主楼ID不能为空");
        }
        if (StringUtils.isEmpty(cmsComment.getReplyBy()) || SecurityUtils.getUsername() == cmsComment.getReplyBy()) {
            return AjaxResult.error("回复人不能为空且自己不能回复自己");
        }
        if (cmsComment.getCommentType() != null) {
            UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
            String ip = IpUtils.getIpAddr();
            cmsComment.setIp(ip);
            cmsComment.setUserSystem(userAgent.getOperatingSystem().getName());
            cmsComment.setFromBy(SecurityUtils.getUsername());
            cmsComment.setUserBower(userAgent.getBrowser().getName());
            cmsComment.setVisible(CommentVisibleEnums.PASS.ordinal());
            return AjaxResult.success(cmsCommentService.insertCmsComment(cmsComment));
        } else {
            return AjaxResult.error("评论类型不能为空!");
        }
    }

    /**
     * 批量
     * 修改评论状态 批量评论ids 状态visible
     * 修改评论级别 批量评论ids 排序orderNum
     * 
     * @param ids
     * @return
     */
    @PutMapping("/batch/{ids}")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('cms:comment:edit')")
    public AjaxResult updateCommentStatus(@PathVariable Long[] ids,
            @RequestBody CmsComment cmsComment) {
        return toAjax(cmsCommentService.batchCmsComment(ids, cmsComment.getVisible(), cmsComment.getOrderNum(),
                cmsComment.getRemark(), SecurityUtils.getUsername()));
    }

    /**
     * 删除评论
     */
    @PreAuthorize("@ss.hasPermi('cms:comment:remove')")
    @Log(title = "评论", businessType = BusinessType.DELETE)
    @DeleteMapping("/{commentIds}")
    public AjaxResult remove(@PathVariable Long[] commentIds) {
        return toAjax(cmsCommentService.deleteCmsCommentByCommentIds(commentIds));
    }
}
