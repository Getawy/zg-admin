package com.grace.web.controller.cms;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.grace.common.annotation.Log;
import com.grace.common.constant.HttpStatus;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.core.page.TableDataInfo;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.StringUtils;
import com.grace.system.domain.SysConfig;
import com.grace.system.service.impl.SysConfigServiceImpl;
import com.oly.cms.admin.service.ICmsThemeService;
import com.oly.cms.admin.service.ICmsWebService;
import com.oly.cms.common.constant.OlySystemConstant;
import com.oly.cms.common.domain.entity.CmsTheme;
import com.oly.cms.common.domain.entity.CmsWeb;
import com.oly.cms.common.enums.ThemeEnabledEnums;
import com.oly.cms.common.model.properties.OlyThemeConfigProperties;
import com.oly.cms.common.model.properties.OlyWebConfigProperties;

//统一主题管理入口
@RestController
@RequestMapping("/cms/theme")
public class CmsThemeController extends BaseController {

    @Autowired
    private ICmsThemeService cmsThemeService;

    @Autowired
    private ICmsWebService webService;

    @Autowired
    private SysConfigServiceImpl sysConfigService;

    private static final Logger logger = LoggerFactory.getLogger(CmsThemeController.class);

    /**
     * 查询主题列表
     * @param cmsTheme
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:theme:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsTheme cmsTheme) {
        startPage();
        List<CmsTheme> list = cmsThemeService.listCmsTheme(cmsTheme);
        return getDataTable(list);
    }

   /**
    * 查询主题
    * @param webName 站点名
    * @param themeName 主题名
    * @return
    */
    @PreAuthorize("@ss.hasPermi('cms:theme:list')")
    @GetMapping("/getTheme/{webName}/{themeName}")
    public AjaxResult getTheme(@PathVariable("webName") String webName,
            @PathVariable("themeName") String themeName) {
        CmsTheme cmsTheme = new CmsTheme();
        cmsTheme.setWebName(webName);
        cmsTheme.setThemeName(themeName);
        return success(cmsThemeService.selectCmsThemeByWt(cmsTheme));
    }

    /**
     * 更新主题
     * @param cmsTheme
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:theme:edit')")
    @Log(title = "主题管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsTheme cmsTheme) {

        return toAjax(cmsThemeService.updateCmsTheme(cmsTheme));
    }

    /**
     * 
     * 获取主题名列表
     * 
     * @param webName
     * @return
     */
    @PreAuthorize("@ss.hasPermi('cms:theme:list')")
    @GetMapping("/listThemeNames/{webName}")
    public AjaxResult listThemeNames(@PathVariable("webName") String webName) {
        CmsTheme cmsTheme = new CmsTheme();
        cmsTheme.setWebName(webName);
        List<String> themeNames = cmsThemeService.listCmsTheme(cmsTheme).stream()
                .map(CmsTheme::getThemeName).collect(Collectors.toList());
        return success(themeNames);
    }

    /**
     * 修改保存参数配置 通过key批量保存 默认设置为字符串类型
     */

    @Log(title = "主题管理", businessType = BusinessType.UPDATE)
    @PostMapping("/updateConfig/{configGroup}")
    @PreAuthorize("@ss.hasPermi('cms:theme:config')")
    public AjaxResult updateConfig(@RequestBody Map<String, Object> mp,
            @PathVariable("configGroup") String configGroup) {
        mp.put("configGroup", configGroup);
        int num = sysConfigService.updatesByMap(mp, getUsername());
        return toAjax(num);
    }

    /**
     * 上传主题
     * 
     * 
     * @param file        文件名
     * @param webName     服务名
     * @param cover       是否覆盖
     * @param request
     * @return
     * @throws Throwable
     */
    @PostMapping("/uploadTheme")
    @PreAuthorize("@ss.hasPermi('cms:theme:install')")
    @Log(title = "主题管理", businessType = BusinessType.UPDATE)
    public AjaxResult themeUpload(@RequestParam("themeFile") MultipartFile file, boolean cover,
            HttpServletRequest request) {
        String[] wt = FilenameUtils.getBaseName(file.getOriginalFilename()).split("_");
        String webName, themeName;
        if (wt.length == 2) {
            webName = wt[0];
            themeName = wt[1];
        } else {
            return error("文件名格式命名错误!");
        }
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在,请先插入站点再进行安装");
        }
        if (file.isEmpty() || !"zip|gz|rar".contains(FilenameUtils.getExtension(file.getOriginalFilename()))) {
            return error("文件不存在或者不支持压缩类型,请上传.zip|.gz|.rar压缩包");
        } else {
            Map<String, String> header = new HashMap<>(1);
            Map<String, String> params = new HashMap<>();
            params.put("fileType", "zip/gz/rar");
            params.put("cover", Boolean.toString(cover));
            header.put("Accept", "application/json");
            header.put("Accept-Charset", "UTF-8");
            AjaxResult aj = this.uploadFile(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/uploadTheme", file,
                    "themeFile",
                    header,
                    params);
            if (aj.get(AjaxResult.CODE_TAG).equals(HttpStatus.SUCCESS)) {
                try {
                    return syncThemeInfo(webName, themeName, cover,
                            cmsWeb.getWebUrl());
                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
            }
            return aj;
        }

    }

    /**
     * 删除主题
     * 
     * @param webName
     * @param themeName
     * @return
     * @throws IOException
     * @throws ClientProtocolException
     */
    @DeleteMapping("/remove/{webName}/{themeName}")
    @PreAuthorize("@ss.hasPermi('cms:theme:remove')")
    @Log(title = "主题管理", businessType = BusinessType.DELETE)
    public AjaxResult removeTheme(@PathVariable("webName") String webName, @PathVariable("themeName") String themeName)
            throws ClientProtocolException {
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在!");
        }
        if (deleteThemeFile(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/deleteTheme",
                themeName).get(AjaxResult.CODE_TAG)
                .equals(HttpStatus.SUCCESS)) {
            // 删除配置参数
            sysConfigService.deleteConfigByGroup(webName + "_" + themeName);
            CmsTheme cmsTheme = new CmsTheme();
            cmsTheme.setWebName(webName);
            cmsTheme.setThemeName(themeName);
            // 删除主题和主题关联的文章
            cmsThemeService.deleteCmsThemeByTn(cmsTheme);
            return success();
        }
        return error();
    }

    /**
     * 获取配置表单
     * 
     * @param webName
     * @param themeName
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    @GetMapping("/getThemeConfigForm/{webName}/{themeName}")
    @PreAuthorize("@ss.hasPermi('cms:theme:config')")
    public AjaxResult getThemeConfigForm(@PathVariable("webName") String webName,
            @PathVariable("themeName") String themeName)
            throws ClientProtocolException, IOException {
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在!");
        }
        return this.get(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/getThemeConfigForm/" + themeName);
    }

    // 更新索引文件
    @PostMapping("/updateSiteMapIndex/{webName}/{themeName}")
    @PreAuthorize("@ss.hasPermi('cms:theme:config')")
    public AjaxResult updateSiteMapIndex(@PathVariable("webName") String webName,
            @PathVariable("themeName") String themeName) throws ClientProtocolException {
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在!");
        }
        return this.post(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/siteMapIndex/" + themeName, null);
    }

    /**
     * 同步主题数据
     * 
     * @param webName
     * @param themeName
     * @param cover
     * @param url
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    private AjaxResult syncThemeInfo(String webName, String themeName, boolean cover, String url)
            throws ClientProtocolException, IOException {
        CmsTheme searchTheme = new CmsTheme();
        searchTheme.setWebName(webName);
        searchTheme.setThemeName(themeName);
        CmsTheme s = cmsThemeService.selectCmsThemeByWt(searchTheme);
        if (s != null && !cover) {
            return AjaxResult.success("主题已上传,数据未进更新!");
        } else {
            try {
                // 获取配置Json
                JSONObject con = JSONObject
                        .parseObject(
                                this.get(url + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/getThemeSetting/" + themeName).get(AjaxResult.DATA_TAG)
                                        .toString());
                CmsTheme theme = JSONObject.parseObject(
                        JSON.toJSONString(
                                con.get(OlyThemeConfigProperties.THEME_DATA_PREFIX.defaultValue())),
                        CmsTheme.class);
                theme.setWebName(webName);
                theme.setCreateBy(getUsername());
                theme.setThemeName(themeName);
                if (s != null && cover) {
                    theme.setThemeEnabled(s.getThemeEnabled());
                    cmsThemeService.updateCmsTheme(theme);
                    addThemeConfig(JSON.parseArray(
                            JSON.toJSONString(
                                    con.get(OlyThemeConfigProperties.THEME_CONFIG_PREFIX.defaultValue())),
                            SysConfig.class), themeName, webName);
                    return AjaxResult.success("主题已覆盖上传,数据已经更新!");
                } else {
                    theme.setThemeEnabled(ThemeEnabledEnums.CLOSE.ordinal());
                    cmsThemeService.insertCmsTheme(theme);
                    addThemeConfig(JSON.parseArray(
                            JSON.toJSONString(
                                    con.get(OlyThemeConfigProperties.THEME_CONFIG_PREFIX.defaultValue())),
                            SysConfig.class), themeName, webName);
                    return AjaxResult.success("主题已上传,数据已同步!");
                }
            } catch (Exception e) {
                logger.info("主题上传异常,数据同步失败!" + e.getMessage());
                return AjaxResult.error("主题上传异常,数据同步失败!");
            }

        }

    }

    /**
     * 获取主题树
     * 
     * @param webName
     * @param themeName
     * @return
     * @throws IOException
     * @throws ClientProtocolException
     */
    @GetMapping("/getThemeTree/{webName}")
    @PreAuthorize("@ss.hasPermi('cms:theme:tree')")
    public AjaxResult themeTree(@PathVariable("webName") String webName) throws ClientProtocolException, IOException {
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在!");
        }
        return get(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/getThemeTree/" + webName);
    }

    /**
     * 获取文件内容
     * 
     * @param path
     * @return
     * @throws IOException
     * @throws ClientProtocolException
     */
    @GetMapping("/getThemeFileContent/{webName}")
    public AjaxResult themeContent(@PathVariable("webName") String webName, String path) {
        if (isSupportSuffix(path)) {
            return AjaxResult.error("不支持文件类型！");
        }
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在!");
        }
        return this.get(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/getThemeFileContent?path=" + path);
    }

    @PostMapping("/updateThemeFileContent/{webName}")
    @PreAuthorize("@ss.hasPermi('cms:theme:update')")
    @Log(title = "主题管理", businessType = BusinessType.UPDATE)
    public AjaxResult updateThemeFileContent(@PathVariable("webName") String webName,
            @RequestBody Map<String, String> paMap) {
        String path = paMap.get("path");
        String fileContent = paMap.get("fileContent");
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在,请先插入站点再进行安装");
        } else if (isSupportSuffix(path)) {
            return AjaxResult.error("不支持文件类型！");
        } else {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("fileContent", fileContent));
            params.add(new BasicNameValuePair("path", path));
            try {
                return this.post(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/updateThemeFileContent/" + webName, params);
            } catch (ClientProtocolException e) {
                logger.info("客户端连接异常!" + e);
                return error("客户端连接异常!" + e.getMessage());
            }
        }

    }

    @PostMapping("/themeDownload/{webName}/{themeName}")
    public void themeDownload(@PathVariable("webName") String webName,
            @PathVariable("themeName") String themeName, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        String url = cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/themeDownload/" + webName + "/" + themeName;
        clientDown(request, response, url, themeName);

    }

    /**
     * 同步主题
     * 
     * @throws IOException
     * @throws ClientProtocolException
     */
    @PostMapping("/syncTheme/{webName}")
    public AjaxResult syncTheme(@PathVariable("webName") String webName) throws ClientProtocolException, IOException {
        CmsWeb cmsWeb = webService.selectCmsWebByWebName(webName);
        CmsTheme cmsTheme = new CmsTheme();
        cmsTheme.setWebName(webName);
        if (cmsWeb == null) {
            return error("站点不存在!");
        }
        // 获取当前站点下已经安装的主题
        List<CmsTheme> listThemes = cmsThemeService.listCmsTheme(cmsTheme);
        List<String> upThemeNames = listThemes.stream().map(CmsTheme::getThemeName)
                .collect(Collectors.toList());
        // 获取已经在本地的主题
        List<String> localThemeNames = JSONArray.parseArray(
                this.get(cmsWeb.getWebUrl() + OlySystemConstant.THEME_MANAGE_URL_PREFIX+"/getThemeNames").get(AjaxResult.DATA_TAG).toString(),
                String.class);
        // 取差集
        Collection<String> syncThemeNames = CollectionUtils.subtract(localThemeNames, upThemeNames);
        if (syncThemeNames != null && syncThemeNames.size() > 0) {
            for (String themeName : syncThemeNames) {
                this.syncThemeInfo(webName, themeName, false, cmsWeb.getWebUrl());
            }
            return success("同步成功:" + syncThemeNames.toArray().toString());
        }
        return error("没有需要同步的主题!");

    }

    /**
     *
     * @param request
     * @param response
     * @param urlPath  调用访问下载文件的url(方法)
     * @param webName  站点名
     */
    public void clientDown(HttpServletRequest request, HttpServletResponse response,
            String urlPath, String webName) {

        // 创建默认的httpClient实例.
        CloseableHttpClient httpClient = HttpClients.createDefault();
        OutputStream out = null;
        InputStream in = null;
        try {
            HttpPost httpPost = new HttpPost(urlPath);
            httpPost.setHeader("Content-Disposition", "attachment;filename=" + webName + ".zip");

            // 这里访问server 端（也就是urlPath路径），server下载文件并将文件流回传
            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity entity = httpResponse.getEntity();
            in = entity.getContent();

            long length = entity.getContentLength();
            if (length <= 0) {
                logger.info("下载文件不存在!");
                return;
            }
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Content-Disposition", "attachment;filename=" + webName + ".zip");

            out = new BufferedOutputStream(response.getOutputStream());
            byte[] buffer = new byte[4096];
            int readLength = 0;
            while ((readLength = in.read(buffer)) > 0) {
                byte[] bytes = new byte[readLength];
                System.arraycopy(buffer, 0, bytes, 0, readLength);
                out.write(bytes);
            }
            out.flush();

        } catch (IOException e) {
            logger.info("io异常!" + e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                logger.info("关闭io异常!" + e);
            }

            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                logger.info("关闭io异常!" + e);
            }
        }

    }

    /**
     * 读取配置
     * 
     * @param listConfigs
     * @param themeName   主题名字
     * @param webName     站点名字
     */
    private void addThemeConfig(List<SysConfig> listConfigs, String themeName, String webName) {
        if (StringUtils.isNotEmpty(listConfigs)) {
            for (SysConfig sysConfig : listConfigs) {
                if (sysConfig.getConfigKey()
                        .startsWith(OlyWebConfigProperties.THEME_CONFIG_PREFIX.getValue() + themeName)) {
                    sysConfig.setConfigType("N");
                    sysConfig.setConfigGroup(webName + "_" + themeName);
                    if (sysConfigService.selectConfigByGk(sysConfig.getConfigGroup(),
                            sysConfig.getConfigKey()) != null) {
                        sysConfig.setUpdateBy("");
                        sysConfigService.updateConfigByGk(sysConfig);
                    } else {
                        sysConfig.setCreateBy("");
                        sysConfigService.insertConfig(sysConfig);
                    }
                }
            }
        }
    }

    /**
     * 删除远程主题
     * 
     * @param url
     * @param themeName
     * @return
     * @throws IOException
     * @throws ClientProtocolException
     */
    private AjaxResult deleteThemeFile(String url, String themeName)
            throws ClientProtocolException {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("themeName", themeName));
        return post(url, params);
    };

    /**
     * 发送 post请求访问本地应用并根据传递参数不同返回不同结果
     * 
     * @throws IOException
     * @throws ClientProtocolException
     */
    private AjaxResult post(String url, List<NameValuePair> params) throws ClientProtocolException {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // 创建httppost
        HttpPost httpPost = new HttpPost(url);    
        try {
            if(params!=null){
                UrlEncodedFormEntity   uefEntity = new UrlEncodedFormEntity(params, "UTF-8");
                httpPost.setEntity(uefEntity);
            } 
            CloseableHttpResponse response = httpClient.execute(httpPost);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    return JSONObject.parseObject(EntityUtils.toString(entity, "UTF-8"), AjaxResult.class);
                }
            } finally {
                response.close();
            }
        } catch (UnsupportedEncodingException e1) {
            logger.info("参数编码!" + e1);
        } catch (IOException e) {
            logger.info("io异常!" + e);
        } finally {
            // 关闭连接,释放资源
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.info("关闭io异常!" + e);
            }
        }
        return error("数据提交异常");
    }

    /**
     * 发送get请求
     * 
     * @param url
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    private AjaxResult get(String url) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response;
        try {
            response = httpClient.execute(httpGet);
            try {
                return JSONObject.parseObject(EntityUtils.toString(response.getEntity(), "UTF-8"), AjaxResult.class);
            } catch (ParseException e) {
                logger.info("转换异常!" + e);
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            logger.info("连接异常!" + e);
        } catch (IOException e) {
            logger.info("IO异常!" + e);
        }

        return error("数据获取失败");
    }

    /**
     * 使用httpclint 发送文件
     * @param file 上传的文件
     * @return 响应结果
     * 
     */
    private AjaxResult uploadFile(String url, MultipartFile file, String fileParamName,
            Map<String, String> headerParams, Map<String, String> otherParams) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        AjaxResult result = error();
        try {
            String fileName = file.getOriginalFilename();
            HttpPost httpPost = new HttpPost(url);
            // 添加header
            for (Map.Entry<String, String> e : headerParams.entrySet()) {
                httpPost.addHeader(e.getKey(), e.getValue());
            }
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setCharset(Charset.forName("utf-8"));
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);// 加上此行代码解决返回中文乱码问题
            builder.addBinaryBody(fileParamName, file.getInputStream(), ContentType.MULTIPART_FORM_DATA, fileName);// 文件流
            for (Map.Entry<String, String> e : otherParams.entrySet()) {
                builder.addTextBody(e.getKey(), e.getValue());// 类似浏览器表单提交，对应input的name和value
            }
            HttpEntity entity = builder.build();
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost);// 执行提交
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                // 将响应内容转换为字符串
                result = JSONObject.parseObject(EntityUtils.toString(responseEntity, Charset.forName("UTF-8")),
                        AjaxResult.class);
            }
        } catch (IOException e) {
            logger.info("io异常!" + e);
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.info("关闭io异常!" + e);
            }
        }
        return result;
    }
    
    /**
     * 判断文件后缀是否支持编辑
     * 
     * @param path
     * @return
     */ 
    private boolean isSupportSuffix(String path) {
        String supportTypes = sysConfigService.selectConfigDefaultValue(
                OlyThemeConfigProperties.THEME_CONFIG_GROUP.getValue(),
                OlyThemeConfigProperties.THEME_FILE_EDIT_SUPPORT);
        return !supportTypes.contains(FilenameUtils.getExtension(path));
    }

}
