package com.grace.web.controller.cms;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oly.cms.admin.service.ICmsLinkService;
import com.oly.cms.common.domain.entity.CmsLink;
import com.grace.common.annotation.Log;
import com.grace.common.core.controller.BaseController;
import com.grace.common.core.domain.AjaxResult;
import com.grace.common.enums.BusinessType;
import com.grace.common.utils.poi.ExcelUtil;

/**
 * 友情链接Controller
 * 
 * @author zg
 * @date 2022-11-15
 */
@RestController
@RequestMapping("/cms/link")
public class CmsLinkController extends BaseController {
    @Autowired
    private ICmsLinkService cmsLinkService;

    /**
     * 查询友情链接列表
     */
    @PreAuthorize("@ss.hasPermi('cms:link:list')")
    @GetMapping("/list")
    public AjaxResult list(CmsLink cmsLink) {

        return success(cmsLinkService.selectCmsLinkList(cmsLink));
    }

    /**
     * 导出友情链接列表
     */
    @PreAuthorize("@ss.hasPermi('cms:link:export')")
    @Log(title = "友情链接", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsLink cmsLink) {
        List<CmsLink> list = cmsLinkService.selectCmsLinkList(cmsLink);
        ExcelUtil<CmsLink> util = new ExcelUtil<CmsLink>(CmsLink.class);
        util.exportExcel(response, list, "友情链接数据");
    }

    /**
     * 获取友情链接详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:link:query')")
    @GetMapping(value = "/{linkId}")
    public AjaxResult getInfo(@PathVariable("linkId") Integer linkId) {
        return success(cmsLinkService.selectCmsLinkById(linkId));
    }

    /**
     * 新增友情链接
     */
    @PreAuthorize("@ss.hasPermi('cms:link:add')")
    @Log(title = "友情链接", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsLink cmsLink) {
        cmsLink.setCreateBy(getUsername());
        return toAjax(cmsLinkService.insertCmsLink(cmsLink));
    }

    /**
     * 修改友情链接
     */
    @PreAuthorize("@ss.hasPermi('cms:link:edit')")
    @Log(title = "友情链接", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsLink cmsLink) {
        cmsLink.setUpdateBy(getUsername());
        return toAjax(cmsLinkService.updateCmsLink(cmsLink));
    }

    /**
     * 批量修改友情链接状态
     */
    @PreAuthorize("@ss.hasPermi('cms:link:edit')")
    @Log(title = "友情链接", businessType = BusinessType.UPDATE)
    @PutMapping("/visible/{visible}/{linkIds}")
    public AjaxResult updateVisible(@PathVariable Integer visible, @PathVariable Long[] linkIds) {
        CmsLink cmsLink = new CmsLink();
        cmsLink.setVisible(visible);
        cmsLink.setUpdateBy(getUsername());
        for (long linkId : linkIds) {
            cmsLink.setLinkId(linkId);
            cmsLinkService.updateCmsLink(cmsLink);
        }
        return toAjax(linkIds.length);
    }

    /**
     * 批量修改友情链接打开方式
     */
    @PreAuthorize("@ss.hasPermi('cms:link:edit')")
    @Log(title = "友情链接", businessType = BusinessType.UPDATE)
    @PutMapping("/openType/{openType}/{linkIds}")
    public AjaxResult updateOpenType(@PathVariable Integer openType, @PathVariable Long[] linkIds) {
        CmsLink cmsLink = new CmsLink();
        cmsLink.setOpenType(openType);
        cmsLink.setUpdateBy(getUsername());
        for (long linkId : linkIds) {
            cmsLink.setLinkId(linkId);
            cmsLinkService.updateCmsLink(cmsLink);
        }
        return toAjax(linkIds.length);
    }

    /**
     * 删除友情链接
     */
    @PreAuthorize("@ss.hasPermi('cms:link:remove')")
    @Log(title = "友情链接", businessType = BusinessType.DELETE)
    @DeleteMapping("/{linkIds}")
    public AjaxResult remove(@PathVariable Long[] linkIds) {
        return toAjax(cmsLinkService.deleteCmsLinkByIds(linkIds));
    }

}
