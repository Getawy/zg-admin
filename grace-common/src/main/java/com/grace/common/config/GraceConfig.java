package com.grace.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 * 
 * @author grace
 */
@Component
@ConfigurationProperties(prefix = "grace")
public class GraceConfig {
    /** 项目名称 */
    private String name;

    /** 版本 */
    private String version;

    /** 版权年份 */
    private String copyrightYear;

    /** 服务工作目录 */
    private static String workServerDir;

    /** 储存工作目录 */
    private static String workStorageDir;

    /** 获取地址开关 */
    private static boolean addressEnabled;

    /** 验证码类型 */
    private static String captchaType;

    /** 唯一编码 */
    private static String onlyCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCopyrightYear() {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear) {
        this.copyrightYear = copyrightYear;
    }

    public static boolean isAddressEnabled() {
        return addressEnabled;
    }

    public void setAddressEnabled(boolean addressEnabled) {
        GraceConfig.addressEnabled = addressEnabled;
    }

    public static String getCaptchaType() {
        return captchaType;
    }

    public void setCaptchaType(String captchaType) {
        GraceConfig.captchaType = captchaType;
    }

    // /**
    // * 获取导入上传路径
    // */
    // public static String getImportPath() {
    // return getWorkDir() + "/import";
    // }

    // /**
    // * 获取下载路径
    // */
    // public static String getDownloadPath() {
    // return getWorkDir() + "/download/";
    // }

    // /**
    // * 获取上传路径
    // */
    // public static String getUploadPath() {
    // return getWorkDir() + "/upload";
    // }

    /**
     * 获取服务唯一编码
     * 
     * @return
     */
    public static String getOnlyCode() {
        return onlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        GraceConfig.onlyCode = onlyCode;
    }

    public static String getWorkServerDir() {
        return workServerDir;
    }

    /**
     * 此方法不能为静态方法
     * 否则获取值为null
     * 
     * @param workServerDir
     */
    public void setWorkServerDir(String workServerDir) {
        GraceConfig.workServerDir = workServerDir;
    }

    public static String getWorkStorageDir() {
        return workStorageDir;
    }

    public void setWorkStorageDir(String workStorageDir) {
        GraceConfig.workStorageDir = workStorageDir;
    }

}
