package com.grace.common.core.domain.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.model.JsonModel;

/**
 * 用户详细信息对象 sys_user_info
 * 
 * @author ZD
 * @date 2022-04-28
 */
public class SysUserInfo extends SysUser {
    private static final long serialVersionUID = 1L;

    /** 标签 */
    @Excel(name = "标签")
    private String tags;

    /** 爱好 */
    @Excel(name = "爱好")
    private String hobby;

    /** 位置 */
    @Excel(name = "位置")
    private JsonModel[] local;

    /** 介绍 */
    @Excel(name = "介绍")
    private String introduce;

    /** 教育 */
    @Excel(name = "教育")
    private JsonModel[] teach;

    /** 社交 */
    @Excel(name = "社交")
    private JsonModel[] contact;

    /** 联系 */
    @Excel(name = "联系")
    private JsonModel[] touch;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 更多展示 */
    @Excel(name = "更多展示")
    private JsonModel[] shows;

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getHobby() {
        return hobby;
    }

    public void setLocal(JsonModel[] local) {
        this.local = local;
    }

    public JsonModel[] getLocal() {
        return local;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setTeach(JsonModel[] teach) {
        this.teach = teach;
    }

    public JsonModel[] getTeach() {
        return teach;
    }

    public void setContact(JsonModel[] contact) {
        this.contact = contact;
    }

    public JsonModel[] getContact() {
        return contact;
    }

    public void setTouch(JsonModel[] touch) {
        this.touch = touch;
    }

    public JsonModel[] getTouch() {
        return touch;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setShows(JsonModel[] shows) {
        this.shows = shows;
    }

    public JsonModel[] getShows() {

        return shows;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("userId", getUserId())
                .append("tags", getTags()).append("hobby", getHobby()).append("local", getLocal())
                .append("introduce", getIntroduce()).append("teach", getTeach()).append("contact", getContact())
                .append("touch", getTouch()).append("birthday", getBirthday()).append("shows", getShows()).toString();
    }
}
