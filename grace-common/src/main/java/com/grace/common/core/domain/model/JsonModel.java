package com.grace.common.core.domain.model;

import java.io.Serializable;

/**
 * Json 模型
 * 对应mysql json数据类型
 * [{key:qq,label:qq,value:634866553},{key:phone,label:电话,value:17678999448},{key:myPhone,label:我的电话,value:17678999448}]
 * 
 * @author zg
 */
public class JsonModel implements Serializable {
    private static final long serialVersionUID = 653673311404741257L;
    /** 主键 */
    private String key;
    /** 名字 */
    private String label;
    /** 参数 */
    private String value;
    /** 备注 */
    private String remark;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
