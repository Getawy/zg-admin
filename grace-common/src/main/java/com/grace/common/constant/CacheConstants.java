package com.grace.common.constant;

/**
 * 缓存的key 常量
 * 
 * @author grace
 */
public class CacheConstants {
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "system_login_tokens:";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "system_captcha_codes:";

    /**
     * 其它验证码 redis key
     */
    public static final String THIRD_CODE_KEY = "system_third_codes:";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "system_sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "system_sys_dict:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "system_repeat_submit:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "system_rate_limit:";

    /**
     * 登录账户密码错误次数 redis key
     */
    public static final String PWD_ERR_CNT_KEY = "system_pwd_err_cnt:";

}
