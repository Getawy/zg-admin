package com.grace.common.enums;
/**
 * 通用状态
 */
public enum SysYesNo {
     /**
     * 是
     */
    YES("是"),

    /**
     * 否
     */
    NO("否");

    private String value;

    private SysYesNo(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
