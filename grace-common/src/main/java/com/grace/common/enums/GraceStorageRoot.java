package com.grace.common.enums;

import java.nio.file.Paths;

import com.grace.common.config.GraceConfig;

/**
 * 文件储存目录
 * xxx/.tzg/olyCode/value
 * 前台后台都能访问
 */
public enum GraceStorageRoot {

    // 本地存储文件地址
    UPLOAD_DIR("upload"),
    // 云服务存储文件地址
    UPLOAD_YUN_DIR("upload"),
    // 头像目录
    AVATAR_DIR("avatar"),
    // 临时目录
    TMP_DIR("tmp");

    private String value;

    GraceStorageRoot(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    /**
     * 仅仅获取路径
     * 
     * @param childDir
     * @return
     */
    public String getDir(String... childDir) {
        return Paths.get(value, childDir).toString();
    }

    /**
     * 获取带工作目录的路径+文件夹+指定路径
     * workStorageDir/value/childDir
     * 
     * @param childDir
     * @return
     */
    public String getWorkRoot(String childDir) {
        return Paths.get(GraceConfig.getWorkStorageDir(), value, childDir).toString();
    }

    public String getWorkRoot() {
        return Paths.get(GraceConfig.getWorkStorageDir(), value).toString();
    }

}
