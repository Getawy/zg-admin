package com.grace.common.enums;

import java.nio.file.Paths;

import com.grace.common.config.GraceConfig;

/**
 * 文件储存目录
 * xxx/.tzg/olyCode/value
 * 
 */
public enum GraceServerRoot {
    // 备份目录
    BACK_DIR("backUp"),
    // 邮件目录
    MAIL_DIR("mail"),
    // 本地日志目录
    LOGS_DIR("logs"),
    // 导入临时路径
    IMPORT_DIR("import"),
    // 导出临时路径
    EXPORT_DIR("export"),
    /**
     * workDir xxx/.tzg/olyCode/themes 存放主题的目录
     * 
     * xxx/.tzg/olyCode/themes/ 存放主题的目录
     * static/theme1
     * templates/theme1
     * 
     * xxx/.tzg/olyCode/themes/ 存放主题的目录
     * static/theme2
     * templates/theme2
     * 
     */
    THEME_DIR("themes"),
    // 模板目录
    THEME_STATIC_DIR("static"),
    // 模板目录
    THEME_TEMPLATE_DIR("templates"),
    // siteMap目录
    SITE_DIR("site");

    private String value;

    GraceServerRoot(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    /**
     * 仅仅获取路径
     * 
     * @param childDir
     * @return
     */
    public String getDir(String... childDir) {
        return Paths.get(value, childDir).toString();
    }

    /**
     * 获取带工作目录的路径+文件夹+指定路径
     * workServerDir/value/childDir
     * 
     * @param childDir
     * @return
     */
    public String getWorkRoot(String childDir) {
        return Paths.get(GraceConfig.getWorkServerDir(), value, childDir).toString();
    }

    public String getWorkRoot() {
        return Paths.get(GraceConfig.getWorkServerDir(), value).toString();
    }

}
