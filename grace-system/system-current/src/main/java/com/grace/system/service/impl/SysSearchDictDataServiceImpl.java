package com.grace.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grace.common.core.domain.entity.SysDictData;
import com.grace.system.mapper.SysSearchDictDataMapper;
import com.grace.system.service.ISysSearchDictDataService;

@Service
public class SysSearchDictDataServiceImpl implements ISysSearchDictDataService {

    @Autowired
    private SysSearchDictDataMapper searchDictDataMapper;

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataList(SysDictData dictData) {
        return searchDictDataMapper.selectDictDataList(dictData);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue) {
        return searchDictDataMapper.selectDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictData selectDictDataById(Long dictCode) {
        return searchDictDataMapper.selectDictDataById(dictCode);
    }

    @Override
    public int countDictDataByType(String dictType) {

        return searchDictDataMapper.countDictDataByType(dictType);
    }

    @Override
    public List<SysDictData> selectDictDataByType(String dictType) {

        return searchDictDataMapper.selectDictDataByType(dictType);
    }

}
