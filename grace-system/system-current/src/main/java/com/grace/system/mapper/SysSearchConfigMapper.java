package com.grace.system.mapper;

import java.util.List;
import com.grace.system.domain.SysConfig;

/**
 * 参数配置 数据层
 * 
 * @author grace
 */
public interface SysSearchConfigMapper {
    /**
     * 查询参数配置信息
     * 
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    public SysConfig selectConfig(SysConfig config);

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<SysConfig> selectConfigList(SysConfig config);

    /**
     * 获取配置列表
     * 
     * @return 配置组列表
     */
    public List<String> selectConfigGroupList();

}
