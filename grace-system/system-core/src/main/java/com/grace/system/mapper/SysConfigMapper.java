package com.grace.system.mapper;

import com.grace.system.domain.SysConfig;

/**
 * 参数配置 数据层
 * 
 * @author grace
 */
public interface SysConfigMapper {

    /**
     * 新增参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int insertConfig(SysConfig config);

    /**
     * 删除参数配置
     * 
     * @param configId 参数ID
     * @return 结果
     */
    public int deleteConfigById(Long configId);

    /**
     * 批量删除参数信息
     * 
     * @param configIds 需要删除的参数ID
     * @return 结果
     */
    public int deleteConfigByIds(Long[] configIds);

    /**
     * 修改参数配置ById
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfigById(SysConfig config);

    /**
     * 修改参数配置通过配置组
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfigByGk(SysConfig config);

}
