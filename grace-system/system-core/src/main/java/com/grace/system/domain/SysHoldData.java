package com.grace.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.grace.common.annotation.Excel;
import com.grace.common.core.domain.BaseEntity;
import com.grace.common.core.domain.model.JsonModel;

/**
 * 权限数据映射对象 sys_hold_data
 * 
 * @author grace
 * @date 2023-09-07
 */
public class SysHoldData extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 权限类型 */
    @Excel(name = "权限类型")
    private Integer holdType;

    /** 数据类型 */
    @Excel(name = "数据类型")
    private Integer dataType;

    /** 权限名称 */
    @Excel(name = "权限名称")
    private String holdName;

    /** 数据内容 */
    @Excel(name = "数据内容")
    private JsonModel[] holdData;

    /** 权限code(类型+字段ID) */
    @Excel(name = "权限Code")
    private String holdCode;

    public void setHoldType(Integer holdType) {
        this.holdType = holdType;
    }

    public Integer getHoldType() {
        return holdType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setHoldName(String holdName) {
        this.holdName = holdName;
    }

    public String getHoldName() {
        return holdName;
    }

    public JsonModel[] getHoldData() {
        return holdData;
    }

    public void setHoldData(JsonModel[] holdData) {
        this.holdData = holdData;
    }

    public void setHoldCode(String HoldCode) {
        this.holdCode = HoldCode;
    }

    public String getHoldCode() {
        return holdCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("holdType", getHoldType())
                .append("dataType", getDataType())
                .append("holdName", getHoldName())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("holdData", getHoldData())
                .append("remark", getRemark())
                .append("holdCode", getHoldCode())
                .toString();
    }
}
