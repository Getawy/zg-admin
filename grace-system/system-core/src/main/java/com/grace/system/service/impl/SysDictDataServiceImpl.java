package com.grace.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.grace.common.core.domain.entity.SysDictData;
import com.grace.common.utils.DictUtils;
import com.grace.system.mapper.SysDictDataMapper;
import com.grace.system.service.ISysDictDataService;

/**
 * 字典 业务层处理
 * 
 * @author grace
 */
@Service
public class SysDictDataServiceImpl implements ISysDictDataService {
    @Autowired
    private SysDictDataMapper dictDataMapper;

    @Autowired
    private SysSearchDictDataServiceImpl searchDictDataService;

    /**
     * 批量删除字典数据信息
     * 
     * @param dictCodes 需要删除的字典数据ID
     */
    @Override
    public void deleteDictDataByIds(Long[] dictCodes) {
        for (Long dictCode : dictCodes) {
            SysDictData data = selectDictDataById(dictCode);
            dictDataMapper.deleteDictDataById(dictCode);
            List<SysDictData> dictDatas = searchDictDataService.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(SysDictData data) {
        int row = dictDataMapper.insertDictData(data);
        if (row > 0) {
            List<SysDictData> dictDatas = searchDictDataService.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(SysDictData data) {
        int row = dictDataMapper.updateDictData(data);
        if (row > 0) {
            List<SysDictData> dictDatas = searchDictDataService.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    @Override
    public int countDictDataByType(String dictType) {

        return searchDictDataService.countDictDataByType(dictType);
    }

    @Override
    public List<SysDictData> selectDictDataList(SysDictData dictData) {

        return searchDictDataService.selectDictDataList(dictData);
    }

    @Override
    public String selectDictLabel(String dictType, String dictValue) {

        return searchDictDataService.selectDictLabel(dictType, dictValue);
    }

    @Override
    public SysDictData selectDictDataById(Long dictCode) {

        return searchDictDataService.selectDictDataById(dictCode);
    }

    @Override
    public List<SysDictData> selectDictDataByType(String dictType) {

        return searchDictDataService.selectDictDataByType(dictType);
    }
}
