## 
    主题制作相关

### 默认工具

#### 系统默认工具

* 主题目录生成工具

源码位于grace-oly/oly-cms/cms-web/src/resources/static/default/js/tocTree.js

```html
// 引入
<script th:src="@{/static/default/js/tocTree.js}"></script>
<script >
   // 构造目录树 buildToc(包裹内容的ID,需要截取的目录节点,描点前缀)
  const tocData =new buildToc('article-container','h1, h2, h3, h4, h5, h6','love-').getTree();
   // 依据目录树构造目录
</script>



```

#### 默认错误页面
